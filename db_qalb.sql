/*
Navicat MySQL Data Transfer

Source Server         : MySQL Local
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : db_qalb

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2020-02-02 14:53:48
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for banners
-- ----------------------------
DROP TABLE IF EXISTS `banners`;
CREATE TABLE `banners` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `excerpt` text,
  `detail` text,
  `file` varchar(255) NOT NULL,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`banner_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of banners
-- ----------------------------
INSERT INTO `banners` VALUES ('1', null, 'Getup school', '', 'Getup school', '<p>Getup school</p>', 'uploads/banners/2019/03/6ea94cf71fa14c6c9f77d695642e9df4.JPG', '1', '0', '2019-02-27 10:18:22', '1', '2019-04-09 21:50:50', '1', '0', '2019-04-09 15:09:00', '1', '2019-03-12', '2019-04-01', 'home');
INSERT INTO `banners` VALUES ('2', null, 'Getup school to', 'getup-school-to', 'Getup school to', '<p>Getup school to<br></p>', 'uploads/banners/2019/03/94c3cde0d841b4d7dc5c1aeef2fa4faf.JPG', '0', '0', '2019-03-12 23:55:40', '1', '2019-04-09 16:37:25', '1', '1', '2019-04-06 14:58:07', '1', '2019-03-12', '2019-04-01', 'articles');
INSERT INTO `banners` VALUES ('3', null, 'Getup school tree', 'getup-school-tree', 'Getup school tree', '<p>Getup school tree<br></p>', 'uploads/banners/2019/03/478bc989e8cc7d24575e4cf0218a69bc.JPG', '0', '0', '2019-03-12 23:59:18', '1', '2019-04-09 16:37:25', '1', '1', '2019-04-06 14:58:07', '1', '2019-03-12', '2019-04-01', 'home');
INSERT INTO `banners` VALUES ('4', null, 'Getup school fo', 'getup-school-fo', 'Getup school fo', '<p>Getup school fo<br></p>', 'uploads/banners/2019/03/7119a1ccc0afcdae3824889b8839eec8.JPG', '1', '0', '2019-03-13 23:09:51', '1', '2019-06-20 13:38:50', '1', '0', '2019-04-09 17:23:48', '1', '2019-03-13', '2019-04-01', 'courses');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `categoryId` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` smallint(6) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `type` varchar(150) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `detail` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`categoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1', null, '', null, null, 'ฟ', 'ฟ', 'ฟ', '0', '0', '2019-12-19 23:29:50', '1', '2019-12-19 23:32:49', '1', '1', '2019-12-19 23:32:49', '1');
INSERT INTO `category` VALUES ('2', null, '', null, null, 'ิิแ', 'แ', 'แ', '0', '0', '2019-12-19 23:30:10', '1', '2019-12-19 23:31:51', '1', '2', '2019-12-19 23:31:34', '1');
INSERT INTO `category` VALUES ('3', '0', '', null, 'product', 'USB มิเตอร์', 'USB มิเตอร์', 'USB มิเตอร์', '1', '0', '2019-12-19 23:51:34', '1', '2019-12-20 00:01:02', '1', '0', null, null);

-- ----------------------------
-- Table structure for ci_sessions
-- ----------------------------
DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions` (
  `id` varchar(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `data` longtext,
  PRIMARY KEY (`id`,`ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ci_sessions
-- ----------------------------
INSERT INTO `ci_sessions` VALUES ('0md8lo465jporqs2ib5imbjodv9jo2gl', '127.0.0.1', '1576767580', '__ci_last_regenerate|i:1576767580;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:13:\"administrator\";s:4:\"Name\";s:42:\"Super โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:35:\"http://qalb.test:82/images/user.png\";s:5:\"roles\";a:19:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}');
INSERT INTO `ci_sessions` VALUES ('18us591af4ddoak297t05c39sgbo217b', '127.0.0.1', '1576771735', '__ci_last_regenerate|i:1576771735;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:13:\"administrator\";s:4:\"Name\";s:42:\"Super โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:35:\"http://qalb.test:82/images/user.png\";s:5:\"roles\";a:7:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:3;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:6;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:4;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:5;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}');
INSERT INTO `ci_sessions` VALUES ('36telmm9pqscrr0152dhgo40alegk3a2', '127.0.0.1', '1576772671', '__ci_last_regenerate|i:1576772671;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:13:\"administrator\";s:4:\"Name\";s:42:\"Super โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:35:\"http://qalb.test:82/images/user.png\";s:5:\"roles\";a:7:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:3;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:6;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:4;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:5;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}');
INSERT INTO `ci_sessions` VALUES ('3ij0tc0i91vhgn1q3gbtvt1530f7l0q9', '127.0.0.1', '1579972391', '__ci_last_regenerate|i:1579972314;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:13:\"administrator\";s:4:\"Name\";s:42:\"Super โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:35:\"http://qalb.test:82/images/user.png\";s:5:\"roles\";a:7:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:3;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:6;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:4;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:5;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:16:\"category_product\";a:7:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"updated_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:7:\"recycle\";i:0;}}');
INSERT INTO `ci_sessions` VALUES ('3s5jvf2khk4of35hhbp1m4kbs93unall', '127.0.0.1', '1576768382', '__ci_last_regenerate|i:1576768382;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:13:\"administrator\";s:4:\"Name\";s:42:\"Super โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:35:\"http://qalb.test:82/images/user.png\";s:5:\"roles\";a:19:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}');
INSERT INTO `ci_sessions` VALUES ('65ge0chet5pma8qb87kqnilsta4g2v4u', '127.0.0.1', '1576774872', '__ci_last_regenerate|i:1576774872;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:13:\"administrator\";s:4:\"Name\";s:42:\"Super โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:35:\"http://qalb.test:82/images/user.png\";s:5:\"roles\";a:7:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:3;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:6;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:4;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:5;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:16:\"category_product\";a:7:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"updated_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:7:\"recycle\";i:0;}}');
INSERT INTO `ci_sessions` VALUES ('6is6hiiof6a6djk597nq9ina4k5slu7t', '127.0.0.1', '1576770171', '__ci_last_regenerate|i:1576770171;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:13:\"administrator\";s:4:\"Name\";s:42:\"Super โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:35:\"http://qalb.test:82/images/user.png\";s:5:\"roles\";a:19:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}');
INSERT INTO `ci_sessions` VALUES ('817g5s6oq5pdikaqq8vtjd9uhcp56imm', '127.0.0.1', '1576776479', '__ci_last_regenerate|i:1576776193;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:13:\"administrator\";s:4:\"Name\";s:42:\"Super โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:35:\"http://qalb.test:82/images/user.png\";s:5:\"roles\";a:7:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:3;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:6;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:4;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:5;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:8:\"products\";a:7:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"updated_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:7:\"recycle\";i:0;}}');
INSERT INTO `ci_sessions` VALUES ('84qpd0mbvjihc7od473i58n0flo9smcv', '127.0.0.1', '1576774239', '__ci_last_regenerate|i:1576774239;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:13:\"administrator\";s:4:\"Name\";s:42:\"Super โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:35:\"http://qalb.test:82/images/user.png\";s:5:\"roles\";a:7:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:3;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:6;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:4;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:5;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:16:\"category_product\";a:7:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"updated_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:7:\"recycle\";i:0;}}');
INSERT INTO `ci_sessions` VALUES ('8q8l3lplmip5co3qh594riqvvffrpbhm', '127.0.0.1', '1576768766', '__ci_last_regenerate|i:1576768766;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:13:\"administrator\";s:4:\"Name\";s:42:\"Super โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:35:\"http://qalb.test:82/images/user.png\";s:5:\"roles\";a:19:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}');
INSERT INTO `ci_sessions` VALUES ('a3ov6t8ou0aiiho4ispqaf5p7hnoiphr', '127.0.0.1', '1576775801', '__ci_last_regenerate|i:1576775800;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:13:\"administrator\";s:4:\"Name\";s:42:\"Super โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:35:\"http://qalb.test:82/images/user.png\";s:5:\"roles\";a:7:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:3;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:6;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:4;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:5;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:8:\"products\";a:7:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"updated_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:7:\"recycle\";i:0;}}');
INSERT INTO `ci_sessions` VALUES ('a5c2o6gr24fed83q32jqovmb1847g7r2', '127.0.0.1', '1576773426', '__ci_last_regenerate|i:1576773426;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:13:\"administrator\";s:4:\"Name\";s:42:\"Super โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:35:\"http://qalb.test:82/images/user.png\";s:5:\"roles\";a:7:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:3;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:6;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:4;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:5;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:16:\"category_product\";a:7:{s:4:\"draw\";s:1:\"2\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"updated_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:7:\"recycle\";i:0;}}');
INSERT INTO `ci_sessions` VALUES ('atriq80n4rlhljllhnjau99ns2mvg2m7', '127.0.0.1', '1576768079', '__ci_last_regenerate|i:1576768079;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:13:\"administrator\";s:4:\"Name\";s:42:\"Super โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:35:\"http://qalb.test:82/images/user.png\";s:5:\"roles\";a:19:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}');
INSERT INTO `ci_sessions` VALUES ('caghvq7jd48k8iqemhor0g8gd38fju31', '127.0.0.1', '1576776193', '__ci_last_regenerate|i:1576776193;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:13:\"administrator\";s:4:\"Name\";s:42:\"Super โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:35:\"http://qalb.test:82/images/user.png\";s:5:\"roles\";a:7:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:3;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:6;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:4;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:5;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:8:\"products\";a:7:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"updated_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:7:\"recycle\";i:0;}}');
INSERT INTO `ci_sessions` VALUES ('idugs40in81g8e29c2fqcfulfcqnavhm', '127.0.0.1', '1576769385', '__ci_last_regenerate|i:1576769385;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:13:\"administrator\";s:4:\"Name\";s:42:\"Super โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:35:\"http://qalb.test:82/images/user.png\";s:5:\"roles\";a:19:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}');
INSERT INTO `ci_sessions` VALUES ('nk0r5nq3sn3sama09qaedmof9nar20rh', '127.0.0.1', '1576769074', '__ci_last_regenerate|i:1576769074;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:13:\"administrator\";s:4:\"Name\";s:42:\"Super โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:35:\"http://qalb.test:82/images/user.png\";s:5:\"roles\";a:19:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}');
INSERT INTO `ci_sessions` VALUES ('o6rssps87t9bj8hoauq1pvfk856k9tro', '127.0.0.1', '1576774569', '__ci_last_regenerate|i:1576774569;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:13:\"administrator\";s:4:\"Name\";s:42:\"Super โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:35:\"http://qalb.test:82/images/user.png\";s:5:\"roles\";a:7:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:3;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:6;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:4;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:5;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:16:\"category_product\";a:7:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"updated_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:7:\"recycle\";i:0;}}');
INSERT INTO `ci_sessions` VALUES ('p5f9lffsa6vga2khd8nku13cecfq15af', '127.0.0.1', '1576775174', '__ci_last_regenerate|i:1576775174;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:13:\"administrator\";s:4:\"Name\";s:42:\"Super โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:35:\"http://qalb.test:82/images/user.png\";s:5:\"roles\";a:7:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:3;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:6;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:4;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:5;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:8:\"products\";a:7:{s:4:\"draw\";s:1:\"2\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"updated_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:7:\"recycle\";i:0;}}');
INSERT INTO `ci_sessions` VALUES ('qk3d9avl9rtkssudh95nj97hho4rlsnr', '127.0.0.1', '1576775495', '__ci_last_regenerate|i:1576775495;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:13:\"administrator\";s:4:\"Name\";s:42:\"Super โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:35:\"http://qalb.test:82/images/user.png\";s:5:\"roles\";a:7:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:3;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:6;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:4;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:5;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:8:\"products\";a:7:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"updated_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:7:\"recycle\";i:0;}}');
INSERT INTO `ci_sessions` VALUES ('rs6smr2blman0hg0dl4mjepupqffhh7f', '127.0.0.1', '1576770919', '__ci_last_regenerate|i:1576770919;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:13:\"administrator\";s:4:\"Name\";s:42:\"Super โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:35:\"http://qalb.test:82/images/user.png\";s:5:\"roles\";a:7:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:3;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:6;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:4;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:5;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}');
INSERT INTO `ci_sessions` VALUES ('sq47dunguu6285ufj0b54cl3hem8hkrm', '127.0.0.1', '1576772984', '__ci_last_regenerate|i:1576772984;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:13:\"administrator\";s:4:\"Name\";s:42:\"Super โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:35:\"http://qalb.test:82/images/user.png\";s:5:\"roles\";a:7:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:3;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:6;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:4;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:5;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:16:\"category_product\";a:7:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"updated_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:7:\"recycle\";i:0;}}');
INSERT INTO `ci_sessions` VALUES ('tipevs993lrcqcr2qfvevdhetsb2tcvu', '127.0.0.1', '1579972314', '__ci_last_regenerate|i:1579972314;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:13:\"administrator\";s:4:\"Name\";s:42:\"Super โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:35:\"http://qalb.test:82/images/user.png\";s:5:\"roles\";a:7:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:3;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:6;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:4;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:5;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:16:\"category_product\";a:7:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"updated_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:7:\"recycle\";i:0;}}');
INSERT INTO `ci_sessions` VALUES ('u5a2c8nb8rm9ssc332d76vtcg9s8nitm', '127.0.0.1', '1576771283', '__ci_last_regenerate|i:1576771283;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:13:\"administrator\";s:4:\"Name\";s:42:\"Super โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:35:\"http://qalb.test:82/images/user.png\";s:5:\"roles\";a:7:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:3;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:6;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:4;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:5;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}');
INSERT INTO `ci_sessions` VALUES ('ui6jo4paghjvafltgmt6khgdg9i42e4n', '127.0.0.1', '1576770555', '__ci_last_regenerate|i:1576770555;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:13:\"administrator\";s:4:\"Name\";s:42:\"Super โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:35:\"http://qalb.test:82/images/user.png\";s:5:\"roles\";a:7:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:3;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:6;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:4;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:5;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}');
INSERT INTO `ci_sessions` VALUES ('vmuqem4344d4s8710ip19hdbpl4u4cbt', '127.0.0.1', '1576769869', '__ci_last_regenerate|i:1576769869;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:13:\"administrator\";s:4:\"Name\";s:42:\"Super โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:35:\"http://qalb.test:82/images/user.png\";s:5:\"roles\";a:19:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}');

-- ----------------------------
-- Table structure for config
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `variable` varchar(50) NOT NULL DEFAULT '',
  `value` text,
  `lang` varchar(2) NOT NULL DEFAULT 'th',
  `description` varchar(250) DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`variable`,`lang`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of config
-- ----------------------------
INSERT INTO `config` VALUES ('content', 'บริษัทฯ ก่อตั้งโดยคณะผู้บริหารมืออาชีพที่มากด้วยประสบการณ์และความเชี่ยวชาญมาเป็นเวลานาน โดยมีวัตถุประสงค์เพื่อเสริมสร้างคุณภาพชีวิตที่ดีของผู้พักอาศัย และเสริมสร้างภาพลักษณ์ให้กับโครงการ และสร้างมูลค่าเพิ่มของสินทรัพย์ให้แก่เจ้าของ', 'th', null, '2018-03-15 08:23:05', 'about');
INSERT INTO `config` VALUES ('content', 'Chrn Property Management ถนนประชาอุทิศ ตำบลบางแม่นาง อำเภอบางใหญ่ จังหวัดนนทบุรี 11140\r\nโทรศัพท์: 02-225-3654 โทรสาร: 02-225-3654\r\nอีเมล์: admin@admin.com\r\n', 'th', null, '2018-03-15 09:03:35', 'contact');
INSERT INTO `config` VALUES ('content', 'ข่าวสาร บทความ และกิจกรรมของบริษัทฯ', 'th', null, '2018-03-11 14:17:02', 'news');
INSERT INTO `config` VALUES ('content', 'บริหารงานโดยบุคลากรที่มีประสบการณ์ มีความรู้ความชำนาญและนักวิชาการให้คำแนะนำวางแผนและจัดระบบ จึงทำให้การทำงานมีประสิทธิภาพ  โดยคัดสรรบุคลากรที่มีคุณภาพ  รักงานบริการมีการอบรมบุคลากรทั้งภาคทฤษฏีและภาคปฏิบัติ  มีเจ้าหน้าที่ตรวจสอบ  การปฏิบัติงานจากส่วนกลางเข้าทำการตรวจสอบเพื่อตรวจสอบการปฏิบัติงานของพนักงาน ให้ได้คุณภาพมาตรฐาน  มีหน่วยปฏิบัติการเข้า พบลูกค้าเพื่อแก้ไขปัญหาต่าง ๆ ของระบบงาน', 'th', null, '2018-03-15 08:47:18', 'portfolio');
INSERT INTO `config` VALUES ('content', 'บริหารงานโดยบุคลากรที่มีประสบการณ์ มีความรู้ความชำนาญและนักวิชาการให้คำแนะนำวางแผนและจัดระบบ จึงทำให้การทำงานมีประสิทธิภาพ  โดยคัดสรรบุคลากรที่มีคุณภาพ  รักงานบริการมีการอบรมบุคลากรทั้งภาคทฤษฏีและภาคปฏิบัติ  มีเจ้าหน้าที่ตรวจสอบ  การปฏิบัติงานจากส่วนกลางเข้าทำการตรวจสอบเพื่อตรวจสอบการปฏิบัติงานของพนักงาน ให้ได้คุณภาพมาตรฐาน  มีหน่วยปฏิบัติการเข้า พบลูกค้าเพื่อแก้ไขปัญหาต่าง ๆ ของระบบงาน', 'th', null, '2018-03-15 09:18:38', 'service');
INSERT INTO `config` VALUES ('content', 'ส่วนหนึ่งของทีมงานบริหารที่มีประสบการณ์การทำงานที่เชี่ยวชาญ', 'th', null, '2018-03-10 16:42:36', 'team');
INSERT INTO `config` VALUES ('mailAuthenticate', '1', 'th', null, '2019-03-02 15:39:02', 'mail');
INSERT INTO `config` VALUES ('mailDefault', 'st.info@gmail.com', 'th', null, '2019-03-02 15:39:02', 'mail');
INSERT INTO `config` VALUES ('mailMethod', '1', 'th', null, '2019-03-02 15:39:02', 'mail');
INSERT INTO `config` VALUES ('metaDescription', 'บริษัท คาลบ์ อินดัสทรี (ประเทศไทย) จำกัด', 'th', null, '2019-10-07 10:54:21', 'general');
INSERT INTO `config` VALUES ('metaKeyword', 'บริษัท คาลบ์ อินดัสทรี (ประเทศไทย) จำกัด', 'th', null, '2019-10-07 10:54:21', 'general');
INSERT INTO `config` VALUES ('senderEmail', 'st.info@gmail.com', 'th', null, '2019-03-02 15:39:02', 'mail');
INSERT INTO `config` VALUES ('senderName', 'ST Developer', 'th', null, '2019-03-02 15:39:02', 'mail');
INSERT INTO `config` VALUES ('siteTitle', 'บริษัท คาลบ์ อินดัสทรี (ประเทศไทย) จำกัด', 'th', null, '2019-10-07 10:54:21', 'general');
INSERT INTO `config` VALUES ('SMTPpassword', 'P@ssw0rdGorra', 'th', null, '2019-03-02 15:39:02', 'mail');
INSERT INTO `config` VALUES ('SMTPport', '465', 'th', null, '2019-03-02 15:39:02', 'mail');
INSERT INTO `config` VALUES ('SMTPserver', 'ssl://smtp.gmail.com', 'th', null, '2019-03-02 15:39:02', 'mail');
INSERT INTO `config` VALUES ('SMTPusername', 'st.info@gmail.com', 'th', null, '2019-03-02 15:39:02', 'mail');

-- ----------------------------
-- Table structure for language
-- ----------------------------
DROP TABLE IF EXISTS `language`;
CREATE TABLE `language` (
  `languageId` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `filename` varchar(64) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `default` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `order` int(3) NOT NULL DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  PRIMARY KEY (`languageId`),
  KEY `fk_language_idx` (`default`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of language
-- ----------------------------
INSERT INTO `language` VALUES ('1', 'ไทย', 'th', 'ไทย', null, '1', '1', '1', '2015-06-10 09:57:05', null, '2015-09-23 14:58:11', null);
INSERT INTO `language` VALUES ('2', 'อังกฤษ', 'en', 'English', null, '1', '0', '2', '2015-06-10 09:48:36', null, '2015-09-23 14:58:11', null);

-- ----------------------------
-- Table structure for module
-- ----------------------------
DROP TABLE IF EXISTS `module`;
CREATE TABLE `module` (
  `moduleId` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` smallint(6) DEFAULT NULL,
  `type` int(11) DEFAULT '1' COMMENT '1menu, 2separate',
  `active` tinyint(1) DEFAULT '0' COMMENT '0Disabled, 1Enabled',
  `title` varchar(150) DEFAULT NULL,
  `directory` varchar(200) DEFAULT NULL,
  `class` varchar(50) DEFAULT NULL,
  `order` int(10) DEFAULT '0',
  `method` varchar(50) DEFAULT NULL,
  `param` varchar(150) DEFAULT NULL,
  `isSidebar` int(11) DEFAULT '1' COMMENT '0:hide,1show',
  `isDev` tinyint(1) DEFAULT '0',
  `icon` varchar(50) DEFAULT 'fa fa-angle-double-right',
  `createBy` int(11) DEFAULT '0',
  `updateBy` int(11) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `modify` int(11) DEFAULT '0',
  `view` int(11) DEFAULT '0',
  `print` int(11) DEFAULT '0',
  `import` int(11) DEFAULT '0',
  `export` int(11) DEFAULT '0',
  `descript` text,
  `recycle` int(11) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`moduleId`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of module
-- ----------------------------
INSERT INTO `module` VALUES ('1', '0', '1', '1', 'แผงควบคุม', 'admin', 'dashboard', '1', '', '', '1', '0', 'glyph-icon iconsminds-dashboard', '0', '1', '0000-00-00 00:00:00', '2019-11-20 16:24:49', '0', '1', '0', '0', '0', '', '0', '0000-00-00 00:00:00', null);
INSERT INTO `module` VALUES ('2', '0', '1', '1', 'ตั้งค่า', 'admin', '', '3', '', '', '1', '0', 'glyph-icon simple-icon-settings', '0', '1', '0000-00-00 00:00:00', '2019-04-09 16:34:31', '0', '1', '0', '0', '0', '', '0', '0000-00-00 00:00:00', null);
INSERT INTO `module` VALUES ('3', '2', '1', '1', 'ผู้ใช้งาน', 'admin', 'users', '1', '', '', '1', '0', 'glyph-icon simple-icon-user', '0', '1', '0000-00-00 00:00:00', '2019-04-09 16:34:31', '1', '1', '0', '0', '0', '', '0', '0000-00-00 00:00:00', null);
INSERT INTO `module` VALUES ('4', '2', '1', '1', 'พื้นฐาน', 'admin', 'config_general', '3', '', '', '1', '0', 'glyph-icon iconsminds-monitor-3', '0', '1', '0000-00-00 00:00:00', '2019-04-09 16:34:31', '1', '1', '0', '0', '0', '', '0', '0000-00-00 00:00:00', null);
INSERT INTO `module` VALUES ('5', '2', '1', '1', 'อีเมล์', 'admin', 'config_mail', '4', '', '', '1', '0', 'glyph-icon simple-icon-envelope-open', '0', '1', '0000-00-00 00:00:00', '2019-11-20 16:28:03', '1', '1', '0', '0', '0', '', '0', '0000-00-00 00:00:00', null);
INSERT INTO `module` VALUES ('6', '2', '1', '1', 'กลุ่มผู้ใช้งาน', 'admin', 'roles', '2', '', '', '1', '0', 'glyph-icon simple-icon-people', '0', '1', '0000-00-00 00:00:00', '2019-04-09 16:34:31', '1', '1', '0', '0', '0', '', '0', '0000-00-00 00:00:00', null);
INSERT INTO `module` VALUES ('7', '0', '1', '1', 'โมดูล', 'admin', 'module', '4', '', '', '1', '1', 'glyph-icon simple-icon-list', '0', '1', '0000-00-00 00:00:00', '2019-11-20 16:28:02', '0', '0', '0', '0', '0', '', '0', '0000-00-00 00:00:00', null);
INSERT INTO `module` VALUES ('8', '0', '1', '1', 'ข้อมูลสินค้า', '', '', '2', null, null, '1', '0', 'glyph-icon iconsminds-shop-4', '1', '0', '2019-10-07 11:11:58', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับข้อมูลสินค้า', '0', null, null);
INSERT INTO `module` VALUES ('9', '8', '1', '1', 'ประเภทสินค้า', '', 'category_product', '1', null, null, '1', '0', 'simple-icon-grid', '1', '1', '2019-10-07 11:33:43', '2019-11-20 16:16:41', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับประเภทสินค้า', '0', null, null);
INSERT INTO `module` VALUES ('10', '8', '1', '1', 'รายการสินค้า', '', 'products', '2', null, null, '1', '0', 'simple-icon-list', '1', '0', '2019-10-07 11:34:28', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับรายการสินค้า', '0', null, null);

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT 'ชื่อสิทธิ์',
  `name_en` varchar(255) DEFAULT NULL COMMENT 'ชื่อสิทธิ์',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'เวลาสร้าง',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'เวลาแก้ไข',
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES ('1', 'เข้าถึง', 'view', '2018-01-21 17:24:49', '2018-01-21 17:24:52');
INSERT INTO `permissions` VALUES ('2', 'สร้าง', 'create', '2018-01-21 17:27:13', '2018-01-21 17:27:15');
INSERT INTO `permissions` VALUES ('3', 'แก้ไข', 'edit', '2018-01-21 17:27:40', '2018-01-21 17:27:42');
INSERT INTO `permissions` VALUES ('4', 'ลบ', 'delete', '2018-01-21 17:28:20', '2018-01-21 17:28:22');

-- ----------------------------
-- Table structure for permission_role
-- ----------------------------
DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role` (
  `role_id` int(11) NOT NULL,
  `moduleId` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of permission_role
-- ----------------------------
INSERT INTO `permission_role` VALUES ('21', '1', '1');
INSERT INTO `permission_role` VALUES ('21', '1', '2');
INSERT INTO `permission_role` VALUES ('21', '1', '3');
INSERT INTO `permission_role` VALUES ('21', '1', '4');
INSERT INTO `permission_role` VALUES ('21', '299', '1');
INSERT INTO `permission_role` VALUES ('21', '299', '2');
INSERT INTO `permission_role` VALUES ('21', '299', '3');
INSERT INTO `permission_role` VALUES ('21', '299', '4');
INSERT INTO `permission_role` VALUES ('21', '300', '1');
INSERT INTO `permission_role` VALUES ('21', '300', '2');
INSERT INTO `permission_role` VALUES ('21', '300', '3');
INSERT INTO `permission_role` VALUES ('21', '300', '4');
INSERT INTO `permission_role` VALUES ('21', '296', '1');
INSERT INTO `permission_role` VALUES ('21', '296', '2');
INSERT INTO `permission_role` VALUES ('21', '296', '3');
INSERT INTO `permission_role` VALUES ('21', '296', '4');
INSERT INTO `permission_role` VALUES ('21', '297', '1');
INSERT INTO `permission_role` VALUES ('21', '297', '2');
INSERT INTO `permission_role` VALUES ('21', '297', '3');
INSERT INTO `permission_role` VALUES ('21', '297', '4');
INSERT INTO `permission_role` VALUES ('21', '20', '1');
INSERT INTO `permission_role` VALUES ('21', '20', '2');
INSERT INTO `permission_role` VALUES ('21', '20', '3');
INSERT INTO `permission_role` VALUES ('21', '20', '4');
INSERT INTO `permission_role` VALUES ('16', '1', '1');
INSERT INTO `permission_role` VALUES ('16', '1', '2');
INSERT INTO `permission_role` VALUES ('16', '1', '3');
INSERT INTO `permission_role` VALUES ('16', '1', '4');
INSERT INTO `permission_role` VALUES ('16', '307', '1');
INSERT INTO `permission_role` VALUES ('16', '307', '2');
INSERT INTO `permission_role` VALUES ('16', '307', '3');
INSERT INTO `permission_role` VALUES ('16', '307', '4');
INSERT INTO `permission_role` VALUES ('16', '306', '1');
INSERT INTO `permission_role` VALUES ('16', '306', '2');
INSERT INTO `permission_role` VALUES ('16', '306', '3');
INSERT INTO `permission_role` VALUES ('16', '306', '4');
INSERT INTO `permission_role` VALUES ('16', '305', '1');
INSERT INTO `permission_role` VALUES ('16', '305', '2');
INSERT INTO `permission_role` VALUES ('16', '305', '3');
INSERT INTO `permission_role` VALUES ('16', '305', '4');
INSERT INTO `permission_role` VALUES ('16', '295', '1');
INSERT INTO `permission_role` VALUES ('16', '295', '2');
INSERT INTO `permission_role` VALUES ('16', '295', '3');
INSERT INTO `permission_role` VALUES ('16', '295', '4');
INSERT INTO `permission_role` VALUES ('1', '1', '1');
INSERT INTO `permission_role` VALUES ('1', '1', '2');
INSERT INTO `permission_role` VALUES ('1', '1', '3');
INSERT INTO `permission_role` VALUES ('1', '1', '4');
INSERT INTO `permission_role` VALUES ('1', '9', '1');
INSERT INTO `permission_role` VALUES ('1', '9', '2');
INSERT INTO `permission_role` VALUES ('1', '9', '3');
INSERT INTO `permission_role` VALUES ('1', '9', '4');
INSERT INTO `permission_role` VALUES ('1', '10', '1');
INSERT INTO `permission_role` VALUES ('1', '10', '2');
INSERT INTO `permission_role` VALUES ('1', '10', '3');
INSERT INTO `permission_role` VALUES ('1', '10', '4');
INSERT INTO `permission_role` VALUES ('1', '3', '1');
INSERT INTO `permission_role` VALUES ('1', '3', '2');
INSERT INTO `permission_role` VALUES ('1', '3', '3');
INSERT INTO `permission_role` VALUES ('1', '3', '4');
INSERT INTO `permission_role` VALUES ('1', '6', '1');
INSERT INTO `permission_role` VALUES ('1', '6', '2');
INSERT INTO `permission_role` VALUES ('1', '6', '3');
INSERT INTO `permission_role` VALUES ('1', '6', '4');
INSERT INTO `permission_role` VALUES ('1', '4', '1');
INSERT INTO `permission_role` VALUES ('1', '4', '2');
INSERT INTO `permission_role` VALUES ('1', '4', '3');
INSERT INTO `permission_role` VALUES ('1', '4', '4');
INSERT INTO `permission_role` VALUES ('1', '5', '1');
INSERT INTO `permission_role` VALUES ('1', '5', '2');
INSERT INTO `permission_role` VALUES ('1', '5', '3');
INSERT INTO `permission_role` VALUES ('1', '5', '4');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `productsId` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `detail` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`productsId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of products
-- ----------------------------

-- ----------------------------
-- Table structure for repo
-- ----------------------------
DROP TABLE IF EXISTS `repo`;
CREATE TABLE `repo` (
  `repoId` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `detail` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`repoId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of repo
-- ----------------------------
INSERT INTO `repo` VALUES ('1', '', null, 'ฟ', 'ฟ', 'ฟ', '0', '0', '2019-12-19 23:29:50', '1', '2019-12-19 23:32:49', '1', '1', '2019-12-19 23:32:49', '1');
INSERT INTO `repo` VALUES ('2', '', null, 'ิิแ', 'แ', 'แ', '0', '0', '2019-12-19 23:30:10', '1', '2019-12-19 23:31:51', '1', '2', '2019-12-19 23:31:34', '1');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `role_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `remark` text,
  `active` tinyint(1) unsigned DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `create_by` int(11) unsigned DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_by` int(11) unsigned DEFAULT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `visible` int(11) DEFAULT '1',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'ผู้ดูแลระบบ', 'บริหารจัดการระบบ', '1', '2016-07-13 10:55:21', null, '2019-12-19 22:45:00', '1', '0', null, null, '0');
INSERT INTO `roles` VALUES ('16', 'ผู้อัพเดทข้อมูลเว็บไซต์', 'บริหารจัดการข้อมูลเว็บไซต์', '1', '2018-08-05 00:20:00', '22', '2019-12-19 23:10:41', '1', '0', '2019-12-19 23:02:00', '1', '1');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `oauth_provider` enum('','facebook','google','twitter') DEFAULT NULL,
  `oauth_uid` varchar(50) DEFAULT NULL,
  `oauth_picture` varchar(500) DEFAULT NULL,
  `active` int(1) DEFAULT '0',
  `file` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'administrator', '38a142d6f6a7b889587a97379cdc0775334169ee', 'gmcZSu9Ce5DhZWl1w.bZ3e', 'Super', 'โปรแกรมเมอร์', 'Super  โปรแกรมเมอร์', '', 'admin@admin.com', '', '1', '2019-02-03 16:25:30', 'ADMIN.S', '2019-03-31 17:45:05', '1', '0', null, null, 'developer', null, null, null, '1', null);
INSERT INTO `users` VALUES ('2', 'admin', '38a142d6f6a7b889587a97379cdc0775334169ee', 'gmcZSu9Ce5DhZWl1w.bZ3e', 'Super', 'Admin', 'Super Admin', '', 'admin@admin.com', '', '1', '2019-02-03 16:25:30', 'ADMIN.S', '2019-12-19 22:49:37', '1', '0', '0000-00-00 00:00:00', null, 'admin', null, null, null, '1', null);
INSERT INTO `users` VALUES ('3', 'aa', '5a6eacdfd91bbd8809b1e595f1adebb50c14723b', '4tgVAIMkkv20MkIuELRJlu', 'aa', 'aa', 'aa aa', null, 'test@test.com', null, '16', '2019-12-19 22:15:51', '1', '2019-12-19 23:10:07', '1', '2', '2019-12-19 23:09:59', '1', 'admin', null, null, null, '0', null);

-- ----------------------------
-- Table structure for usertracking
-- ----------------------------
DROP TABLE IF EXISTS `usertracking`;
CREATE TABLE `usertracking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT '0',
  `class` varchar(50) DEFAULT NULL,
  `function` varchar(50) DEFAULT NULL,
  `method` varchar(20) DEFAULT NULL,
  `sessionId` varchar(100) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `requestUri` text NOT NULL,
  `timestamp` varchar(20) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `agent` text NOT NULL,
  `isBrowser` int(11) DEFAULT NULL,
  `browser` varchar(50) DEFAULT NULL,
  `isMobile` int(11) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `isRobot` tinyint(4) DEFAULT NULL,
  `robot` varchar(50) DEFAULT NULL,
  `platform` varchar(50) DEFAULT NULL,
  `isBackend` tinyint(4) DEFAULT '0',
  `referer` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usertracking
-- ----------------------------
INSERT INTO `usertracking` VALUES ('1', '0', 'dashboard', 'index', 'get', 'kb3p4a0g4rkldrrphbvhdb6pl0kf62q3', '', '/dashboard', '1553609676', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '1', 'Chrome', '0', '', '0', '', 'Windows 10', '0', 'http://st-stock.test:82/login/check_login');
INSERT INTO `usertracking` VALUES ('2', '1', 'dashboard', 'index', 'get', 'qen7cojlv1p26l89phgp57cpdmtl2pmh', '', '/dashboard', '1553609878', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '1', 'Chrome', '0', '', '0', '', 'Windows 10', '1', 'http://st-stock.test:82/login/check_login');
INSERT INTO `usertracking` VALUES ('3', '1', 'catalogs', 'index', 'get', 'qen7cojlv1p26l89phgp57cpdmtl2pmh', '', '/catalogs', '1553609954', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '1', 'Chrome', '0', '', '0', '', 'Windows 10', '1', 'http://st-stock.test:82/dashboard');
INSERT INTO `usertracking` VALUES ('4', '1', 'catalogs', 'create', 'get', 'qen7cojlv1p26l89phgp57cpdmtl2pmh', '', '/catalogs/create', '1553609957', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '1', 'Chrome', '0', '', '0', '', 'Windows 10', '1', 'http://st-stock.test:82/catalogs');
INSERT INTO `usertracking` VALUES ('5', '1', 'catalogs', 'index', 'get', 'qen7cojlv1p26l89phgp57cpdmtl2pmh', '', '/catalogs', '1553609964', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '1', 'Chrome', '0', '', '0', '', 'Windows 10', '1', 'http://st-stock.test:82/catalogs/create');
