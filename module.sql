/*
Navicat MySQL Data Transfer

Source Server         : MySQL Local
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : plateform_db

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2019-03-30 15:25:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for module
-- ----------------------------
DROP TABLE IF EXISTS `module`;
CREATE TABLE `module` (
  `moduleId` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` smallint(6) DEFAULT NULL,
  `type` int(11) DEFAULT '1' COMMENT '1menu, 2separate',
  `active` tinyint(1) DEFAULT '0' COMMENT '0Disabled, 1Enabled',
  `title` varchar(150) DEFAULT NULL,
  `directory` varchar(200) DEFAULT NULL,
  `class` varchar(50) DEFAULT NULL,
  `order` int(10) DEFAULT '0',
  `method` varchar(50) DEFAULT NULL,
  `param` varchar(150) DEFAULT NULL,
  `isSidebar` int(11) DEFAULT '1' COMMENT '0:hide,1show',
  `isDev` tinyint(1) DEFAULT '0',
  `icon` varchar(50) DEFAULT 'fa fa-angle-double-right',
  `createBy` int(11) DEFAULT '0',
  `updateBy` int(11) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `modify` int(11) DEFAULT '0',
  `view` int(11) DEFAULT '0',
  `print` int(11) DEFAULT '0',
  `import` int(11) DEFAULT '0',
  `export` int(11) DEFAULT '0',
  `descript` text,
  `recycle` int(11) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`moduleId`)
) ENGINE=InnoDB AUTO_INCREMENT=315 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of module
-- ----------------------------
INSERT INTO `module` VALUES ('1', '0', '1', '1', 'แผงควบคุม', 'admin', 'dashboard', '1', '', null, '1', '0', 'flaticon-line-graph', '0', '3', '0000-00-00 00:00:00', '2019-02-04 16:46:17', '0', '1', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('2', '0', '1', '1', 'ตั้งค่า', 'admin', '', '8', '', '', '1', '0', 'fa fa-cogs', '0', '7', '0000-00-00 00:00:00', '2016-12-10 10:37:56', '0', '1', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('8', '2', '1', '1', 'ผู้ใช้งาน', 'admin', 'users', '3', '', '', '1', '0', 'fa fa-user', '0', '1', '0000-00-00 00:00:00', '2019-03-25 22:00:37', '1', '1', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('9', '2', '1', '1', 'พื้นฐาน', 'admin', 'config_general', '5', null, '', '1', '0', 'fa fa-circle-o', '0', '1', '0000-00-00 00:00:00', '2019-03-02 15:28:20', '1', '1', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('10', '2', '1', '1', 'อีเมล์', 'admin', 'config_mail', '6', '', '', '1', '0', 'fa fa-circle-o', '0', '1', '0000-00-00 00:00:00', '2019-03-02 15:28:21', '1', '1', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('20', '2', '1', '1', 'กลุ่มผู้ใช้งาน', 'admin', 'roles', '4', '', '', '1', '0', 'fa fa-users', '0', null, '0000-00-00 00:00:00', '2019-02-06 22:47:00', '1', '1', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('30', '0', '1', '1', 'โมดูล', 'admin', 'module', '11', '', '', '1', '1', 'fa fa-cog', '0', '7', '0000-00-00 00:00:00', '2016-07-16 13:04:45', '0', '0', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('166', '0', '1', '1', 'คลังไฟล์', 'admin', 'library', '10', '', '', '0', '0', 'fa fa-briefcase', '7', '1', '2016-07-23 08:54:42', '2018-03-23 14:48:52', '0', '0', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('193', '0', '1', '0', 'รายงาน', 'admin', '', '7', '', '', '1', '0', 'fa fa-bar-chart', '7', '3', '2017-01-19 13:06:36', '2018-09-23 22:48:27', '0', '0', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('195', '0', '1', '0', 'ประวัติการใช้งาน', 'admin', 'stat_admin', '14', '', '', '1', '0', 'fa fa-calendar-o', '7', '3', '2017-01-19 13:11:57', '2018-10-13 00:20:58', '0', '0', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('267', '0', '1', '0', 'Repo', 'admin', 'repo', '21', null, null, '1', '1', 'fa fa-circle-o', '1', '3', '2018-03-16 21:32:34', '2018-07-03 04:28:33', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับrepo', '0', null, null);
INSERT INTO `module` VALUES ('270', '0', '2', '0', 'รายการเมนู', 'admin', '', '2', null, null, '1', '0', '', '1', '1', '2018-03-23 13:17:03', '2019-03-29 23:04:45', '0', '0', '0', '0', '0', '', '1', '2019-03-29 23:04:45', '1');
INSERT INTO `module` VALUES ('271', '0', '2', '1', 'นักพัฒนา', 'admin', '', '9', null, null, '1', '1', '', '1', '1', '2018-03-23 16:15:05', '2018-03-23 16:15:16', '0', '0', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('272', '0', '1', '0', 'สำรองข้อมูล', 'admin', 'backup', '16', null, null, '1', '0', 'fa fa-database', '1', '3', '2018-03-23 17:12:35', '2018-07-03 04:29:21', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับสำรองข้อมูล', '0', null, null);
INSERT INTO `module` VALUES ('294', '0', '2', '0', 'สำหรับผู้ดูแลระบบ', 'admin', '', '3', null, null, '1', '0', '', '3', '1', '2018-07-03 03:19:39', '2019-03-29 23:04:45', '0', '0', '0', '0', '0', '', '1', '2019-03-29 23:04:45', '1');
INSERT INTO `module` VALUES ('295', '314', '1', '1', 'รายการบทความ', 'admin', 'articles', '2', null, null, '1', '0', 'fa fa-clone', '1', '1', '2019-02-16 21:30:53', '2019-03-29 23:00:57', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับ ___', '0', null, null);
INSERT INTO `module` VALUES ('296', '0', '1', '0', 'หมวดหมู่บทความ', 'admin', '', '1', null, null, '1', '0', 'fa fa-circle-o', '1', '1', '2019-02-16 22:02:01', '2019-03-29 23:04:32', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับหมวดหมู่บทความ', '1', '2019-03-29 23:04:32', '1');
INSERT INTO `module` VALUES ('297', '0', '1', '0', 'รายการบทความ', 'admin', '', '2', null, null, '1', '0', 'fa fa-angle-right', '1', '1', '2019-02-16 22:03:33', '2019-03-29 23:04:32', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับรายการบทความ', '1', '2019-03-29 23:04:32', '1');
INSERT INTO `module` VALUES ('298', '0', '1', '1', 'จัดการข้อมูลคอร์สเรียน', '', '', '5', null, null, '1', '0', 'fa fa-atlas', '1', '1', '2019-02-16 22:25:20', '2019-02-16 22:26:19', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับ', '0', null, null);
INSERT INTO `module` VALUES ('299', '298', '1', '1', 'ข้อมูลผู้สอน', 'admin', 'instructors', '1', null, null, '1', '0', 'fa fa-angle-right', '1', '1', '2019-02-16 22:27:50', '2019-02-16 22:32:16', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับผู้สอน', '0', null, null);
INSERT INTO `module` VALUES ('300', '298', '1', '1', 'ข้อมูลคอร์สเรียน', 'admin', 'courses', '3', null, null, '1', '0', 'fa fa-angle-right', '1', '0', '2019-02-16 23:09:42', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับคอร์สเรียน', '0', null, null);
INSERT INTO `module` VALUES ('301', '300', '1', '1', 'ข้อมูลเนื้อหาคอร์สเรียน', 'admin', 'courses_lesson', '2', null, null, '0', '0', '', '1', '1', '2019-02-23 16:30:58', '2019-03-02 15:13:54', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับเนื้อหาคอร์สเรียน', '0', null, null);
INSERT INTO `module` VALUES ('302', '302', '1', '1', 'จัดการบทความ', 'admin', 'articles', '0', null, null, '1', '0', 'fa fa-angle-right', '1', '1', '2019-02-23 17:57:07', '2019-02-23 18:00:17', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับจัดการบทความ', '0', null, null);
INSERT INTO `module` VALUES ('303', '302', '1', '1', 'จัดการประเภทบทความ', 'admin', 'managearticles', '0', null, null, '1', '0', 'fa fa-angle-right', '1', '0', '2019-02-23 17:59:35', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับจัดการประเภทบทความ', '0', null, null);
INSERT INTO `module` VALUES ('304', '300', '1', '1', 'จัดการข้อมูลโปรโมชั่น', '', 'promotions', '3', null, null, '0', '0', 'fa fa-angle-right', '1', '0', '2019-03-06 22:18:05', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับโปรโมชั่น', '0', null, null);
INSERT INTO `module` VALUES ('305', '314', '1', '1', 'ประเภทบทความ', '', 'articles_categories', '1', null, null, '1', '0', 'fa fa-angle-right', '1', '1', '2019-03-09 18:38:39', '2019-03-29 23:00:35', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับจัดการประเภทบทความ', '0', null, null);
INSERT INTO `module` VALUES ('306', '0', '1', '1', 'กิจกรรม', '', 'activities', '3', null, null, '1', '0', 'fa fa-calendar', '1', '1', '2019-03-09 19:12:53', '2019-03-29 23:05:24', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับกิจกรรม', '0', null, null);
INSERT INTO `module` VALUES ('307', '0', '1', '1', 'จัดการ banner', '', 'banners', '2', null, null, '1', '0', 'fa fa-map-signs', '1', '1', '2019-03-09 19:15:54', '2019-03-29 23:05:42', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับจัดการ banner', '0', null, null);
INSERT INTO `module` VALUES ('308', '298', '1', '1', 'ประเภทคอร์สเรียน', '', 'courses_categories', '2', null, null, '1', '0', 'fa fa-book', '1', '1', '2019-03-12 16:23:56', '2019-03-29 23:18:05', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับจัดการประเภทคอร์สเรียน', '0', null, null);
INSERT INTO `module` VALUES ('309', '2', '1', '1', 'ผู้สนับสนุน', '', 'supports', '1', null, null, '1', '0', 'fa fa-angle-right', '1', '0', '2019-03-14 11:45:16', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับผู้สนับสนุน', '0', null, null);
INSERT INTO `module` VALUES ('310', '0', '1', '1', 'ข้อมูลเกียรติบัตร', '', 'certificates', '7', null, null, '1', '0', 'fa fa-clone', '1', '1', '2019-03-14 11:53:35', '2019-03-29 23:06:56', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับข้อมูลเกียรติบัตร', '0', null, null);
INSERT INTO `module` VALUES ('311', '2', '1', '1', 'วีดีโอแนะนำ', '', 'presents', '2', null, null, '1', '0', 'fa fa-angle-right', '1', '0', '2019-03-14 22:37:38', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับวีดีโอแนะนำ', '0', null, null);
INSERT INTO `module` VALUES ('312', '300', '1', '1', 'รีวิวคอร์สเรียน', '', 'reviews', '1', null, null, '0', '0', 'fa fa-angle-right', '1', '0', '2019-03-19 21:23:09', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับรีวิวคอร์สเรียน', '0', null, null);
INSERT INTO `module` VALUES ('313', '0', '1', '1', 'คูปองส่วนลด', '', 'coupons', '6', null, null, '1', '0', 'fa fa-credit-card', '1', '1', '2019-03-29 22:13:00', '2019-03-29 22:13:44', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับคูปองส่วนลด', '0', null, null);
INSERT INTO `module` VALUES ('314', '0', '1', '1', 'จัดการข้อมูลบทความ', '', '', '4', null, null, '1', '0', 'fa fa-newspaper', '1', '1', '2019-03-29 22:59:04', '2019-03-29 22:59:15', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับข้อมูลบทความ', '0', null, null);
