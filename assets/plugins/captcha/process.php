<?php

// Begin the session
session_start();

// To avoid case conflicts, make the input uppercase and check against the session value
// If it's correct, echo '1' as a string
if(strtoupper($_POST['captcha_code']) == $_SESSION['captcha_id']){
	$valid = 'true';
// Else echo '0' as a string
}else{
	$valid = 'false';
}

$array=array('valid' => $valid);
echo json_encode($array);

?>