﻿// JavaScript Document

$(document).ready(function() {
  $('.fancybox').fancybox({

	  helpers: {
              title : {
                  type : 'float'
              }
          }
  });

  $('#mainmenu .fancybox').fancybox({

	   afterShow: function(){
       var customContent = "<div class='customHTML'><h5 class='cols textvideo font_level5 pinkcolor circular'>Advanced search</h5></div>"
      $('.fancybox-wrap').append(customContent);
   }
  });

   $('.forgot-password').fancybox({

	   afterShow: function(){
       var customContent = "<div class='customHTML_forgot'><h5 class='cols textvideo font_level5 pinkcolor circular'>Forget Password</h5></div>"
      $('.fancybox-wrap').append(customContent);
 }
  });


  $('.for-register').fancybox({
	   // 'width'  : 600,           // set the width
//        'height' : 200,           // set the height
//        'type'   : 'iframe'       // tell the script to create an iframe
	   afterShow: function(){
       var customContent = "<div class='customHTML titleheadhidden'><h5 class='cols textvideo font_level5 pinkcolor circular'>Register</h5></div>"
      $('.fancybox-wrap').append(customContent);
     }
  });




});