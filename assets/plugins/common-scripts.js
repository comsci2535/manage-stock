/*---LEFT BAR ACCORDION----*/
$(function() {

    /*$('#nav-accordion').dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: true,
        disableLink: true,
        speed: 'slow',
        showCount: false,
        autoExpand: true,
        cookie: 'dcjq-accordion-1',
        classExpand: 'dcjq-current-parent'
    });*/


});

var Script = function () {

//    sidebar dropdown menu auto scrolling

    jQuery('#sidebar .sub-menu > a').click(function () {
        var o = ($(this).offset());
        diff = 250 - o.top;
        if(diff>0)
            $("#sidebar").scrollTo("-="+Math.abs(diff),500);
        else
            $("#sidebar").scrollTo("+="+Math.abs(diff),500);
    });

//    sidebar toggle

    $(function() {
        function responsiveView() {
            var wSize = $(window).width();
            if (wSize <= 768) {
                $('#container').addClass('sidebar-close');
                $('#sidebar > ul').hide();
            }

            if (wSize > 768) {
                $('#container').removeClass('sidebar-close');
                $('#sidebar > ul').show();
            }
        }
        $(window).on('load', responsiveView);
        $(window).on('resize', responsiveView);
    });

    $('.icon-reorder').click(function () {
        if ($('#sidebar > ul').is(":visible") === true) {
            $('#main-content').css({
                'margin-left': '0px'
            });
            $('#sidebar').css({
                'margin-left': '-210px'
            });
            $('#sidebar > ul').hide();
            $("#container").addClass("sidebar-closed");
        } else {
            $('#main-content').css({
                'margin-left': '210px'
            });
            $('#sidebar > ul').show();
            $('#sidebar').css({
                'margin-left': '0'
            });
            $("#container").removeClass("sidebar-closed");
        }
    });

// custom scrollbar
    //$("#sidebar").niceScroll({styler:"fb",cursorcolor:"#e8403f", cursorwidth: '3', cursorborderradius: '10px', background: '#404040', spacebarenabled:false, cursorborder: ''});

    //$("html").niceScroll({styler:"fb",cursorcolor:"#e8403f", cursorwidth: '6', cursorborderradius: '10px', background: '#404040', spacebarenabled:false,  cursorborder: '', zindex: '1000'});

// widget tools

    jQuery('.panel .tools .icon-chevron-down').click(function () {
        var el = jQuery(this).parents(".panel").children(".panel-body");
        if (jQuery(this).hasClass("icon-chevron-down")) {
            jQuery(this).removeClass("icon-chevron-down").addClass("icon-chevron-up");
            el.slideUp(200);
        } else {
            jQuery(this).removeClass("icon-chevron-up").addClass("icon-chevron-down");
            el.slideDown(200);
        }
    });

    jQuery('.panel .tools .icon-remove').click(function () {
        jQuery(this).parents(".panel").parent().remove();
    });


    jQuery('.icon-chevron-down-in-panel').click(function () {

        var el = jQuery(this).parents(".panel-heading").siblings(".panel-body");

        if (jQuery(this).hasClass("icon-chevron-down-in-panel")) {
            jQuery(this).removeClass("icon-chevron-down-in-panel").addClass("icon-chevron-up-in-panel");
            el.slideUp(200);
        } else {
            //console.log(jQuery(this).attr('class'));
            jQuery(this).removeClass("icon-chevron-up-in-panel").addClass("icon-chevron-down-in-panel");
            el.slideDown(200);
        }

    });

//    tool tips

    $('.tooltips').tooltip();

//    popovers

    $('.popovers').popover();

// custom bar chart

    if ($(".custom-bar-chart")) {
        $(".bar").each(function () {
            var i = $(this).find(".value").html();
            $(this).find(".value").html("");
            $(this).find(".value").animate({
                height: i
            }, 2000)
        })
    }

// Jquery ScrollTo

    $('a[id=scroll_top]').click(function(event){ // Footer
        event.preventDefault();
        $(this).parents('body').scrollTo( 0, 800 );
        return false;
    });


}();

// Fnc Check Doc Type
 function getDoc(frame) {
     var doc = null;

     // IE8 cascading access check
     try {
         if (frame.contentWindow) {
             doc = frame.contentWindow.document;
         }
     } catch(err) {
     }

     if (doc) { // successful getting content
         return doc;
     }

     try { // simply checking may throw in ie8 under ssl or mismatched protocol
         doc = frame.contentDocument ? frame.contentDocument : frame.document;
     } catch(err) {
         // last attempt
         doc = frame.document;
     }
     return doc;
 }

function checkFileImage(obj)
{  
  if(obj.value != '')
  {
    var filename = obj.value;
    var valid_extensions = /(\.jpeg|\.jpg|\.png|\.gif)$/i;
    if(valid_extensions.test(filename)){
        return true;
    }else{
        alert_box('ประเภทไฟล์ไม่ถูกต้อง.');
        obj.value='';
    }
  }
}

function checkFileVDO(obj)
{
  if(obj.value != '')
  {
    var filename = obj.value;
    var valid_extensions = /(\.mp4|\.mp3)$/i;
    if(valid_extensions.test(filename)){
        return true;
    }else{
        alert_box('ประเภทไฟล์ไม่ถูกต้อง.');
        obj.value='';
    }
  }
}


function checkFileAds(obj)
{
  if(obj.value != '')
  {
    var filename = obj.value;
   // var valid_extensions = /(\.mp4|\.f4v|\.gif|\.png|\.jpg|\.JPEG)$/i;
    var valid_extensions = /(\.mp4|\.f4v)$/i;
    if(valid_extensions.test(filename)){
        return true;
    }else{
        alert('Incorrect File Type.');
        obj.value='';
    }
  }
}

function checkFileDoc(obj)
{
  if(obj.value != '')
  {
    var filename = obj.value;
   // var valid_extensions = /(\.mp4|\.f4v|\.gif|\.png|\.jpg|\.JPEG)$/i;
    var valid_extensions = /(\.doc|\.docx|\.xls|\.xlsx|\.rar|\.zip|\.pdf|\.ppt|\.pptx|\.txt|\.gif|\.png|\.jpg|\.JPEG)$/i;
    if(valid_extensions.test(filename)){
        return true;
    }else{
        alert_box('ประเภทไฟล์ไม่ถูกต้อง.');
        obj.value='';
    }
  }
}

function checkFileImport(obj)
{
  if(obj.value != '')
  {
    var filename = obj.value;
   // var valid_extensions = /(\.mp4|\.f4v|\.gif|\.png|\.jpg|\.JPEG)$/i;
    var valid_extensions = /(\.xls|\.xlsx)$/i;
    if(valid_extensions.test(filename)){
        return true;
    }else{
        alert('Incorrect File Type.');
        obj.value='';
    }
  }
}


function checkFiletxt(obj)
{
  if(obj.value != '')
  {
    var filename = obj.value;
   // var valid_extensions = /(\.mp4|\.f4v|\.gif|\.png|\.jpg|\.JPEG)$/i;
    var valid_extensions = /(\.txt)$/i;
    if(valid_extensions.test(filename)){
        return true;
    }else{
        alert('Incorrect File Type.');
        obj.value='';
    }
  }
}

function checkNumeric(obj)
{
        if ( isNaN(obj.value) )
        {
         // console.log(obj);
            alert('Please Input Number Only!');
            obj.value = '';
            return false;

        }
 }

function _confirm()
{
   if (confirm('Do You Want To Delete ?') == true) {
      return true;
   }
   else
   {
      return false;
   }
}

function validateMail(str){
   //  var Email=/^([a-zA-Z0-9]+)@([a-zA-Z0-9]+)\.([a-zA-Z0-9]{2,5})$/
     var Email = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
     if(!str.value.match(Email)){
          alert('In Valid E-Mail Format');
          str.focus();
          return false;
     }
}
