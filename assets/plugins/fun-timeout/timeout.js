//--------------------------------------------------------------------------------------
// START COUNTDOWS TIME AND SET ACTION FOR END TIME
//--------------------------------------------------------------------------------------
// SET COLOR	ON TIME
//--------------------------------------------------------------------------------------
var runTime1 = 0 ;
var runTime2 = 0 ;
function countDown(){
	var hours = parseInt($('.hours').html());
    		var minutes = parseInt($('.minutes').html());
    		var seconds = parseInt($('.seconds').html());
    		if( hours == 0 && minutes < 15  ){
    		    runTime1++;
                if(runTime1 == 1){
                    alert('เหลือเวลาใช้งานอีก 15 นาที ');
                }
    			$('.hours').css( "color", "yellow" );
    			$('.minutes').css( "color", "yellow" );
    			$('.seconds').css( "color", "yellow" );


    		}

            if( hours == 0 && minutes < 5){
                    runTime2++;
                    if(runTime2 == 1){
                        alert('เหลือเวลาใช้งานอีก 5 นาที ');
                    }

    				$('.hours').css( "color", "red" );
    				$('.minutes').css( "color", "red" );
    				$('.seconds').css( "color", "red" );
    		}


            if( hours == 0 && minutes <= 0){
                 alert('หมดเวลาใช้งานระบบ กรุณาต่อเวลาใช้งาน');
                 window.top.location.href = "website/authen/login";
    		}
    		//console.log(hours+" "+minutes+" "+seconds);

}
//--------------------------------------------------------------------------------------
// SET START COUNTDOWN TIME
//--------------------------------------------------------------------------------------
$(function(){
    $('.timer').startTimer({
      onComplete: function(){
        console.log('Complete');
      }
    });
})

function updateClock()
{
	var currentTime = new Date ( );
 	var currentHours = currentTime.getHours ( );
 	var currentMinutes = currentTime.getMinutes ( );
 	var currentSeconds = currentTime.getSeconds ( );

 	// Pad the minutes and seconds with leading zeros, if required
 	currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
 	currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;

 	// Choose either "AM" or "PM" as appropriate
 	var timeOfDay = ( currentHours < 24 ) ? "PM" : "AM";

 	// Convert the hours component to 12-hour format if needed
 	currentHours = ( currentHours > 24 ) ? currentHours - 24 : currentHours;

 	// Convert an hours component of "0" to "12"
 	currentHours = ( currentHours == 0 ) ? 24 : currentHours;

 	// Compose the string for display
 	var currentTimeString = "<i class='glyphicon glyphicon-time'></i>&nbsp;" + currentHours + ":" + currentMinutes + ":" + currentSeconds;


  	$("#clock").html(currentTimeString);

}



$(document).ready(function(){
    setInterval('updateClock()', 1000);
    setInterval('countDown()', 1000);
});
