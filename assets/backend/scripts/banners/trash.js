"use strict";
$(document).ready(function () {
    dataList.DataTable({
       language: {url: rootUrl+"assets/backend/vendors/datatables/Thai.json"},
        serverSide: true,
        ajax: {
            url: controller+"/data_trash",
            type: 'POST',
            data: {'csrfToken': get_cookie('csrfCookie')},
        },
        order: [[1, "asc"]],
        pageLength: 10,
        columns: [
            {data: "checkbox", width: "20px", className: "text-center", orderable: false, visible:true},
            {data: "title", className: "", orderable: true},
            {data: "excerpt", className: "", orderable: true},
            {data: "recycle_at", width: "130px", className: "", orderable: true},
            {data: "action", width: "30px", className: "text-center", orderable: false},
        ]
    }).on('draw', function () {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    }).on('processing', function(e, settings, processing) {
        if ( processing ) {
            $('#overlay-box').removeClass('hidden');
        } else {
            $('#overlay-box').addClass('hidden');
        }
    }).on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    });
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
