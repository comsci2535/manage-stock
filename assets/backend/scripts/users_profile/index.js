"use strict";
$(document).ready(function () {
    
    $('.frm-change-password').validate({
        rules: {
             oldPassword: {
                remote: {
                    url: controller+"/check_password",
                    type: "post",
                    data: {id: function () {
                            return $('#input-id').val();
                        }, mode: function () {
                            return $('#input-mode').val();
                        }, csrfToken: csrfToken}
                }
            },
            rePassword: {equalTo: "#input-password"}
        },
        messages: {
            oldPassword: {remote: 'รหัสผ่านปัจจุบันไม่ถูกต้อง'},
            rePassword: {equalTo: 'โปรดระบุรหัสผ่านอีกครั้ง'}
        }
    });
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
