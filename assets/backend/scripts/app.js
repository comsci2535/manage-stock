"use strict";



var iCheckboxOpt = {
    checkboxClass: 'icheckbox_square-grey',
    radioClass: 'iradio_square-grey'
}

var tbCheckAll = 0;
var csrfToken = get_cookie('csrfCookie')
var dataList = $('#data-list')
var arrayId = []

$(document).ready(function () {

    

    //Initialize Select2 Elements
    $(".select2").select2();

    $('.amount').keyup( function() {
        
        $(this ).val( formatAmount( $( this ).val() ) );  
    });

    $.validator.setDefaults({
        highlight: function (element) {
            $(element).parent('div').addClass('has-error')
        },
        unhighlight: function (element) {
            $(element).parent('div').removeClass('has-error')
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if ( $(element).hasClass('select2') ) {
                error.insertAfter(element.next('span'))
            } else {
                error.insertAfter(element)
            }
        }
    });

    $.validator.addMethod('isEmail', function (value, element) {
        return this.optional(element) || /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value);
    }, 'โปรดระบุรูปแบบอีเมล์ที่ถูกต้อง');

    $('input.icheck').iCheck(iCheckboxOpt);
    
    // กำหนดสถานะจากตาราง
    var errorToggle = false
    $('#data-list').on('change', '.custom-switch-input', function () {
        var bootstrapToggle = $(this)
        var id = $(this).closest('tr').attr('id')
        if(!id){
            var table = dataList.DataTable();
            var data = table.row( $(this)).data();
            id = data.DT_RowId
        }
        var status = $(this).prop('checked')
        if (!errorToggle) {
            arrayId.push(id);
            $('#overlay-box').removeClass('hidden');
            $.post($(this).data('method'), {id: arrayId, status: status, csrfToken: csrfToken})
            .done(function (data) {
                $('#overlay-box').addClass('hidden');
                if (data.success === true) {

                    showNotificationCustom(data.toastr.lineOne,data.toastr.lineTwo,'top','right','primary');

                } else if (data.success === false) {
                    showNotificationCustom(data.toastr.lineOne,data.toastr.lineTwo,'top','right','primary');
                }
                arrayId = []
            })
            .fail(function () {
                $('#overlay-box').addClass('hidden');
                showNotificationCustom('พบข้อผิดพลาดด้านการสื่อสาร','','top','right','primary');
                arrayId = []
            })
        }
    });

    //ย้ายรายการลงถังขยะ กู้รายการจากถังขยะ ลบรายการถาวร 
    $('#data-list').on('click', '.tb-action', function () {
         //BOOTBOX
         var id = $(this).closest('tr').attr('id')
         var url = $(this).data('method');
         
         $('#overlay-box').removeClass('hidden');        
         arrayId.push(id)
         swal({
            title: 'กรุณายืนยันการทำรายการ',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'ตกลง',
            cancelButtonText: 'ยกเลิก'
        }).then(function(result) {
            
            if (result.value) {
                $.ajax({
                  url: url,
                  type: 'POST',
                  dataType: 'json',
                  headers: {
                    'X-CSRF-TOKEN': csrfToken
                },
                data: {id: arrayId, csrfToken: csrfToken},
            })
                .done(function (data) {
                    if (data.success === true) {
                                //toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                                showNotificationCustom(data.toastr.lineOne,data.toastr.lineTwo,'top','right','primary');
                               
                                dataList.DataTable().draw()
                                 $('#data-list .tb-check-all').iCheck('uncheck');
                            } else if (data.success === false) {
                                $('#overlay-box').addClass('hidden');
                                //toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                                showNotificationCustom(data.toastr.lineOne,data.toastr.lineTwo,'top','right','primary');
                            }
                            arrayId = []
                        })
                .fail(function () {
                            //toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", "")
                            showNotificationCustom('พบข้อผิดพลาดด้านการสื่อสาร','','top','right','primary');
                            arrayId = []
                        })

            }
        });
    })

    // เลือกทั้งหมดจากตาราง
    $('input.check-all').click(function(){
        if ( $(this).prop('checked') ) {
            $(this).closest('#data-list').find('tbody tr>td>').find('.check-single').prop("checked", true);
            tbCheckAll = 1;
        } else {
            $(this).closest('#data-list').find('tbody tr>td>').find('.check-single').prop("checked", false);
        }
    })
    $('#data-list').on('click', '.check-single', function () {
        tbCheckAll = 0;
        $('#data-list .check-all').prop('checked', false);
        $('.check-all').prop('checked', false);
    });


    //ลบทีละหลายๆรายการจากตาราง
    $('.box-tools').on('click', '.trash-multi', function () {
        var set = $('#data-list .check-single')
        $(set).each(function () {
            if ($(this).is(":checked")) {
                arrayId.push($(this).closest('tr').attr('id'))
            }
        })
        if (arrayId.length > 0) {
            //if (confirm("กรุณายืนยันการทำรายการ")) {
                var id = $(this).closest('tr').attr('id')
                var url = $(this).data('method');
                
                swal({
                    title: 'กรุณายืนยันการทำรายการ',
                    text: "",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'ตกลง',
                    cancelButtonText: 'ยกเลิก'
                }).then(function(result) {
                    
                    if (result.value) {
                        $.post(url, {id: arrayId, csrfToken: csrfToken})
                        .done(function (data) {
                            if (data.success === true) {
                                //toastr["success"]("บันทึการเปลี่ยนแปลงเรียบร้อย", appName)
                                showNotificationCustom(appName,'บันทึการเปลี่ยนแปลงเรียบร้อย','top','right','primary');
                                dataList.DataTable().draw()
                                arrayId = []
                            }
                        })
                        .fail(function () {
                            //toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", appName)
                            showNotificationCustom(appName,'พบข้อผิดพลาดด้านการสื่อสาร','top','right','primary');
                        })
                    }
                });
            } else {
            //alert('กรุณาเลือกรายการที่ต้องการลบ')
            alert_box('กรุณาเลือกรายการที่ต้องการลบ');
        }
    })
    
    // ฟิตเตอร์ตารางใน
    $('.box-filter').click(function(e){
        e.preventDefault();
        $('div.filter').slideToggle()
    })
    $('.btn-filter').click(function(){
        dataList.DataTable().draw()
    })
    
    // // create Daterange ใช้ที่ filter ที่หน้าตาราง
    $('input[name="createDateRange"]').daterangepicker({
        locale: { cancelLabel: 'ยกเลิก', applyLabel:'ตกลง', format: 'DD-MM-YYYY' },
        autoUpdateInput: false,
    })   
    $('input[name="createDateRange"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' ถึง ' + picker.endDate.format('DD-MM-YYYY'))
        $('input[name="createStartDate"]').val(picker.startDate.format('YYYY-MM-DD'))
        $('input[name="createEndDate"]').val(picker.endDate.format('YYYY-MM-DD'))
    })
    $('input[name="createDateRange"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
        $('input[name="createStartDate"]').val('')
        $('input[name="createEndDate"]').val('')        
    })  
    
    // update Daterange ใช้ที่ filter ที่หน้าตาราง
    $('input[name="updateDateRange"]').daterangepicker({
        locale: { cancelLabel: 'ยกเลิก', applyLabel:'ตกลง', format: 'DD-MM-YYYY' },
        autoUpdateInput: false,
    })   
    $('input[name="updateDateRange"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' ถึง ' + picker.endDate.format('DD-MM-YYYY'))
        $('input[name="updateStartDate"]').val(picker.startDate.format('YYYY-MM-DD'))
        $('input[name="updateEndDate"]').val(picker.endDate.format('YYYY-MM-DD'))
    })
    $('input[name="updateDateRange"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
        $('input[name="updateStartDate"]').val('')
        $('input[name="updateEndDate"]').val('')        
    })     

})

function SetLocalStorage(name, value)
{
    if (typeof (Storage) !== "undefined") {
        localStorage.setItem(name, value);
    } else {
        alert_box('Web browser ของท่านไม่สนับสนุนการใช้งานฟังก์ชั้นนี้!');
    }
}

function GetLocalStorage(name)
{
    if (typeof (Storage) !== "undefined") {
        var data;
        data = localStorage.getItem(name);
        if (data === null && name === "sa-theme") {
            data = 5;
        }
        return data;
    } else {
        alert_box('Web browser ของท่านไม่สนับสนุนการใช้งานฟังก์ชั้นนี้!');
    }
}

function get_cookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0)
            return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function alert_box(text){
   
 swal({
    title:text, 
    text:"", 
    type:"warning",
    confirmButtonText: 'ตกลง'
});
}


function formatAmount( number ) {

  var number = number.replace( /[^0-9]/g, '' );

  var x = number.split( ',' );
  var x1 = x[0];
  var x2 = x.length > 1 ? ',' + x[1] : '';

  return formatAmountNoDecimals( x1 ) + x2;
}


function formatAmountNoDecimals( number ) {
  var rgx = /(\d+)(\d{3})/;
  while( rgx.test( number ) ) {
      var number = number.replace( rgx, '$1' + ',' + '$2' );
  }
  return number;
}


$(window).on("load", function () {
})

$(window).on("scroll", function () {
})