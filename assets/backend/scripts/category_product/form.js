"use strict";

$(document).ready(function () {
    
    $('.frm-create').validate({
        rules: {
            title: true,
            slug:true,
            managearticle_id: {
                required:true
            },
            excerpt: {
                required:true
            },
            file: {
                required:true
            }
        }
    });

    // form validate editor
    $('form').each(function () {
        if ($(this).data('validator'))
            $(this).data('validator').settings.ignore = ".note-editor *";
    });

    
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
