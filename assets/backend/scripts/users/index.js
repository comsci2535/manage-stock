"use strict";
$(document).ready(function () {
    
    dataList.DataTable({
        sDom: '<"row view-filter"<"col-sm-12"<"float-right"l><"float-left"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
        serverSide: true,
        ajax: {
            url: controller+"/data_index",
            type: 'POST',
            data: {csrfToken:get_cookie('csrfCookie')},
        },
        order: [[1, "asc"]],
        pageLength:10,
        columns: [
            {data: "checkbox", width: "20px", className: "text-center", orderable: false},
            {data: "username", className: "", orderable: true},
            {data: "title", className: "", orderable: true},
            {data: "excerpt", className: "", orderable: true},
            {data: "created_at", width: "100px", className: "", orderable: true},
            {data: "updated_at", width: "100px", className: "", orderable: true},
            {data: "active", width: "100px", className: "text-center", orderable: false},
            {data: "action", width: "100px", className: "text-center", orderable: false},
        ],
        drawCallback: function () {
          $($(".dataTables_wrapper .pagination li:first-of-type"))
            .find("a")
            .addClass("prev");
          $($(".dataTables_wrapper .pagination li:last-of-type"))
            .find("a")
            .addClass("next");
          $(".dataTables_wrapper .pagination").addClass("pagination-sm");
        },
        language: {
          paginate: {
            previous: "<i class='simple-icon-arrow-left'></i>",
            next: "<i class='simple-icon-arrow-right'></i>"
          },
          search: "_INPUT_",
          searchPlaceholder: "ค้นหา...",
          lengthMenu: "แสดง _MENU_ รายการ"
        },
    }).on('draw', function () {
        //$('.tb-check-single').iCheck(iCheckboxOpt);
    }).on('processing', function(e, settings, processing) {
        if ( processing ) {
            $('#overlay-box').removeClass('hidden');
        } else {
            $('#overlay-box').addClass('hidden');
        }
    }).on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
        //$('.tb-check-single').iCheck(iCheckboxOpt);
    });
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
