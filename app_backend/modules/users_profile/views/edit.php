<div class="col-xl-3 col-lg-4">
    <div class="m-portlet m-portlet--full-height  ">
        <div class="m-portlet__body">
            <div class="m-card-profile">
                <div class="m-card-profile__title m--hide">
                    Your Profile
                </div>
                <div class="m-card-profile__pic">
                    <div class="m-card-profile__pic-wrapper">
                        <img src="<?php echo $this->session->users['image'] ?>" alt="" />
                    </div>
                </div>
                <div class="m-card-profile__details">
                    <span class="m-card-profile__name"><?php echo $this->session->users['Name'] ?></span>
                    <a href="" class="m-card-profile__email m-link"><?php echo $this->session->users['email'] ?></a>
                </div>
            </div>
           
        </div>
    </div>
</div>
<div class="col-xl-9 col-lg-8">
    <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
        <div class="m-portlet__head">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                                <i class="flaticon-share m--hide"></i>
                                ข้อมูลทั่วไป
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_user_profile_tab_2" role="tab">
                                เปลี่ยนรหัสผ่าน
                            </a>
                        </li>
                        
                    </ul>
                </div>
        </div>

       
       
        <div class="tab-content">
            <div class="tab-pane active" id="m_user_profile_tab_1">
                 <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-edit', 'method' => 'post')) ?>
                     
                
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" >อีเมล์</label>
                    <div class="col-sm-7">
                        <input value="<?php echo $info->email ?>" type="email" id="input-email" class="form-control" name="email" required >
                    </div>
                </div>
                
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" >ชื่อ</label>
                    <div class="col-sm-7">
                        <input value="<?php echo $info->fname ?>" type="text" id="input-username" class="form-control" name="fname" required>
                    </div>
                </div>  

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" >นามสกุล</label>
                    <div class="col-sm-7">
                        <input value="<?php echo $info->lname ?>" type="text" id="input-username" class="form-control" name="lname">
                    </div>
                </div> 
               
                
               <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" >ชื่อผู้ใช้งาน</label>
                    <div class="col-sm-7">
                        <input value="<?php echo $info->username ?>" type="text" id="input-username" class="form-control" name="username" disabled>
                    </div>
                </div>  
                
                   
                 
           
                <div class="m-portlet__foot m-portlet__foot--fit">

                    <div class="m-form__actions">
                        <div class="row">
                            <div class="col-2">
                            </div>
                            <div class="col-10">
                                <button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">บันทึก</button>
                        <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                            </div>
                        </div>
                        
                    </div>
                </div>
                 <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
                 <input type="hidden" class="form-control" name="db" id="db" value="repo">
                 <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->user_id) ? encode_id($info->user_id) : 0 ?>">
                <?php echo form_close() ?>
            </div>
            <div class="tab-pane " id="m_user_profile_tab_2">
            <?php echo form_open_multipart($frmActionPassword, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-change-password', 'method' => 'post')) ?>
                <div class="m-portlet__body">
                    
                   
                    <!-- <div class="form-group m-form__group row">
                        <label class="col-sm-2 col-form-label"><h4 class="box-title">เปลี่ยนรหัสผ่าน</h4></label>
                        <div class="col-sm-7">
                            
                        </div>
                    </div> -->
                    <div class="form-group m-form__group row">
                        <label class="col-sm-2 col-form-label">รหัสผ่านปัจจุบัน</label>
                        <div class="col-sm-7">
                            <input value="" type="password" id="input-oldPassword" class="form-control" name="oldPassword" required>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-sm-2 col-form-label">รหัสผ่านใหม่</label>
                        <div class="col-sm-7">
                            <input value="" type="password" id="input-password" class="form-control" name="password" required>
                        </div>
                    </div>
                    
                    <div class="form-group m-form__group row">
                        <label class="col-sm-2 col-form-label">ยืนยันรหัสผ่านใหม่</label>
                        <div class="col-sm-7">
                            <input value="" type="password" id="input-repassword" class="form-control" name="rePassword">
                        </div>
                    </div>   
                     
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">

                    <div class="m-form__actions">
                        <div class="row">
                            <div class="col-2">
                            </div>
                            <div class="col-10">
                                <button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">บันทึก</button>
                        <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                            </div>
                        </div>
                        
                    </div>
                </div>
                 <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
                 <input type="hidden" class="form-control" name="db" id="db" value="repo">
                 <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->user_id) ? encode_id($info->user_id) : 0 ?>">
            <?php echo form_close() ?>

            </div>
        </div>
    </div>


    
</div>

<script>
    //set par fileinput;
    var required_icon   = true; 
    var file_image      = '<?=(isset($info->file)) ? $this->config->item('root_url').$info->file : ''; ?>';
    var file_id         = '<?=$info->user_id;?>';
    var deleteUrl       = '<?=base_url();?><?=$this->router->class;?>/deletefile/<?=$info->user_id;?>';

</script>




