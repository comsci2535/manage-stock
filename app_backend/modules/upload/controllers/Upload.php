<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends MX_Controller {
    
    
    public function __construct() {
        parent::__construct();
        $this->load->model("upload_m");
        $this->load->library('image_moo');
        
        
    }
    
    public function update_content($param, $lang=false){
        //print"<pre>";print_r($param);exit();
        if ( empty($param) )
           return $param;
        $result = $this->upload_m->delete_upload_content($param, $lang);
        $rows = $this->upload_m->insert_upload_content($param);
        return $rows;
    }
    
    public function get_upload($contentId="", $grpContent="", $grpType=""){
        $param['contentId'] = $contentId;
        $param['grpContent'] = $grpContent;
        $param['grpType'] = $grpType;
        $info = $this->upload_m->get_upload($param);
        return $info;
    }
    
    public function get_upload_image($contentId="", $grpContent="", $grpType=""){
        $param['contentId'] = $contentId;
        $param['grpContent'] = $grpContent;
        $param['grpType'] = $grpType;
        $upload = $this->upload_m->get_upload($param);
        $info = new stdClass();
        $info->image = null;
        if ( $upload->num_rows() != 0 ) {
            $row = $upload->row();
            $info->uploadId = $row->uploadId;
            if ( is_file("{$row->path}/{$row->filename}") ) {
                $info->thumbnail = base_url("{$row->path}thumbnail/{$row->filename}");
                $info->image = base_url("{$row->path}{$row->filename}");
            }
        }
        return $info;
    }    
    public function get_upload_tmpl($contentId="", $grpContent="", $grpType="", $lang=false){
        $param['contentId'] = $contentId;
        $param['grpContent'] = $grpContent;
        $param['grpType'] = $grpType;
        $param['lang'] = $lang;
        $info = $this->upload_m->get_upload($param);
        $data = new stdClass();
        if ( $info->num_rows() != 0) {
            foreach ( $info->result() as $key => $row ) {
                if (is_file("{$row->path}/{$row->filename}")) {
                    if ( strpos($row->mime,'image') !==  FALSE ) {
                        $url = base_url("{$row->path}{$row->filename}");
                        $thumb = base_url("{$row->path}thumbnail/{$row->filename}");
                    } else {
                        $url = base_url("{$row->path}{$row->filename}");
                        $thumb = base_url("assets/images/extension/{$row->extension}.png");
                    }
                    $data->{$key} = (object) array(
                        'uploadId' => $row->uploadId,
                        'thumbnailUrl' => $thumb,
                        'url' => $url,
                        'title' => $row->title,
                        'lang' => $row->lang
                    );
                }
            }
        }
        return $data;
    }

    public function get_upload_tmpl2($contentId="", $grpContent="", $grpType="", $lang=false){
        $param['contentId'] = $contentId;
        $param['grpContent'] = $grpContent;
        $param['grpType'] = $grpType;
        $param['lang'] = $lang;
        $info = $this->upload_m->get_upload($param);
        $data = new stdClass();
        if ( $info->num_rows() != 0) {
            foreach ( $info->result() as $key => $row ) {
                if (is_file("{$row->path}/{$row->filename}")) {
                    if ( strpos($row->mime,'image') !==  FALSE ) {
                        $url = base_url("{$row->path}{$row->filename}");
                        $thumb = base_url("{$row->path}{$row->filename}");
                    } else {
                        $url = base_url("{$row->path}{$row->filename}");
                        $thumb = base_url("assets/images/extension/{$row->extension}.png");
                    }
                    $data->{$key} = (object) array(
                        'uploadId' => $row->uploadId,
                        'thumbnailUrl' => $thumb,
                        'url' => $url,
                        'title' => $row->title,
                        'lang' => $row->lang
                    );
                }
            }
        }
        return $data;
    }
    
    public function clean() {
        $temp = array();
        $info = $this->db
                        ->select('uploadId')
                        ->get('upload_content')
                        ->result_array();
        if ( empty($info)) return false;
        
        foreach ( $info as $rs ) 
            $temp[] = $rs['uploadId'];
        $info = $this->db
                        ->select('*')
                        ->where_not_in('uploadId', $temp)
                        ->get('upload');
        if ( $info->num_rows() > 0 ) {

            foreach ( $info->result() as $row ){
                $filename = $row->path.'/'.$row->filename;
                echo "Delete: $filename <br/>";
                @unlink($filename);
                $filename = $row->path.'/thumbnail/'.$row->filename;
                @unlink($filename);
            }
            $this->db
                    ->where_not_in('uploadId', $temp)
                    ->delete('upload');
        }
        echo "Done!";
    }

    public function clean2() {
        $temp = array();
        $info = $this->db
                        ->select('uploadId')
                        ->get('upload_content')
                        ->result_array();
        if ( empty($info)) return false;
        
        foreach ( $info as $rs ) 
            $temp[] = $rs['uploadId'];
        $info = $this->db
                        ->select('*')
                        ->where_not_in('uploadId', $temp)
                        ->get('upload');
        if ( $info->num_rows() > 0 ) {

            foreach ( $info->result() as $row ){
                $filename = $row->path.'/'.$row->filename;
                //echo "Delete: $filename <br/>";
                @unlink($filename);
                $filename = $row->path.'/thumbnail/'.$row->filename;
                @unlink($filename);
                $filename = $row->path.'/seo/'.$row->filename;
                @unlink($filename);
            }
            $this->db
                    ->where_not_in('uploadId', $temp)
                    ->delete('upload');
        }
        //echo "Done!";
    }
    
    public function modal($module="", $type="") {
        $data['module'] = $module;
        $data['type'] = $type;
        return $this->load->view('upload/modal', $data);
    }
    
    public function modal_gallery($module="", $type="") {
        $data['module'] = $module;
        $data['type'] = $type;
        return $this->load->view('upload/modal_gallery', $data);
    }
    
    public function modal_crop() {
        return $this->load->view('upload/modal_crop');
    }
    
    private function _buleimp_option($module, $type)
    {
        $ym = 'uploads/'.date('Y').'/'.date('m').'/';
        create_dir($ym);
        if($module=='banner'){
            $image_versions = array(
                '' => array('auto_orient' => true),
                'thumbnail' => array('crop' => true, 'max_width' => 960, 'max_height' => 470, 'jpeg_quality' => 90, 'no_cache' => true),    
            ); 

        }elseif($module=='article'){
            $image_versions = array(
                '' => array('crop' => true, 'max_width' => 748, 'max_height' => 364, 'jpeg_quality' => 85, 'no_cache' => true),
                'thumbnail' => array('crop' => true, 'max_width' => 90, 'max_height' => 70, 'jpeg_quality' => 85, 'no_cache' => true),
                'seo' => array('crop' => true, 'max_width' => 600, 'max_height' => 338, 'jpeg_quality' => 85, 'no_cache' => true),
             
                
            ); 
        }else{
            $image_versions = array(
                '' => array('auto_orient' => true),
                'thumbnail' => array('crop' => true, 'max_width' => 120, 'max_height' => 120, 'jpeg_quality' => 85, 'no_cache' => true),
                // 'mini' => array('crop' => true, 'max_width' => 535, 'max_height' => 400, 'jpeg_quality' => 90, 'no_cache' => true),
                
            );  
        }  
        $options = array();
        $options['encrypt_name'] = TRUE;
        $options['upload_dir'] = $ym;
        $options['image_versions'] = $image_versions;
        $options['accept_file_types'] = "/\.(gif|jpe?g|png|rar|zip|txt|xls?x|doc?x|ppt?x|pdf|mp4)$/i";
        $options['module'] = $module;
        $options['type'] = $type;
        return $options;
    }
    
    public function blueimp($module="", $type=""){

        $options = $this->_buleimp_option($module, $type);
        $uploadHandler = new UploadHandler($options, FALSE);
        switch ($this->input->method(TRUE)) {
//            case 'OPTIONS':
//            case 'HEAD':
//                $this->head();
//                break;
            case 'GET':
                    //$images = $uploadHandler->get();
                    $images = array();
                break;
            case 'PATCH':
            case 'PUT':
            case 'POST':
                    $images = $uploadHandler->post();
                    $images = $this->_post($images, $module, $type);
                break;
            case 'DELETE':
                $images = $uploadHandler->delete();
                break;
//            default:
//                //$this->header('HTTP/1.1 405 Method Not Allowed');
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($images)); 
    }
    
    private function _post($images, $module, $type)
    {
       $image = $images['files'][0];
       //imagejpeg($tn, $save, 75) ; 
       $path = 'uploads/'.date('Y').'/'.date('m').'/';
       $private = $this->input->post('private') ? $this->input->post('private') : 0; 
       if ( $image->size > 0  ) {
            $fileInfo = pathinfo($path.$image->name);
            $value['filename'] = $image->name;
            $value['path'] = $path;
            $value['size'] = $image->size;
            $value['mime'] = $image->type;  
            $value['private'] = $private; 
            $value['extension'] = $fileInfo['extension'];
            $value['createDate'] = db_datetime_now();
            $value['createBy'] = $this->session->users['user_id'];
            $value['grpContent'] = $module;
            $id = $this->upload_m->insert($value);
            if ( $id  && FALSE ) {
                foreach ($this->backend->lang as $key => $lang ) {
                    $langValue[$key]['libraryId'] = $id;
                    $langValue[$key]['langId'] = $key;
                    $langValue[$key]['title'] = $fileInfo['filename'];
                    $langValue[$key]['alt'] = $fileInfo['filename'];
                }
                $this->library_m->add_lang($langValue);
                $images['files'][0]->id = $id;
            }
            if ( strpos($image->type,'image') !==  FALSE) {
                $images['files'][0]->url = base_url($path.$image->name);
                $images['files'][0]->thumbnailUrl = base_url($path.'thumbnail/'.$image->name);
                $images['files'][0]->deleteUrl = base_url('admin/upload/json?file='.$image->name);
            } else {
                $images['files'][0]->thumbnailUrl = base_url("assets/images/extension/{$value['extension']}.png");
                $images['files'][0]->deleteUrl = base_url('admin/upload/json?file='.$image->name);
            }
            $images['files'][0]->uploadId = $id;
            $images['files'][0]->private = $private == 1 ? 1 : NULL; 

            $this->compress_img($module,$image->name);
       }
       return $images;
    }

    public function compress_img($module,$file)
    {
        $this->load->library('Compress');
        $path = 'uploads/'.date('Y').'/'.date('m').'/';
        if($module=='banner'){
           

            $file0 = base_url().$path.'thumbnail/'.$file; // file that you wanna compress
            $new_name_image0 = 'issue_resized'; // name of new file compressed
            $quality0 = 85; // Value that I chose
            $destination0 = base_url().$path.'thumbnail/'.$file; // This destination must be exist on your project

            $compress0 = new Compress();

            $compress0->file_url = $file0;
            $compress0->new_name_image = $new_name_image0;
            $compress0->quality = $quality0;
            $compress0->destination = $destination0;

            $file1 = base_url().$path.$file; // file that you wanna compress
            $new_name_image1 = 'issue_resized'; // name of new file compressed
            $quality1 = 85; // Value that I chose
            $destination1 = base_url().$path.$file; // This destination must be exist on your project

            $compress1 = new Compress();

            $compress1->file_url = $file1;
            $compress1->new_name_image = $new_name_image1;
            $compress1->quality = $quality1;
            $compress1->destination = $destination1;

        }else{

            $file0 = base_url().$path.'thumbnail/'.$file; // file that you wanna compress
            $new_name_image0 = 'issue_resized'; // name of new file compressed
            $quality0 = 85; // Value that I chose
            $destination0 = base_url().$path.'thumbnail/'.$file; // This destination must be exist on your project

            $compress0 = new Compress();

            $compress0->file_url = $file0;
            $compress0->new_name_image = $new_name_image0;
            $compress0->quality = $quality0;
            $compress0->destination = $destination0;

            $file1 = base_url().$path.$file; // file that you wanna compress
            $new_name_image1 = 'issue_resized'; // name of new file compressed
            $quality1 = 85; // Value that I chose
            $destination1 = base_url().$path.$file; // This destination must be exist on your project

            $compress1 = new Compress();

            $compress1->file_url = $file1;
            $compress1->new_name_image = $new_name_image1;
            $compress1->quality = $quality1;
            $compress1->destination = $destination1;

            $file2 = base_url().$path.'seo/'.$file; // file that you wanna compress
            $new_name_image2 = 'issue_resized'; // name of new file compressed
            $quality2 = 85; // Value that I chose
            $destination2 = base_url().$path.'thumbnail/'.$file; // This destination must be exist on your project

            $compress2 = new Compress();

            $compress2->file_url = $file2;
            $compress2->new_name_image = $new_name_image2;
            $compress2->quality = $quality2;
            $compress2->destination = $destination2;
            
                
            
        }
        

        

        
    }
    
    public function cropper()
    {
        $input = $this->input->post();
        $info = $this->db
                        ->where('uploadId', $input['uploadId'])
                        ->get('upload')
                        ->row_array();
        $path = $info['path'].$info['filename'];
        $crop = $input['cropData'];
        foreach ($crop as &$rs)
            //$rs = ceil($rs);
        $top = $crop['x'];
        $left = $crop['y'];
        $bottom = $crop['x'] + $crop['width'];
        $right = $crop['y'] + $crop['height'];
        $handle = new Image_moo();
        $handle->load($path)
                ->crop($top, $left, $bottom, $right)
                ->set_jpeg_quality(100)
                ->save($path, TRUE);
        $handle->load($path)
                ->resize_crop(120,120)
                ->set_jpeg_quality(85)
                ->save($info['path'].'/thumbnail/'.$info['filename'], TRUE);
        $path = base_url().$info['path'].$info['filename']."?v=".uniqid();
        $pathMini = base_url().$info['path'].'/thumbnail/'.$info['filename']."?v=".uniqid();
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('url' => $path,'thumbnailUrl' => $pathMini, 'uploadId'=>$input['uploadId'])));
        
    }
    
    public function get_image()
    {
        $input = $this->input->get();
        $info = $this->db
                        ->where('uploadId', $input['uploadId'])
                        ->get('upload')
                        ->row_array();
        $path = base_url().$info['path'].$info['filename']."?v=".uniqid();
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('url' => $path)));
    }
    public function do_upload_ajax($type="") {
        $config = $this->_get_config($type);
        create_dir($config['upload_path']);
        $this->upload->initialize($config);
        if ( !$this->upload->do_upload('userfile') ) {
            $result['result'] = $this->upload->display_errors();
            $result['success'] = false;
        } else {
            $result = $this->upload->data();
            $result['success'] = true;
            $pathinfo = pathinfo("{$config['upload_path']}{$result['file_name']}");
            $result = array_merge($result, $pathinfo);      
            $value = array(
                'filename' => $result['file_name'],
                'path' => $result['dirname'],
                'size' => $result['file_size']*1024,
                'extension' => $result['extension'],
                'mime' => $result['file_type'],
                'createDate' => db_date_now(),
                'createBy' => $this->session->users['user_id']
            );
            $result['insertId'] = $this->upload_m->insert($value);
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));  
    }
    
    public function upload_ci($type="", $inputFileName="userfile")  {
        $config = $this->_get_config($type);
        create_dir($config['upload_path']);
        $this->upload->initialize($config);

        if ( !$this->upload->do_upload($inputFileName) ) {
            $result['result'] = $this->upload->display_errors();
            $result['success'] = false;
        } else {
            $result = $this->upload->data();
            $result['success'] = true;
            $pathinfo = pathinfo("{$config['upload_path']}{$result['file_name']}");
            $result = array_merge($result, $pathinfo);      
            $value = array(
                'grpContent' => $type,
                'filename' => $result['file_name'],
                'path' => $result['dirname'],
                'size' => $result['file_size']*1024,
                'extension' => $result['extension'],
                'mime' => $result['file_type'],
                'createDate' => db_date_now(),
                'createBy' => $this->session->users['user_id']
            );
            $result['insertId'] = $this->upload_m->insert($value);
        }
        return $result;
    }    
    
    private function _get_config($type){
        $config['upload_path'] = 'uploads/';
        $config['max_size'] = file_upload_max_size();
        $config['resize'] = false;
        if ( $type == "user" ) {
            $config['upload_path'] = 'uploads/user/';
            $config['encrypt_name'] = true;
            $config['allowed_types'] = 'gif|jpg|png|jpeg|image/png|mp4';
            $config['resizeOverwrite'] = true;
            $config['resize'] = true;
            $config['width'] = 160;
            $config['height'] = 160;
        }
        if ( $type == "information" ) {
            $config['upload_path'] = 'uploads/information/'.date('Y').'/'.date('m').'/';
            $config['encrypt_name'] = true;
            $config['allowed_types'] = 'xls|pdf|xlsx|gif|jpg|png|jpeg|image/png|mp4';
            $config['resizeOverwrite'] = true;
            $config['resize'] = false;
        }
        return $config;
    }

    public function json()
    {
        $path = 'uploads/'.date('Y').'/'.date('m').'/';
        $options = [
            'script_url' => site_url('upload/json'),
            'upload_dir' => APPPATH . '../../'.$path,
            'upload_url' => site_url($path)
        ];
        $this->load->library('UploadHandler', $options);
    }
    
    

}
