<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Upload_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_upload($param)
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->select('b.*')
                        ->from('upload a')
                        ->join('upload_content b', 'a.uploadId = b.uploadId', 'left')
                        ->get();
        return $query;
    }
    
    public function insert($value) {
        $this->db->insert('upload', $value);
        return $this->db->insert_id();
    }  
    
    public function delete_upload_content($param, $lang){
        $grpType = array();
        $contentId = null;
        $grpContent = null;
        foreach ($param as $rs) {
            if ( !isset($contentId) )
                $contentId = $rs['contentId'];
            if ( !isset($grpContent) )
                $grpContent = $rs['grpContent'];
            $grpType[] = $rs['grpType'];
            
        }
        $grpType = array_unique($grpType);
        // if ( $lang )
        //     $this->db
        //             ->where('lang', $this->curLang);
        $this->db
                ->where('grpContent', $grpContent)
                ->where('contentId', $contentId);
                //->where_in('grpType', $grpType);
        $query = $this->db
                        ->delete('upload_content');
        return $this->db->affected_rows();
    }
    
    private function _condition($param){
        if ( isset($param['lang']) && $param['lang'] )
            $this->db->where('b.lang', $this->curLang);
        if ( isset($param['grpContent']) )
            $this->db->where('b.grpContent', $param['grpContent']);
        if ( isset($param['grpType']) )
            $this->db->where('b.grpType', $param['grpType']);
        if ( isset($param['contentId']) )
            $this->db->where('b.contentId', $param['contentId']);    
        if ( isset($param['uploadId']) )
            $this->db->where('b.uploadId', $param['uploadId']); 
    }
    
    public function insert_upload_content($value){
        //print_r($value[4]);exit();
        foreach ( $value as $key=>$rs) {
            if ( !$rs['uploadId'] )
                unset($value[$key]);
        }
        if ( !empty($value) )
            $this->db->insert_batch('upload_content', $value);
        return $this->db->affected_rows();
    }

}
