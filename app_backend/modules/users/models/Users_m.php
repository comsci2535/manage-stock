<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->from('users a')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('users a')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.fullname', $param['keyword'])
                    ->or_like('a.email', $param['keyword'])
                    ->group_end();
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }
        if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.updated_at,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
        }     
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }         
        // END form filter
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.fullname', $param['search']['value'])
                    ->or_like('a.email', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.fullname";
            if ($param['order'][0]['column'] == 2) $columnOrder = "a.email";            
            if ( $this->router->method =="data_index" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.created_at";
                if ($param['order'][0]['column'] == 4) $columnOrder = "a.updated_at";
            } else if ( $this->router->method =="data_trash" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.recycle_at";
            }
            $this->db
                    ->order_by($columnOrder, $param['order'][0]['dir']);
        } 
        
        if ( isset($param['user_id']) ) 
            $this->db->where('a.user_id', $param['user_id']);

        if ( isset($param['username']) ) 
            $this->db->where('a.username', $param['username']);

        if ( isset($param['recycle']) ){
            if (is_array($param['recycle'])) {
                $this->db->where_in('a.recycle', $param['recycle']);
            } else {
                $this->db->where('a.recycle', $param['recycle']);
            }
        }
            

       // if ( $this->session->users['type'] != 'developer')
        $this->db->where('a.type !=', 'developer');

    }
    
    public function insert($value) {
        $this->db->insert('users', $value);
        return $this->db->insert_id();
    }
    
    public function update($id, $value)
    {
        $query = $this->db
                        ->where('user_id', $id)
                        ->update('users', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('user_id', $id)
                        ->update('users', $value);
        return $query;
    }    

}
