<!DOCTYPE html>

<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="th">

<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>Admin | Sign In</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Sarabun:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <link href="<?=$this->config->item('template'); ?>assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

    <!--end::Web font -->
    <link href="<?=$this->config->item('template'); ?>assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

    <!--end::Page Vendors Styles -->
    <link rel="shortcut icon" href="<?=$this->config->item('template'); ?>assets/demo/default/media/img/logo/favicon.ico" />
    <style type="text/css">
        .m-login.m-login--2 .m-login__wrapper .m-login__container .m-login__form .m-form__group .form-control{
            border-radius: unset;
        }
    </style>
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-2" id="m_login" style="background-image: url(<?=$this->config->item('template'); ?>assets/app/media/img//bg/bg-3.jpg);">
            <div class="m-grid__item m-grid__item--fluid m-login__wrapper">
                <div class="m-login__container">
                    <div class="m-login__logo">
                        <a href="#">
                            <img width="100" src="<?=$this->config->item('template'); ?>assets/app/media/img/logos/stock-logo.jpg">
                        </a>
                    </div>
                    <div class="m-login__signin">
                        <div class="m-login__head">
                            <h3 class="m-login__title">Sign In To Admin</h3>
                        </div>
                        <?php if($this->session->flashdata('status') === 'error'):?>
                            <div class="m-alert m-alert--icon m-alert--air alert alert-danger alert-dismissible fade show" role="alert">
                                <div class="m-alert__icon">
                                    <i class="la la-warning"></i>
                                </div>
                                <div class="m-alert__text">
                                    <strong>Eroor! </strong><?php echo $this->session->flashdata('message');?>
                                </div>
                                <div class="m-alert__close">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    </button>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if($this->session->flashdata('status') === 'warning'):?>
                            <div class="m-alert m-alert--icon m-alert--air alert alert-warning alert-dismissible fade show" role="alert">
                                <div class="m-alert__icon">
                                    <i class="la la-warning"></i>
                                </div>
                                <div class="m-alert__text">
                                    <strong>Eroor! </strong><?php echo $this->session->flashdata('message');?>
                                </div>
                                <div class="m-alert__close">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    </button>
                                </div>
                            </div>
                        <?php endif; ?>
                        <!--   <form id="sign_in" method="POST" action="<?=base_url('users/login')?>"> -->
                           <?php echo form_open(base_url('users/login'), 'id="frm-login" class="m-login__form m-form"'); ?>
                           <div class="form-group m-form__group">
                            <input class="form-control m-input" type="text" name="username" placeholder="Username" autocomplete="off" required>
                        </div>
                        <div class="form-group m-form__group">
                            <input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password" required>
                        </div>
                        <div class="row m-login__form-sub">
                            <div class="col m--align-left m-login__form-left">
                                <label class="m-checkbox  m-checkbox--focus">
                                    <input type="checkbox" name="remember" value="1" id="rememberme"> Remember me
                                    <span></span>
                                </label>
                            </div>

                        </div>
                        
                        <div class="m-login__form-action">
                            <button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn m-login__btn--primary">Sign In</button>
                        </div>
                        <?php echo form_close(); ?>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- end:: Page -->



    <!--end:: Global Optional Vendors -->

    <!--begin::Global Theme Bundle -->
    <script src="<?=$this->config->item('template'); ?>assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

    <script src="<?=$this->config->item('template'); ?>assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

    <!--end::Global Theme Bundle -->
    <script type="text/javascript">
     $('#frm-login').validate()
     window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove(); 
        });
    }, 4000);
</script>


<!--end::Page Scripts -->
</body>

<!-- end::Body -->
</html>