<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from dore-jquery.coloredstrategies.com/Dashboard.Default.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 02:40:09 GMT -->
<head>
    <meta charset="UTF-8">
    <title><?php echo $pageTitle; ?></title>
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <link rel="stylesheet" href="<?=$this->config->item('template'); ?>font/iconsmind-s/css/iconsminds.css">
    <link rel="stylesheet" href="<?=$this->config->item('template'); ?>font/simple-line-icons/css/simple-line-icons.css">
    <link href="<?=$this->config->item('template'); ?>font/fontawesome5/css/all.min.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="<?=$this->config->item('template'); ?>css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="<?=$this->config->item('template'); ?>css/vendor/fullcalendar.min.css">
    <link rel="stylesheet" href="<?=$this->config->item('template'); ?>css/vendor/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?=$this->config->item('template'); ?>css/vendor/datatables.responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="<?=$this->config->item('template'); ?>css/vendor/select2.min.css">
    <link rel="stylesheet" href="<?=$this->config->item('template'); ?>css/vendor/select2-bootstrap.min.css">
    <link rel="stylesheet" href="<?=$this->config->item('template'); ?>css/vendor/component-custom-switch.min.css">
    <link rel="stylesheet" href="<?=$this->config->item('template'); ?>css/vendor/perfect-scrollbar.css">
    <link rel="stylesheet" href="<?=$this->config->item('template'); ?>css/vendor/owl.carousel.min.css">
    <link rel="stylesheet" href="<?=$this->config->item('template'); ?>css/vendor/bootstrap-stars.css">
    <link rel="stylesheet" href="<?=$this->config->item('template'); ?>css/vendor/nouislider.min.css">
    <link rel="stylesheet" href="<?=$this->config->item('template'); ?>css/vendor/bootstrap-datepicker3.min.css">
    <link rel="stylesheet" href="<?=$this->config->item('template'); ?>css/main.css">

   
     <link rel="stylesheet" href="<?=$this->config->item('assets'); ?>plugins/bootstrap-toggle-master/css/bootstrap-toggle.css">
    <link href="<?=$this->config->item('assets'); ?>plugins/icheck/skins/square/grey.css" rel="stylesheet" type="text/css"/>
    <link href="<?=$this->config->item('assets'); ?>plugins/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=$this->config->item('assets'); ?>plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
    <?php if (in_array($this->router->method, array('order'))) : ?>  
        <link href="<?=$this->config->item('assets'); ?>plugins/nestable2/jquery.nestable.css" rel="stylesheet" type="text/css"/>
    <?php endif; ?>
    
</head>

<body id="app-container" class="menu-default show-spinner">
    <?php $this->load->view('header'); ?>
    <?php $this->load->view('sidebar'); ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                
                <?php $this->load->view('breadcrumb'); ?> 
                <?php $this->load->view($contentView); ?>
                
            </div>
        </div>
    </main>

     <script type="text/javascript">
        var appName = "<?=config_item('appName');?>";
        var csrfToken = get_cookie('csrfCookie');
        var siteUrl = "<?php echo $this->config->item('root_url'); ?>";
        var baseUrl = "<?php echo $this->config->item('root_url'); ?>";
        var root_url = "<?php echo $this->config->item('root_url'); ?>";
        var controller = "<?php echo $this->router->class ?>";
        var method = "<?php echo $this->router->method ?>";
        function get_cookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ')
                    c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0)
                    return c.substring(nameEQ.length, c.length);
            }
            return null;
        }
    </script>
    
    <script src="<?=$this->config->item('template'); ?>js/vendor/jquery-3.3.1.min.js"></script>
    <script src="<?=$this->config->item('template'); ?>js/vendor/bootstrap.bundle.min.js"></script>
    <script src="<?=$this->config->item('template'); ?>js/vendor/Chart.bundle.min.js"></script>
    <script src="<?=$this->config->item('template'); ?>js/vendor/chartjs-plugin-datalabels.js"></script>
    <script src="<?=$this->config->item('template'); ?>js/vendor/moment.min.js"></script>
    <script src="<?=$this->config->item('template'); ?>js/vendor/fullcalendar.min.js"></script>
    <script src="<?=$this->config->item('template'); ?>js/vendor/datatables.min.js"></script>
    <script src="<?=$this->config->item('template'); ?>js/vendor/perfect-scrollbar.min.js"></script>
    <script src="<?=$this->config->item('template'); ?>js/vendor/owl.carousel.min.js"></script>
    <script src="<?=$this->config->item('template'); ?>js/vendor/progressbar.min.js"></script>
    <script src="<?=$this->config->item('template'); ?>js/vendor/jquery.barrating.min.js"></script>
    <script src="<?=$this->config->item('template'); ?>js/vendor/select2.full.js"></script>
    <script src="<?=$this->config->item('template'); ?>js/vendor/nouislider.min.js"></script>
    <script src="<?=$this->config->item('template'); ?>js/vendor/bootstrap-datepicker.js"></script>
    <script src="<?=$this->config->item('template'); ?>js/vendor/Sortable.js"></script>
    <script src="<?=$this->config->item('template'); ?>js/vendor/mousetrap.min.js"></script>
    <script src="<?=$this->config->item('template'); ?>js/vendor/bootstrap-notify.min.js"></script>
    <script src="<?=$this->config->item('template'); ?>js/dore.script.js"></script>
    <script src="<?=$this->config->item('template'); ?>js/scripts.js"></script>

    <!--tari Scripts -->
    <script src="<?php echo $this->config->item('assets') ?>plugins/jquery.form.min.js"></script>
    <script src="<?php echo $this->config->item('assets') ?>plugins/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="<?php echo $this->config->item('assets') ?>plugins/jquery-validation/dist/localization/messages_th.js"></script>  
    <script src="<?php echo $this->config->item('assets') ?>plugins/jQueryUI/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->config->item('assets') ?>plugins/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>

     <script src="<?=$this->config->item('assets'); ?>plugins/bootstrap-toggle-master/js/bootstrap-toggle.min.js"></script>
    <script src="<?=$this->config->item('assets'); ?>plugins/icheck/icheck.min.js" type="text/javascript"></script>
    <script src="<?=$this->config->item('assets'); ?>plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>

    <?php if (in_array($this->router->method, array('order'))) : ?>  
        <script src="<?php echo $this->config->item('assets') ?>plugins/nestable2/jquery.nestable.js" type="text/javascript"></script>
    <?php endif; ?>

    <script src="<?php echo $this->config->item('scripts') ?>app.js?v=<?php echo rand(0,100) ?>"></script>

    <?php  echo $pageScript; ?>

    <script>
    $(document).ready(function () {
        <?php if ($this->session->toastr) : ?>
            setTimeout(function () {
                showNotificationCustom('<?php echo $this->session->toastr['lineOne']; ?>','<?php echo $this->session->toastr['lineTwo']; ?>','top','right','primary');
            }, 500);
            <?php $this->session->unset_userdata('toastr'); ?>
        <?php endif; ?>
    });

    function showNotificationCustom(title,massage,placementFrom, placementAlign, type ) {
      $.notify(
        {
          title: title,
          message: massage,
          target: "_blank"
        },
        {
          element: "body",
          position: null,
          type: type,
          allow_dismiss: true,
          newest_on_top: false,
          showProgressbar: false,
          placement: {
            from: placementFrom,
            align: placementAlign
          },
          offset: 20,
          spacing: 10,
          z_index: 1031,
          delay: 4000,
          timer: 2000,
          url_target: "_blank",
          mouse_over: null,
          animate: {
            enter: "animated fadeInDown",
            exit: "animated fadeOutUp"
          },
          onShow: null,
          onShown: null,
          onClose: null,
          onClosed: null,
          icon_type: "class",
          template:
            '<div data-notify="container" class="col-11 col-sm-3 alert  alert-{0} " role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            '<span data-notify="icon"></span> ' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            "</div>" +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
            "</div>"
        }
      );
    }
    </script>

</body>


<!-- Mirrored from dore-jquery.coloredstrategies.com/Dashboard.Default.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 02:40:38 GMT -->
</html>