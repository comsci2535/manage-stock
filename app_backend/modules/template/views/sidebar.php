<div class="sidebar">
    <div class="main-menu">
        <div class="scroll">
            <ul class="list-unstyled">
                <?php foreach ($sidebar as $level0) : ?>
                    <?php if (!isset($level0['children'])) : ?>
                        <li class="<?php echo $level0['active'] ? "active" : null; ?>">
                            <a  href="<?php echo $level0['href'] ?>" <?php if($level0['target']!=""){ echo 'target="_blank"'; } ?>>
                                <i class="<?php echo $level0['icon'] ?>"></i>
                                <span><?php echo $level0['title'] ?></span>
                            </a>
                        </li>
                    <?php else : ?>
                        <li class="<?php echo $level0['active'] ? "active" : null; ?>">
                            <a  href="#parent<?php echo $level0['moduleId'] ?>" >
                                <i class="<?php echo $level0['icon'] ?>"></i>
                                <span><?php echo $level0['title'] ?></span>
                            </a>
                        </li>
                     <?php endif; ?>
                <?php endforeach; ?>
              
            </ul>
        </div>
    </div>

    <div class="sub-menu">
        <div class="scroll">
            <?php foreach ($sidebar as $level0) : ?>
                <?php if (isset($level0['children'])) : ?>
                    <ul class="list-unstyled" data-link="parent<?php echo $level0['moduleId'] ?>">
                        <?php foreach ($level0['children'] as $level1) : ?>
                        <li class="<?php echo $level1['active'] ? "active" : null; ?>">
                            <a href="<?php echo $level1['href'] ?>" <?php if($level1['target']!=""){ echo 'target="_blank"'; } ?>>
                                <i class="<?php echo $level1['icon'] ?>"></i> <?php echo $level1['title'] ?>
                            </a>
                        </li>
                         <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>