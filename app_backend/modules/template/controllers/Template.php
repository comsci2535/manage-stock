<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Template extends MX_Controller {
    
//    public $language = array("th" => "ไทย", "en" => "English");
    public $language = array("th" => "ไทย");
    public $curLang;

    public function __construct() {
        parent::__construct();
    }
    
    public function layout($data) {

        if (ENVIRONMENT == 'development')
            $this->output->enable_profiler(FALSE);

        if(empty($this->session->users['user_id'])){
             redirect(site_url('login'));
        }
        
        $this->breadcrumbs->push('แผงควบคุม', site_url("dashboard"));
        if (isset($data['breadcrumb']))
            $this->_breadcrumbs($data['breadcrumb']);
        
        if (!isset($data['contentView']))
            $data['contentView'] = 'empty';
        $data['pageTitle'] = "Admin";
        if (isset($data['pageHeader'])) 
            $data['pageTitle'] = $data['pageTitle'] . " | " . $data['pageHeader'];
        
        if ( !isset($data['pageScript']) ) {
             $data['pageScript'] = $this->_page_script();
        } else {
            $data['pageScript'] = '<script src="' . base_url($data['pageScript']) . '?v='.rand(0,100).'"></script>';
        }

       // arr($data['pageScript']);exit();
        
        $data['sidebar'] = modules::run('sidebar/gen');
        //arr($data['sidebar']);exit();
        $data['curLang'] = config_item('language_abbr');
        $data['language'] = $this->language;
        
        $this->load->view('layout', $data);
    }

    private function _page_script() {
        $file = $this->router->method;
        if ( in_array($this->router->method,array('create','edit')) ) 
            $file = 'form';

        $script = "assets/backend/scripts/{$this->router->class}/{$file}.js";
      
        if ( is_file($script) ) {
            $pageScript = '<script src="'.$this->config->item('root_url').$script.'?v='.rand(0,100).'"></script>';
        } else {
            $pageScript = '<!-- page no script -->';
        }
       
        //print_r($pageScript);exit();
        return $pageScript;
    }

    private function _set_config() {
        $query = Modules::run('config/get_config', 'general');
        $lang = config_item('language_abbr');
        $lang = strtoupper($lang);
        foreach ($query as $rs)
            $this->config->set_item($rs['variable'], $rs['value']);
        return true;
    }

    private function _breadcrumbs($breadcurmbs) {
        if ( empty($breadcurmbs) )
            return;
        foreach ($breadcurmbs as $rs) {
            $this->breadcrumbs->push($rs[0], $rs[1]);
        }
        return;
    }

}
