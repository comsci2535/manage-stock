<div class="col-xl-12 col-lg-12 mb-4 filter">
    <div class="card">
        <div class="card-body ">
           <form id="frm-filter" role="form" class="row">
                <div class="col-md-2 form-group">
                    <label for="startRang">วันที่สร้าง</label>
                    <input type="text" name="createDateRange" class="form-control" value="" />
                    <input type="hidden" name="createStartDate" value="" />
                    <input type="hidden" name="createEndDate" value="" />
                </div>    
                <div class="col-md-2 form-group">
                    <label for="startRang">วันที่แก้ไข</label>
                    <input type="text" name="updateDateRange" class="form-control" value="" />
                    <input type="hidden" name="updateStartDate" value="" />
                    <input type="hidden" name="updateEndDate" value="" />
                </div>  
                <div class="col-md-2 form-group">
                    <label for="active">สถานะ</label>
                    <?php $activeDD = array(""=>'ทั้งหมด', 1=>'เปิด', 0=>'ปิด') ?>
                    <?php echo form_dropdown('active', $activeDD, null, 'class="from-control select2-single"') ?>
                </div> 
                
                <div class="col-md-4 form-group">
                    <label for="keyword">คำค้นหา</label>
                    <input class="form-control" name="keyword" type="text">
                </div>           
                <div class="col-md-1 form-group">
                    <label for="keyword"></label>
                    <button type="button" class="btn btn-primary default btn-filter"><i class="fa fa-filter"></i> ค้นหา</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="col-xl-12 col-lg-12 mb-4">
    <div class="card">
        <div class="card-body">
           <!--  <h5 class="card-title">รายการ</h5> -->
        
            <table id="data-list" class="data-table  responsive " style="width:100%">
                <thead>
                    <tr>
                        <th>
                            <!-- <input name="tbCheckAll" type="checkbox" class="icheck tb-check-all"> -->
                            <div class="custom-control custom-checkbox tb-check-all">
                                <input type="checkbox" class="custom-control-input check-all" id="customCheckThisAll">
                                <label class="custom-control-label" for="customCheckThisAll"></label>
                            </div>
                        </th>
                       <th>รายการ</th>
                        <th>เนื้อหาย่อ</th>
                        <th>สร้าง</th>
                        <th>แก้ไข</th>
                        <th>สถานะ</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
