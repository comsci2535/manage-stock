<style type="text/css">
    .hidden {
        display: none!important;
    }
    .policy tr.separator {background-color:#e6eeff;font-weight: bold}
</style>

<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>

       
        <?php echo form_open($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main', 'method' => 'post')) ?>
            <div class="m-portlet__body">
                    
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title">กลุ่มผู้ดูแล</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->name) ? $info->name : NULL ?>" type="text" class="form-control" name="name" required>
                    </div>
                </div>      
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title">อธิบาย</label>
                    <div class="col-sm-7">
                        <textarea name="remark" rows="3" class="form-control"><?php echo isset($info->remark) ? $info->remark : NULL ?></textarea>
                    </div>
                </div>  
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title">สิทธิ</label>
                    <div class="col-sm-7">
                        <table class="table table-bordered policy" width="100%">
                            <thead>
                                <tr>
                                    <th class="text-left" width="70%">รายการ</th>
                                    <th class="text-center">เข้าถึง</th>
                                    <th class="text-center">แก้ไข</th>
                                    <th class="text-center">ทั้งหมด</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($module as $level0) : ?>
                                    <?php if ( $level0['type'] == 2) : ?>
                                    <tr id="<?php echo $level0['moduleId'] ?>" class="<?php echo $level0['type'] == 2 ? "separator" : NULL ?>">
                                        <td class="text-center"><?php echo $level0['title'] ?></td>
                                        <td class="text-center">
                                            <input <?php echo isset($policy->{$level0['moduleId']}->{'1'}) ? "checked" : NULL; ?> type="checkbox" name="policy[<?php echo $level0['moduleId'] ?>][1]" class="icheck access"></td>
                                        <td class="text-left" colspan="2"></td>
                                    </tr>
                                    <?php else : ?>
                                    <tr id="<?php echo $level0['moduleId'] ?>" class="">
                                        <td <?php echo isset($level0['children']) ? "colspan='4' style='background-color:#eee'" : null; ?>><?php echo $level0['title'] ?></td>
                                        <?php if (!isset($level0['children'])) : ?>  
                                            <?php if ( $level0['type'] != 2 ) : ?>
                                            <td class="text-center">
                                                <input <?php echo isset($policy->{$level0['moduleId']}->{'1'}) ? "checked" : NULL; ?> type="checkbox" name="policy[<?php echo $level0['moduleId'] ?>][1]" class="icheck access"></td>
                                            <td class="text-center">
                                                <input <?php echo isset($policy->{$level0['moduleId']}->{'2'}) ? "checked" : NULL; ?> type="checkbox" name="policy[<?php echo $level0['moduleId'] ?>][2]" class="icheck modify"></td>
                                            <td class="text-center"><input type="checkbox" class="icheck check-all"></td>
                                            <?php endif; ?>
                                        <?php endif; ?>                            
                                    </tr>
                                    <?php if (isset($level0['children'])) : ?>
                                        <?php foreach ($level0['children'] as $level1) : ?>
                                            <tr id="<?php echo $level1['moduleId'] ?>">
                                                <td <?php echo isset($level1['children']) ? "colspan='4'" : null; ?> style="padding-left: 25px;"><?php echo $level1['title'] ?></td>
                                                <?php if (!isset($level1['children'])) : ?> 
                                                    <td class="text-center">
                                                        <input <?php echo isset($policy->{$level1['moduleId']}->{'1'}) ? "checked" : NULL; ?> type="checkbox" name="policy[<?php echo $level1['moduleId'] ?>][1]" class="icheck access"></td>
                                                    <td class="text-center">
                                                        <input <?php echo isset($policy->{$level1['moduleId']}->{'2'}) ? "checked" : NULL; ?> type="checkbox" name="policy[<?php echo $level1['moduleId'] ?>][2]" class="icheck modify"></td>
                                                    <td class="text-center"><input type="checkbox" class="icheck check-all"></td>
                                                <?php endif; ?>
                                            </tr>  
                                            <?php if (isset($level1['children'])) : ?>
                                                <?php foreach ($level1['children'] as $level2) : ?>
                                                    <tr id="<?php echo $level2['moduleId'] ?>">
                                                        <td <?php echo isset($level2['children']) ? "colspan='4'" : null; ?> style="padding-left: 45px"><?php echo $level2['title'] ?></td>
                                                        <?php if (!isset($level2['children'])) : ?> 
                                                            <td class="text-center">
                                                                <input <?php echo isset($policy->{$level2['moduleId']}->{'1'}) ? "checked" : NULL; ?> type="checkbox" name="policy[<?php echo $level2['moduleId'] ?>][1]" class="icheck access"></td>
                                                            <td class="text-center">
                                                                <input <?php echo isset($policy->{$level2['moduleId']}->{'2'}) ? "checked" : NULL; ?> type="checkbox" name="policy[<?php echo $level2['moduleId'] ?>][2]" class="icheck modify"></td>
                                                            <td class="text-center"><input type="checkbox" class="icheck check-all"></td>
                                                        <?php endif; ?>
                                                    </tr>  
                                                    <?php if (isset($level2['children'])) : ?>
                                                        <?php foreach ($level2['children'] as $level3) : ?>
                                                            <tr id="<?php echo $level3['moduleId'] ?>">
                                                                <td <?php echo isset($level3['children']) ? "colspan='4'" : null; ?> style="padding-left: 65px"><?php echo $level3['title'] ?></td>
                                                                <?php if (!isset($level3['children'])) : ?>
                                                                    <td class="text-center">
                                                                        <input <?php echo isset($policy->{$level3['moduleId']}->{'1'}) ? "checked" : NULL; ?> type="checkbox" name="policy[<?php echo $level3['moduleId'] ?>][1]" class="icheck access"></td>
                                                                    <td class="text-center">
                                                                        <input <?php echo isset($policy->{$level3['moduleId']}->{'2'}) ? "checked" : NULL; ?> type="checkbox" name="policy[<?php echo $level3['moduleId'] ?>][2]" class="icheck modify"></td>
                                                                    <td class="text-center"><input type="checkbox" class="icheck check-all"></td>
                                                                <?php endif; ?>
                                                            </tr>  
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>                                          
                                                <?php endforeach; ?>
                                            <?php endif; ?>                                
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    <?php endif; ?>                        
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>  
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div>
                    
                </div>
            </div>
            <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
            <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->role_id) ? decode_id($info->role_id) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>


    
</div>




