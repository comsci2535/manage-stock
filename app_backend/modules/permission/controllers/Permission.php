<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Permission extends MX_Controller {

    public function __construct() {
        parent::__construct();
         
        $this->load->model('module/module_m');
        $this->load->model('roles/roles_m');
    }

    public function check() {
        
        if ( $this->session->users['type'] == 'developer' || in_array($this->router->class, array('users_profile')) )
            return true;

        $module = $this->module_m->row_by_class($this->router->class);
        $RoleId = $this->session->users['RoleId'];
        $policy_arr=$this->roles_m->get_permission_role($RoleId)->result_array();
        $policy_ = array();
        if(!empty($policy_arr)){ 
            foreach ($policy_arr as $key => $value) {
                $policy_[$value['moduleId']][] = $value['permission_id'];
            }
        }
        $policy = $policy_;
        
        if (empty($policy) || !isset($policy[$module->moduleId]))
            return false;

       //arr($policy);exit();
        
        $policy = $policy[$module->moduleId];
        $modify = array('create', 'save', 'update', 'action', 'update_password', 'order', 
            'update_order', 'update_v2', 'pdf', 'word', 'excel', 'import', 'export');
        $view= array('index');
        $create= array('create','save');
        $edit= array('edit','update','action','order','update_order','update_v2','restore' );
        $delete= array('destroy','delete','restore');

        if (empty($policy) && in_array($this->router->method, $modify)) {
            
            $permission = false;

        } else {

            if(!in_array(1, $policy) && in_array($this->router->method, $view)){
                $permission = false;
            }else if(!in_array(2, $policy) && in_array($this->router->method, $create)){
                $permission = false;
            }else if(!in_array(3, $policy) && in_array($this->router->method, $edit)){
                $permission = false;
            }else if(!in_array(4, $policy) && in_array($this->router->method, $delete)){
                $permission = false;
            }else{
                $permission = true;
            }
            
        }
        return $permission;
    }

}
