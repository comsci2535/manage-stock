<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>

                <ol class="breadcrumb">
                    <li class="active">Logs</li>
                </ol>
                <!-- <small>Taken from <a href="https://datatables.net/" target="_blank">datatables.net</a></small> -->
            </h2>
        </div>
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            ข้อมูล Log การใช้งาน
                        </h2>
                        
                    </div>
                    <div class="body">
                        <div class="table-responsive">

                            <table class="table table-bordered table-striped table-hover dataTable log-exportable">
                                <thead>
                                    <tr>
                                        <th width="23%">Date</th>
                                        <th width="20%">User</th>
                                        <th width="17%">Ip address</th>
                                        <th width="10%">Class</th>
                                        <th width="10%">Method date</th>
                                        <th width="20%">Messege</th> 
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                     <th>Date</th>
                                     <th>User</th>
                                     <th>Ip address</th>
                                     <th>Class</th>
                                     <th>Method date</th>
                                     <th>Messege</th>
                                 </tr>
                             </tfoot>
                             <tbody>

                                <?php 
                                foreach ($logs as $index => $log): 
                                    ?>
                                    <tr>
                                       <td>
                                        <p style="display: none;"><?=$log->created_at?><p>
                                            <?=DateThai($log->created_at)?>
                                        </td>
                                        <td><?=$log->username?></td>
                                        <td><?=$log->ip_address?></td>
                                        <td><?=$log->class?></td>
                                        <td><?=$log->method?></td>
                                        <td><?=$log->messege?></td>

                                    </tr>

                                    <?php 
                                endforeach;
                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
</div>
</section>


