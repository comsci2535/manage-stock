<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
<script type="text/javascript">

    $('.log-exportable').DataTable({
        dom: 'Bfrtip',
        responsive: true,
        ordering : true,
        "order": [[ 0, "desc" ]],
        "pageLength": 20,
        buttons: [
            //'copy', 'excel', 'pdf',
            'excel', 
            {
                extend: 'print',
                exportOptions: {
                    columns: ':visible'
                },
            },


            ],

        });
    
    var table = $('.log-exportable').DataTable();

    table.on('page.dt', function() {
      $('html, body').animate({
        scrollTop: $(".content").offset().top
    }, 'fate');
  });
    
</script>
