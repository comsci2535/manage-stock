<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logs extends MY_Controller {

	function __construct() {
		parent::__construct();

		$this->load->helper(array('form', 'url' ,'fn'));
		$this->load->model('logs/logs_model');
		$this->load->library('logs_library');
	}

	public function index() {
		$flash_message = $this->session->flashdata('status');	
		$data = array(
			'page_index' => 'backendlog',
			'page' => 'browse',
			'title_page' => 'log',
			'description_page' => 'My Log backend',
			'flash_message' => $flash_message,
		);
		
		$data['logs']  =  $this->logs_model->getLogAll();


		//loade view
		$this->load->view('masters/header', $data);
		$this->load->view('index', $data);
		
	}

}
