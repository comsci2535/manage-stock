<style type="text/css">
    .hidden {
    display: none!important;
}
</style>

<div class="col-xl-12 col-lg-12 mb-4">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">รายการ</h5>
        
            <div class="widget-inner">
                <?php echo form_open($frmAction, array('class' => 'form-horizontal m-form m-form--fit m-form--label-align-right', 'method' => 'post')) ?>
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row ">
                        <div class="col-sm-1 text-right hidden">
                            <button title="ขยายทั้งหมด" id="expand-all" type="button" class="btn btn-xs btn-flat bg-purple"><i class="fa fa-plus"></i></button>
                            <button title="ยุบทั้งหมด" id="collapse-all" type="button" class="btn btn-xs btn-flat bg-purple"><i class="fa fa-minus"></i></button>
                        </div>
                       
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-sm-1 col-form-label"></label>
                        <div class="col-sm-7">
                            <div class="dd" id="nestable"></div>
                        </div>
                    </div>    

                    <div class="form-group m-form__group row">
                        <label class="col-sm-1 col-form-label"></label>
                        <div class="col-sm-7">
                             <textarea class="form-control hidden" name="order" id="nestable-output"></textarea> 
                        </div>
                    </div>  
                    
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">

                    <div class="m-form__actions">
                        <div class="row">
                            <div class="col-1">
                            </div>
                            <div class="col-10">
                                <button type="submit" class="btn btn-success default">บันทึก</button>
                        <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                            </div>
                        </div>
                        
                    </div>
                </div>
                <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
                <input type="hidden" name="categoryType" id="input-catetory-type" value="<?php echo isset($categoryType) ? $categoryType : NULL ?>">
            <?php echo form_close() ?>
            </div>
        </div>
    </div>
</div>





<script>
    var category = <?php echo json_encode($treeData) ?>;
    var options = {'json': category}
    var categoryType = "";
</script>

