<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Module extends MY_Controller{

    private $_title = 'โมดูล';
    private $_pageExcerpt = 'การจัดการข้อมูลเกี่ยวกับโมดูล';
    private $_grpContent = 'module';
    private $_permission;
    private $_treeData;
    private $_orderData = array();

    public function __construct() {
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr', 'error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->_treeData = new stdClass();
        $this->load->model('module_m');
    }

    public function index() {
        $this->load->module('template');

        // toobar
        $action[1][] = action_refresh(base_url("{$this->router->class}"));
        //$action[1][] = action_filter();
        $action[1][] = action_order(base_url("{$this->router->class}/order"));
        $action[2][] = action_add(base_url("{$this->router->class}/create"));
        $action[2][] = action_trash_multi("{$this->router->class}/destroy");
        //$action[3][] = action_trash_view(base_url("{$this->router->class}/trash"));
        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);

        // breadcrumb
        $data['breadcrumb'][] = array("นักพัฒนา", "javascript:void(0)");
        $data['breadcrumb'][] = array($this->_title, base_url("{$this->router->class}"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/index";

        $this->template->layout($data);
    }

    public function data_index() 
    {
        $input = $this->input->post();
        $input['recycle'] = 0;
        $input['grpContent'] = $this->_grpContent;
        $info = $this->module_m->get_rows($input);
        $infoCount = $this->module_m->get_count($input);
        $tree = $this->_build_tree($info);
        $treeData = $this->_print_tree($tree);
        $column = array();
        $key = 0;
        foreach ($treeData as $rsTree) {
            $rs = $rsTree->info;
            if ( isset($input['search']['value']) && $input['search']['value'] != "" ) {
                if ( mb_stripos($rsTree->title, $input['search']['value']) !== false ) {
                    $found = true;
                } else {
                    $found = false;
                }
            } else {
                $found = true;
            }
            if ( $found ) {
                $id = encode_id($rs->moduleId);
                $action = array();
                $action[1][] = table_edit(site_url("{$this->router->class}/edit/{$id}"));
                $active = $rs->active ? "checked" : null;
                $column[$key]['DT_RowId'] = $id;
                $column[$key]['checkbox'] = '<div class="custom-control custom-checkbox tb-check-single">
                                <input type="checkbox" class="custom-control-input check-single" id="customCheckThis'.$key.'">
                                <label class="custom-control-label" for="customCheckThis'.$key.'"></label>
                            </div>';
                $column[$key]['title'] = $rsTree->title;
                $column[$key]['descript'] = $rs->descript;
                $column[$key]['active'] = toggle_active($active, "{$this->router->class}/action/active",$rs->moduleId);
                $column[$key]['createDate'] = datetime_table($rs->createDate);
                $column[$key]['updateDate'] = datetime_table($rs->updateDate);
                $column[$key]['action'] = Modules::run('utils/build_button_group', $action);
                $key++;
            }
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }  

    private function _parent_dropdwon($deep) {
        $info = $this->module_m->get_menu();
        foreach ($info as &$rs ){
            $rs['id'] = $rs['moduleId'];
        }
        $tree = Modules::run('utils/build_tree', $info);
        $print_tree = Modules::run('utils/print_tree', $tree, $deep);
        return $print_tree;
    }

    public function create() {

        $this->load->module('template');
        
        $info['active'] = 1;
        $info['type'] = 1;
        $info['createModule'] = 0;
        $info['isSidebar'] = 0;
        $data['info'] = $info;

        $dropDown = form_dropdown('parentId', $this->_parent_dropdwon(1), '', 'class="form-control select2-single" required');
        $data['parentModule'] = $dropDown;
        $data['frmAction'] = site_url("{$this->router->class}/storage");

        $data['breadcrumb'][] = array("นักพัฒนา", "javascript:void(0)");
        $data['breadcrumb'][] = array($this->_title, base_url("{$this->router->class}"));
        $data['breadcrumb'][] = array("เพิ่มใหม่", base_url("{$this->router->class}/create"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/form";

        $this->template->layout($data);
    }

    public function storage() {
        $input = $this->input->post();
        $value = $this->_build_data($input);
        $id = $this->module_m->insert($value);
        $value['moduleId'] = $id;
        if ( $id ) {
            if ($input['createModule'] == 1) {
                $this->_create_files($value);
            }
           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(base_url("{$this->router->class}"));
    }

    private function _create_files($value) {
        $module = $value['class'];
        $modulesDir['p'] = APPPATH."modules\\".$module."\\";
        $modulesDir['c'] = APPPATH."modules\\".$module."\\controllers\\";
        $modulesDir['m'] = APPPATH."modules\\".$module."\\models\\";
        $modulesDir['v'] = APPPATH."modules\\".$module."\\views\\";
        $modulesDir['s'] = FCPATH ."assets\\backend\\scripts\\".$module."\\";

        foreach ($modulesDir as $key => $rs) {
            create_dir($rs);
            $fileName = array();
            if ($key == 'c') {
                $fileName[0]['name'] = $rs . ucfirst($module) . ".php";
                $content = file_get_contents(APPPATH."modules\\repo\\controllers\\Repo.php");
                $content = str_replace("Repo", ucfirst($value['class']), $content);
                $content = str_replace("repo_m", $value['class']."_m", $content);
                $content = str_replace("repo", $value['class'], $content);
                $content = str_replace("{$value['class']}Id", "repoId", $content);
                $content = str_replace("{{title}}", $value['title'], $content);
                $content = str_replace("{{excerpt}}", $value['descript'], $content);
                $fileName[0]['content'] = $content;
            } else if ($key == 'm') {
                $fileName[0]['name'] = $rs . ucfirst($module) . "_m.php";
                $content = file_get_contents(APPPATH."modules\\repo\\models\\Repo_m.php");
                $content = str_replace("Repo", ucfirst($value['class']), $content);
                $fileName[0]['content'] = $content;
            } else if ($key == 'v') {
                $viewDir = APPPATH . "modules\\repo\\views\\";
                
                $fileName[0]['name'] = $rs . "form.php";
                $content = file_get_contents($viewDir."form.php");
                $fileName[0]['content'] = $content;
                
                $fileName[1]['name'] = $rs . "index.php";
                $content = file_get_contents($viewDir."index.php");
                $fileName[1]['content'] = $content;
                
                $fileName[2]['name'] = $rs . "trash.php";
                $content = file_get_contents($viewDir."trash.php");
                $fileName[2]['content'] = $content;
            } else if ($key == 's') {
                $scriptDir = FCPATH ."assets\\backend\\scripts\\repo\\";
///
                $fileName[0]['name'] = $rs . "form.js";
                $content = file_get_contents($scriptDir."form.js");
                $fileName[0]['content'] = $content;
                
                $fileName[1]['name'] = $rs . "index.js";
                $content = file_get_contents($scriptDir."index.js");
                $fileName[1]['content'] = $content;
                
                $fileName[2]['name'] = $rs . "trash.js";
                $content = file_get_contents($scriptDir."trash.js");
                $fileName[2]['content'] = $content;                
            }

            if ($key != "p") {
                foreach ($fileName as $file) {
                    $fh = fopen($file['name'], "w+") or die("Couldn't open file");
                    fwrite($fh, $file['content']);
                    fclose($fh);
                }
            }
        }
        return true;
    }

    public function edit($id = 0) {
        $this->load->module('template');
        
        $data['breadcrumb'][] = array("นักพัฒนา", "javascript:void(0)");
        $data['breadcrumb'][] = array($this->_title, base_url("{$this->router->class}"));
        $data['breadcrumb'][] = array("แก้ไข", base_url("{$this->router->class}/edit/"));

        $id = decode_id($id);
        $info = $this->module_m->get_by_id($id);
        $dropDown = form_dropdown('parentId', $this->_parent_dropdwon(1), $info['parentId'], 'class="form-control select2-single" required');
        $data['parentModule'] = $dropDown;
        $data['info'] = $info;
        $data['frmAction'] = base_url("{$this->router->class}/update");

       
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/form";

        $this->template->layout($data);
    }

    public function update() {

        $input = $this->input->post();
        $value = $this->_build_data($input);
        $input['id'] = decode_id($input['id']);
        $result = $this->module_m->update($value, $input['id']);
        if ( $result ) {
           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(base_url("{$this->router->class}"));
    }

    public function delete() {
        Modules::run('utility/permission', 2);
        $input = $this->input->post();
        $param = $input['id'];
        $id = $this->module_m->delete($param);
        if ($id) {
            Modules::run('utility/notify', 'warning', 'ลบรายการเรียบร้อย', 'โปรดปรับปรุงสิทธิผู้ดูแลระบบ');
        } else {
            Modules::run('utility/notify', 'error', 'ลบรายการไม่สำเร็จ');
        }
        redirect(base_url("{$this->router->class}"));
    }

    public function order() {
        $this->load->module('template');
        
        $input['recycle'] = 0;
        $input['order'][0]['column'] = 1;
        $input['order'][0]['dir'] = 'asc';
        $info = $this->module_m->get_rows($input);
//        arrx($info->result());
        foreach ($info->result() as $rs) {
            $thisref = &$refs[$rs->moduleId];
            $thisref['id'] = $rs->moduleId;
            if ( $rs->type == 2 ) {
                $thisref['content'] = "-----  ".$rs->title."  -----";
            } else {
                $thisref['content'] = $rs->title;
            }
            if ($rs->parentId != 0) {
                $refs[$rs->parentId]['children'][] = &$thisref;
            } else {
                $treeData[] = &$thisref;
            }
        }
//        arrx($treeData);
        $data['treeData'] = $treeData;
        $data['frmAction'] = site_url("{$this->router->class}/update_order");
        
        
        // toobar
        $action[1][] = action_refresh(base_url("{$this->router->class}/order"));
        $action[2][] = action_list_view(base_url("{$this->router->class}"));
        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb

        $data['breadcrumb'][] = array($this->_title, base_url("{$this->router->class}"));
        $data['breadcrumb'][] = array('จัดลำดับ', base_url("{$this->router->class}/order"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/order";

        $this->template->layout($data);
    }
    
    public function update_order(){
        $input = $this->input->post();
        $order = json_decode($input['order']);
        $value = $this->_build_data_order($order);
        $result = $this->db->update_batch('module', $value, 'moduleId');
        if ( $result ) {
            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}/order"));
    }
    
    private function _build_data_order($info, $parentId=0){
        $order = 0;
        foreach ($info as $rs) {
            $order++;
            $this->_orderData[$rs->id]['moduleId'] = $rs->id; 
            $this->_orderData[$rs->id]['order'] = $order;
            $this->_orderData[$rs->id]['parentId'] = $parentId;
            if ( isset($rs->children) )
                $this->_build_data_order ($rs->children, $rs->id);
        }
        return $this->_orderData;
    }

    private function _build_tree($info, $parentId=0) {
        $tree = new stdClass();
        foreach ($info->result() as $d) {
            if ($d->parentId == $parentId) {
                $children = $this->_build_tree($info, $d->moduleId);
                if ( !empty($children) ) {
                    $d->children = $children;
                }
                $tree->{$d->moduleId} = $d;
            }
        }
        return $tree;
    }
    
    private function _print_tree($tree, $level = 5, $r = 1, $p = null, $d = NULL) {
        foreach ( $tree as $t ) {
            $dash = ($t->parentId == 0) ? '' : $d . ' &#8594; ';
            $this->_treeData->{$t->moduleId} = new stdClass();
            $this->_treeData->{$t->moduleId}->title = $dash . $t->title;
            $this->_treeData->{$t->moduleId}->info = $t;
            if ( isset($t->children) ) {
                if ( $r < $level ) {
                    $this->_print_tree($t->children, $level, $r + 1, $t->parentId, $dash . $t->title);
                }
            }
        }
        return $this->_treeData;
    }

    private function _build_data($input) {
        $value = array();
        $value['active'] = (int)$input['active'];
        $value['type'] = (int)$input['type'];
        $value['parentId'] = (int)$input['parentId'];
        $value['isSidebar'] = (int)$input['isSidebar'];
        $value['directory'] = html_escape($input['directory']);
        $value['title'] = html_escape($input['title']);
        $value['class'] = html_escape($input['class']);
        $value['icon'] = html_escape($input['icon']);
        $value['descript'] = html_escape($input['descript']);
        if ($input['mode'] == 'create') {
            $value['createDate'] = db_datetime_now();
            $value['createBy'] = $this->session->users['user_id'];
        } else {
            $value['updateBy'] = $this->session->users['user_id'];
            $value['updateDate'] = db_datetime_now();
        }
        return $value;
    }
    
    public function destroy()
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ($input['id'] as &$rs) $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updateDate'] = $dateTime;
            $value['updateBy'] = $this->session->users['user_id'];
            
            $value['active'] = 0;
            $value['recycle'] = 1;
            $value['recycleDate'] = $dateTime;
            $value['recycleBy'] = $this->session->users['user_id'];
            $result = $this->module_m->update_in($input['id'], $value);
            
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    } 
    
    public function restore()
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ($input['id'] as &$rs) $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updateDate'] = $dateTime;
            $value['updateBy'] = $this->session->users['user_id'];
            
            $value['active'] = 0;
            $value['recycle'] = 0;
            $result = $this->module_m->update_in($input['id'], $value);
            
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    } 

    public function action($type="")
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ($input['id'] as &$rs) $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updateDate'] = $dateTime;
            $value['updateBy'] = $this->session->users['user_id'];
            $result = false;
            if ( $type == "active" ) {
                $value['active'] = $input['status'] == "true" ? 1 : 0;
                $result = $this->module_m->update_in($input['id'], $value);
            }
            if ( $type == "trash" ) {
                $value['active'] = 0;
                $value['recycle'] = 1;
                $value['recycleDate'] = $dateTime;
                $value['recycleBy'] = $this->session->users['user_id'];
                $result = $this->module_m->update_in($input['id'], $value);
            }
            if ( $type == "restore" ) {
                $value['active'] = 0;
                $value['recycle'] = 0;
                $result = $this->module_m->update_in($input['id'], $value);
            }
            if ( $type == "delete" ) {
                $value['active'] = 0;
                $value['recycle'] = 2;
                $result = $this->module_m->update_in($input['id'], $value);
            }   
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }   

}
