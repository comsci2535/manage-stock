<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Utils extends MX_Controller {

    private $printTree = array('' => 'เลือกรายการ', '0' => 'ไม่กำหนด');
    private $printTreeMulti = array();
    private $temp;
    private $indexOrdering = 0;

    public function __construct() {
        parent::__construct();
        $this->load->model('utils_m');
    }

    public function language() {
        $info = $this->utils_m->get_language();
        foreach ($info as $rs) {
            $lang[$rs['id']] = $rs;
        }

        return $lang;
    }

    public function toastr($type, $lineOne, $lineTwo = "") 
    {
        $temp = array('type' => $type, 'lineOne' => $lineOne, 'lineTwo' => $lineTwo);
        $this->session->set_userdata('toastr', $temp);
    }
    
    public function record_history($info, $mode)
    {
        if ( $mode == 'edit' ) {
            $this->load->model('user/user_m');
            $userCreate = $this->user_m->get_record_admin($info['createBy']);
            $data['create'] = $userCreate['username'].' วันที่ '.date('d-m-Y, H:i', strtotime($info['createDate']));
            $userModify = $this->user_m->get_record_admin($info['modifyBy']);
            $data['modify'] = $userModify['username'].' วันที่ '.date('d-m-Y, H:i', strtotime($info['modifyDate']));
        } else {
            return false;
        }
        $data['mode'] = $mode;
        $this->load->view('utility/record_history', $data);
    }
    
    public function build_tree(Array $data, $parent = 0) {
       
        $tree = array();
        foreach ($data as $d) {
            if ($d['parentId'] == $parent) {
                $children = $this->build_tree($data, $d['id']);
                if (!empty($children)) {
                    $d['children'] = $children;
                }
                $tree[$d['id']] = $d;
            }
        }
        
        return $tree;
    }
    public function build_tree_2(Array $data, $parent = 0) {
       
        $tree = array();
        foreach ($data as $d) {
            if ($d['parent_id'] == $parent) {
                $children = $this->build_tree_2($data, $d['id']);
                if (!empty($children)) {
                    $d['children'] = $children;
                }
                $tree[$d['id']] = $d;
            }
        }
        
        return $tree;
    }

    public function print_tree($tree, $level = 3, $round = 1, $p = null, $d = NULL) {
        foreach ($tree as $t) {
            $dash = '';
            if ($round > 1) {
                $str = '&nbsp;&nbsp;';
                $dash = str_repeat($str, $round);
            }
            $this->printTree[$t['id']] = $dash . "" . $t['title'];
            if (isset($t['children'])) {
                if ($round < $level) {
                    $this->print_tree($t['children'], $level, $round + 1, null, null);
                }
            }
        }

       
        return $this->printTree;
    }

    public function print_tree_label($tree, $level = 3, $r = 1, $p = null, $d = NULL) {
        foreach ($tree as $t) {
            $dash = ($t['parent'] == 0) ? '' : $d . ' &#8594; ';
            $this->printTreeMulti[$t['id']] = $dash . $t['title'];
            if (isset($t['children'])) {
                if ($r < $level) {
                    $this->print_tree_label($t['children'], $level, $r + 1, $t['parent'], $dash . $t['title']);
                }
            }
        }
        return $this->printTreeMulti;
    }

    public function build_data_orderig_nestable($tree, $parent = 0) {
        foreach ($tree as $t) {
            $this->indexOrdering++;
            if (isset($t->children)) {
                $this->temp[$t->id]['id'] = $t->id;
                $this->temp[$t->id]['ordering'] = $this->indexOrdering;
                $this->temp[$t->id]['parent'] = $parent;
                $this->build_data_orderig_nestable($t->children, $t->id);
            } else {
                $this->temp[$t->id]['id'] = $t->id;
                $this->temp[$t->id]['ordering'] = $this->indexOrdering;
                $this->temp[$t->id]['parent'] = $parent;
            }
        }
        return $this->temp;
    }
    
     public function build_toolbar($action)
    {
        $str = '<div class="btn-toolbar box-tools" style="float:right;" role="toolbar" aria-label="Toolbar with button groups">';
        foreach ($action as $key=>$group){
            $str .= '<div class="btn-group mr-2" role="group" aria-label="'.$key.' group">';
            foreach ($group as $rs) $str .= $rs;
            $str .="</div>";
        }
        $str .="</div>";
        return $str;
    }
    
    public function build_button_group($action)
    {
        $str = null;
        $str = '<div class="d-inline-block" role="toolbar" aria-label="Toolbar with button groups">';
        foreach ($action as $key=>$group){
            $str .= '<div class="btn-group mr-2" role="group" aria-label="'.$key.' group">';
            foreach ($group as $rs) $str .= $rs;
            $str .="</div>";
        }
         $str .="</div>";
        return $str;

    }
/*
 * delete lang table
 */    
    public function delete_lang($input)
    {
        $query = $this->db
                        ->where('grpContent', $input['grpContent'])
                        ->where_in('contentId', $input['id'])
                        ->delete('lang');
        return $query;
    }

}
