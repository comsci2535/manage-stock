<div class="box-tools col-lg-12 m-b10" >
    <?php echo $boxAction; ?>
</div>
<div class="col-lg-12 ">
    <div class="filter">
    <form id="frm-filter" role="form">
        <div class="col-md-3 form-group">
            <label for="startRang">วันที่สร้าง</label>
            <input type="text" name="createDateRange" class="form-control" value="" />
            <input type="hidden" name="createStartDate" value="" />
            <input type="hidden" name="createEndDate" value="" />
        </div>    
        <div class="col-md-3 form-group">
            <label for="startRang">วันที่แก้ไข</label>
            <input type="text" name="updateDateRange" class="form-control" value="" />
            <input type="hidden" name="updateStartDate" value="" />
            <input type="hidden" name="updateEndDate" value="" />
        </div>  
        <div class="col-md-2 form-group">
            <label for="active">สถานะ</label>
            <?php $activeDD = array(""=>'ทั้งหมด', 1=>'เปิด', 0=>'ปิด') ?>
            <?php echo form_dropdown('active', $activeDD, null, 'class="from-control select2"') ?>
        </div> 
        
        <div class="col-md-3 form-group">
            <label for="keyword">คำค้นหา</label>
            <input class="form-control" name="keyword" type="text">
        </div>           
        <div class="col-md-1 form-group">
            <button type="button" class="btn btn-primary btn-flat btn-block btn-filter"><i class="fa fa-filter"></i> ค้นหา</button>
        </div>
    </form>
    </div>
</div>
<div class="col-lg-12 m-b30">
    <div class="widget-box">
        <div class="wc-title">
            <h4>รายการ</h4>
        </div>
        <div class="widget-inner">
            <table id="data-list" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th><input name="tbCheckAll" type="checkbox" class="icheck tb-check-all"></th>
                        <th>รายการ</th>
                        <th>เนื้อหาย่อ</th>
                        <th>สร้าง</th>
                        <th>แก้ไข</th>
                        <th>สถานะ</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
