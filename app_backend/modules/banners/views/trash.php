


<div class="box-tools col-lg-12 m-b10" >
    <?php echo $boxAction; ?>
</div>

<div class="col-lg-12 m-b30">
    <div class="widget-box">
        <div class="wc-title">
            <h4>รายการ</h4>
        </div>
        <div class="widget-inner">
            <table id="data-list" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th><input name="tbCheckAll" type="checkbox" class="icheck tb-check-all"></th>
                        <th>รายการ</th>
                        <th>เนื้อหาย่อ</th>
                        <th>ขยะ</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
