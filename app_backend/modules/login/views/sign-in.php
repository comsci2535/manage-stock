<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Admin | Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="<?=$this->config->item('template'); ?>font/iconsmind-s/css/iconsminds.css" />
    <link rel="stylesheet" href="<?=$this->config->item('template'); ?>font/simple-line-icons/css/simple-line-icons.css" />

    <link rel="stylesheet" href="<?=$this->config->item('template'); ?>css/vendor/bootstrap.min.css" />
    <link rel="stylesheet" href="<?=$this->config->item('template'); ?>css/vendor/bootstrap-float-label.min.css" />
    <link rel="stylesheet" href="<?=$this->config->item('template'); ?>css/main.css" />
</head>

<body class="background show-spinner">
    <div class="fixed-background"></div>
    <main>
        <div class="container">
            <div class="row h-100">
                <div class="col-12 col-md-10 mx-auto my-auto">
                    <div class="card auth-card">
                        <div class="position-relative image-side ">

                            <p class=" text-white h2">เพื่อนช่าง</p>

                            <p class="white mb-0">
                               บริษัท คาลบ์ อินดัสทรี (ประเทศไทย) จำกัด
                            </p>
                        </div>
                        <div class="form-side">
                            <a href="Dashboard.Default.html">
                                <span class="logo-single"></span>
                            </a>
                            <h6 class="mb-4">Login</h6>
                            <?php if($this->session->flashdata('status') === 'error'):?>
                                
                                <div class="alert alert-danger alert-dismissible fade show  mb-10" role="alert">
                                    <strong><?php echo $this->session->flashdata('message');?></strong> 
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                               
                            <?php endif; ?>
                            <?php if($this->session->flashdata('status') === 'warning'):?>
                                

                                <div class="alert alert-warning alert-dismissible fade show  mb-10" role="alert">
                                    <strong><?php echo $this->session->flashdata('message');?></strong> 
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                               
                            <?php endif; ?>
                              <?php echo form_open(base_url('login/check_login'), 'id="frm-login" class="m-login__form m-form"'); ?>
                                <label class="form-group has-float-label mb-4">
                                    <input class="form-control" name="username" />
                                    <span>Username</span>
                                </label>

                                <label class="form-group has-float-label mb-4">
                                    <input class="form-control" type="password" name="password" placeholder="" />
                                    <span>Password</span>
                                </label>
                                <div class="d-flex justify-content-between align-items-center">
                                    <a href="#">Forget password?</a>
                                    <button id="m_login_signin_submit" class="btn btn-primary btn-lg btn-shadow" type="submit">LOGIN</button>
                                </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script type="text/javascript">
        var appName = "<?=config_item('appName');?>";
        var csrfToken = get_cookie('csrfCookie');
        var siteUrl = "<?php echo $this->config->item('root_url'); ?>";
        var baseUrl = "<?php echo $this->config->item('root_url'); ?>";
        var root_url = "<?php echo $this->config->item('root_url'); ?>";
        var controller = "<?php echo $this->router->class ?>";
        var method = "<?php echo $this->router->method ?>";
        function get_cookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ')
                    c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0)
                    return c.substring(nameEQ.length, c.length);
            }
            return null;
        }
    </script>
    <script src="<?=$this->config->item('template'); ?>js/vendor/jquery-3.3.1.min.js"></script>
    <script src="<?=$this->config->item('template'); ?>js/vendor/bootstrap.bundle.min.js"></script>
    <script src="<?=$this->config->item('template'); ?>js/dore.script.js"></script>
    <script src="<?=$this->config->item('template'); ?>js/scripts.js"></script>
    <script src="<?=$this->config->item('template'); ?>scripts/app.js"></script>


    <script type="text/javascript">

         $('#frm-login').validate()
         window.setTimeout(function() {
            $(".alert").fadeTo(500, 0).slideUp(500, function(){
                $(this).remove(); 
            });
        }, 4000);
    </script>
</body>

</html>