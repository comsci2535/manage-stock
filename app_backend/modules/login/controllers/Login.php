<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller
{
	

	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users_library');
		$this->load->library('logs_library');
		
	}

	
	
	public function index()
	{
		$this->load->view('sign-in');
	}

	public function check_login()
    {
    	

		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if ($this->form_validation->run() == TRUE)
		{
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$remember = (bool)$this->input->post('remember');

			$users = $this->users_library->login(
				$username,
				$password,
				$remember
			);
  			

			if($users['status'] === 'success'){
				$users = $this->session->users;
                $users['isBackend'] = TRUE;
                $users['user_id'] = $users['UID'];
                $users['image'] = $this->config->item('root_url')."images/user.png";
                $this->session->set_userdata('users', $users);
                $this->session->set_flashdata('firstTime', '1');
                //$this->login_m->update_last_login();
				
				//Go to  dashboard
				redirect(base_url('dashboard'),'refresh');
			}elseif ($users['status'] === 'warning') {
			
				$this->session->set_flashdata('status',$users['status']);
				$this->session->set_flashdata('message',$users['message']);
				//Go to  users/login
				redirect(base_url('login/check_login'),'refresh');
			}else{
				
				$this->session->set_flashdata('status',$users['status']);
				$this->session->set_flashdata('message',$users['message']);
				//Go to  users/login
				redirect(base_url('login/check_login'),'refresh');
			}

			
		}
		else
		{
            //loade view
			$this->load->view('sign-in');
		}

	}

	public function Logout()
	{
		$usename = $this->session->userdata('users');
		//Log
		//$this->logs_library->save_log($usename['Username'],$this->class,$this->method,'Logout : Logouted');

		delete_cookie('username');
		delete_cookie('salt');

		$this->session->unset_userdata('users');
		redirect(base_url('login'),'refresh');
	}
}
