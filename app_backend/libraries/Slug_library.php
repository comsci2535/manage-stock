<?php

/**
 *
 */
class Slug_library {
	public $CI;

	function __construct() {
		$this->CI = &get_instance();

		$this->CI->load->model('slug_model');
	}

	public function getSlug($name, $id = null, $db) {
		$data = $this->CI->slug_model->getSlugCount($name, $id, $db);
		if ($data === 0) {
			return true;
		} else {
			return false;
		}
	}

	public function getName($name, $id = null, $db) {
		$data = $this->CI->slug_model->getNameCount($name, $id, $db);
		if ($data === 0) {
			return true;
		} else {
			return false;
		}
	}

}

?>