<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['socialShare'] = TRUE;
$config['shareThis'] = TRUE;
$config['keepTrack'] = TRUE;
