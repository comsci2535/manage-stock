<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct(){
		parent::__construct();

        // สำหรับใช้คำสั่ง site_url และ redirect
        $this->load->helper('url');
	}

	public function login(){

		$this->load->library('facebook'); // เรียกใช้งาน library Facebook โหลดค่าจาก config/facebook.php อัตโนมัติ
        // หรือ
        // ถ้าคุณไม่ต้องการให้โหลดค่า App id , App Seret จาก ไฟล์ config/facebook.php ใช้คำสั่งด้านล่างนี้แทน
        //$this->load->library('facebook', array(
        //    'appId' => 'ใส่ APP_ID',
        //    'secret' => 'ใส่ APP_SECRET',
        //    ));

		$user = $this->facebook->getUser(); // เรียกค่าการ Login
        
        if ($user) { // ตรวจสอบการ Login
            try {
                $data['user_profile'] = $this->facebook->api('/me');
            } catch (FacebookApiException $e) {
                $user = null;
            }
                // เรียกค่าต่างๆจาก Facebook
        }else {
            $this->facebook->destroySession(); // ลบ Session การ Login กรณี ไม่มีการ Login
        }

        if ($user) { // ตรวจสอบว่าในตัวแปลมีข้อมูลการ Login
            //เข้าสู่ระบบแล้ว
            $data['logout_url'] = site_url('welcome/logout'); // ออกจากระบบและส่งกลับไปที่หน้า Welcome/logout
            // หรือ
            // ออกจากระบบด้วยคำสั่งอัตโนมัติ
            // $data['logout_url'] = $this->facebook->getLogoutUrl();

        } else {
            //ยังไม่มีการเข้าสู่ระบบ
            $data['login_url'] = $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url('welcome/login'), 
                'scope' => array("email") //ลิงค์หลังจากการเข้าสู่ระบบและย้อนกลับมาที่หน้า welcome/login
            ));
        }
        // แสดงข้อมูลที่หน้า view/login.php
        $this->load->view('login',$data);

	}

    public function logout(){

        $this->load->library('facebook');

        // เรียกใช้งาน library Facebook
        $this->facebook->destroySession();
        // ออกจากระบบและย้อนกลับไปที่หน้า welcome/login

        redirect('welcome/login');
    }

}

