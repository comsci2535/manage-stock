<?php
function arr($arr)
{
    echo "<pre>";
    print_r($arr);
    echo "</pre>";
}

function arrx($arr,$arr2=array())
{
    header('Content-Type: text/html; charset=utf-8');
    echo "<pre>";
    print_r($arr);
    if (!empty($arr2)){
        print_r($arr2);
    }
    echo "</pre>";
    exit();
}

function encode_id($id = 0)
{
    if (!config_item('encodeId') ) return $id;
    if ( $id > 0 )
        $string = base64_encode("b64" . $id . "@" . rand(1, 99));
    else
        return $id;

    return $string;
}

function decode_id($string = '')
{
    if ( $string != '' ) {
        if ( !is_numeric($string) ) {
            $tmp = base64_decode($string);
            $tmp = explode("@", $tmp);
            $id = substr($tmp[0], 3);
            if ( !is_numeric($id) )
                unset($id);
        }
        else return $string;
    }
    return $id;
}

function redirect_back()
{
    if ( isset($_SERVER['HTTP_REFERER']) ){
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    } else {
        header('Location: http://' . $_SERVER['SERVER_NAME']);
    }
    die;
}

function url_change_lang($uriString, $inputGet, $lang)
{
    if (!empty($inputGet)) {
        $q = urldecode(http_build_query($inputGet)); 
        $url = base_url("{$lang}/{$uriString}?{$q}");
    } else {
        $url = base_url("{$lang}/{$uriString}");
    }
    return $url;
}

function random_string($word_length = 8)
{
    $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $word = '';
    for ( $i = 0, $mt_rand_max = strlen($pool) - 1; $i < $word_length; $i++ )
    {
        $word .= $pool[mt_rand(0, $mt_rand_max)];
    }
    return $word;
}

function db_date_now()
{
    return date("Y-m-d");
}

function db_datetime_now()
{
    return date("Y-m-d H:i:s");
}

function datetime_table($date)
{
    $str = "";
    if ($date) {
        $str = date("d-m-Y H:i", strtotime($date));
    }
    return $str;
}

function thai_month_abbr($month) {
    $thai_month_arr = array(   
        "0"=>"",   
        "1"=>"ม.ค.",   
        "2"=>"ก.พ.",   
        "3"=>"มี.ค.",   
        "4"=>"เม.ย.",   
        "5"=>"พ.ค.",   
        "6"=>"มิ.ย.",    
        "7"=>"ก.ค.",   
        "8"=>"ส.ค.",   
        "9"=>"ก.ย.",   
        "10"=>"ต.ค.",   
        "11"=>"พ.ย.",   
        "12"=>"ธ.ค."                    
    );       
    return ($thai_month_arr[intval($month)]);
}

function thai_month($month) {
    $thai_month_arr = array(
        "0"=>"",
        "1"=>"มกราคม",
        "2"=>"กุมภาพันธ์",
        "3"=>"มีนาคม",
        "4"=>"เมษายน",
        "5"=>"พฤษภาคม",
        "6"=>"มิถุนายน", 
        "7"=>"กรกฎาคม",
        "8"=>"สิงหาคม",
        "9"=>"กันยายน",
        "10"=>"ตุลาคม",
        "11"=>"พฤศจิกายน",
        "12"=>"ธันวาคม"                 
    );
    return ($thai_month_arr[intval($month)]);
}
function thai_date($time){
    $thai_day_arr=array("อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์");
    $thai_month_arr=array(
        "0"=>"",
        "1"=>"มกราคม",
        "2"=>"กุมภาพันธ์",
        "3"=>"มีนาคม",
        "4"=>"เมษายน",
        "5"=>"พฤษภาคม",
        "6"=>"มิถุนายน", 
        "7"=>"กรกฎาคม",
        "8"=>"สิงหาคม",
        "9"=>"กันยายน",
        "10"=>"ตุลาคม",
        "11"=>"พฤศจิกายน",
        "12"=>"ธันวาคม"                 
    );
//    global $thai_day_arr,$thai_month_arr;
    $thai_date_return="วัน".$thai_day_arr[date("w",$time)];
    $thai_date_return.= "ที่ ".date("j",$time);
    $thai_date_return.=" เดือน".$thai_month_arr[date("n",$time)];
    $thai_date_return.= " พ.ศ.".(date("Yํ",$time)+543);
    $thai_date_return.= "  ".date("H:i",$time)." น.";
    return $thai_date_return;
}

function create_dir($path="uploads/", $indexFile=TRUE)
{
    $rs = 1;
    if(!is_dir($path)){
     mkdir($path,0777,true);
     $rs = 0;

     if ($indexFile) {
        $fh = fopen( "{$path}index.html", "w+" ) or die( "Couldn't open file" );
        fwrite( $fh, "<!DOCTYPE html>
            <html>
            <head>
            <title>403 Forbidden</title>
            </head>
            <body>
            <h1>403 Forbidden</h1>
            <p>Directory access is forbidden.</p>
            </body>
            </html>" );
        fclose( $fh );
    }
}
return $rs;
}

function file_upload_max_size() {
    static $max_size = -1;

    if ($max_size < 0) {
      // Start with post_max_size.
      $max_size = parse_size(ini_get('post_max_size'));

      // If upload_max_size is less, then reduce. Except if upload_max_size is
      // zero, which indicates no limit.
      $upload_max = parse_size(ini_get('upload_max_filesize'));
      if ($upload_max > 0 && $upload_max < $max_size) {
        $max_size = $upload_max;
    }
}
return $max_size;
}

function parse_size($size) {
    $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
    $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
    if ($unit) {
      // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
      return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
  }
  else {
      return round($size);
  }
  
}

function file_size_unit($bytes) {
  if ($bytes >= 1073741824) {
      $bytes = number_format($bytes / 1073741824, 2) . ' GB';
  } elseif ($bytes >= 1048576) {
      $bytes = number_format($bytes / 1048576, 2) . ' MB';
  } elseif ($bytes >= 1024) {
      $bytes = number_format($bytes / 1024, 2) . ' kB';
  } elseif ($bytes > 1) {
      $bytes = $bytes . ' bytes';
  } elseif ($bytes == 1) {
      $bytes = $bytes . ' byte';
  } else {
      $bytes = '0 bytes';
  }
  
  return $bytes;
}
function action_export_group($info)
{
    $str ="<div class=\"btn-group\">
    <button type=\"button\" class=\"btn btn-sm  default \"><i class='fa fa-download'></i> ส่งออก</button>
    <button type=\"button\" class=\"btn btn-sm  default  dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
    <span class=\"caret\"></span>
    <span class=\"sr-only\">Toggle Dropdown</span>
    </button>
    <ul class=\"dropdown-menu\" role=\"menu\">";
    foreach ( $info as $key=>$rs) {
        switch ($key) {
            case "excel": $icon = "fa fa-file-excel-o"; $title = "ไฟล์ MS Excel"; break;
            case "pdf": $icon = "fa fa-file-pdf-o"; $title = "ไฟล์ PDF"; break;
            case "word": $icon = "fa fa-file-pdf-o"; $title = "ไฟล์ MS Word"; break;
            default : $icon = "fa fa-download"; $title = "เอกสาร"; break;
        }
        $str .= " <li><a href=\"{$rs}\"><i class='{$icon}'></i>{$title}</a></li>";
    }
    $str .= "</ul>
    </div>";
    return $str;
}
function action_filter($url="javascript:;")
{
    $str = "<a href =\"{$url}\" class=\"btn btn-sm  btn-warning default box-filter\" data-target=\"#filter\" data-toggle=\"collapse\"><span style=\"padding: 8px 0px\" title=\"กรอง\"><i class=\"fa fa-filter\"></i></span> กรองข้อมูล</a>";
    return $str;
}

function action_custom($url, $btn, $class, $title, $icon, $onClick="")
{
    if ($onClick != "") $onClick = " onclick='{$onClick}'";
    $str = "<a href =\"{$url}\" class=\"btn btn-sm  {$btn} default box-{$class}\" {$onClick} title=\"{$title}\"><i class=\"fa {$icon}\"></i> {$title}</a>";
    return $str;
}

function action_refresh($url)
{
    $str = "<a href =\"{$url}\" class=\"btn btn-sm btn-primary   default box-add\"  title=\"โหลดใหม่\"><i class=\"glyph-icon simple-icon-refresh\"></i> โหลดหน้า</a>";
    return $str;
}

function action_back($url)
{
    $str = "<a href =\"{$url}\" class=\"btn btn-sm   default box-back\"  title=\"ย้อนกลับ\"><i class=\"fa fa-back\"></i> </a>";
    return $str;
}

function action_add($url)
{
    $str = "<a href =\"{$url}\" class=\"btn btn-sm  btn-success default box-add\"  title=\"เพิ่ม\"><i class=\"fa fa-plus\"></i> เพิ่มรายการ</a>";
    return $str;
}

function action_import($url)
{
    $str = "<a href =\"{$url}\" class=\"btn btn-sm  btn-info default action-import\"  title=\"นำเข้าข้อมูล\"><i class=\"fa fa-level-down \"></i> นำเข้าข้อมูล</a>";
    return $str;
}

function action_info($url)
{
    $str = "<a href =\"{$url}\" class=\"btn btn-sm   default box-info\"  title=\"ข้อมูลทั่วไป\"><i class=\"fa fa-info-cricle\"></i> เนื้อหาข้อมูล</a>";
    return $str;
}
function action_list_view($url)
{
    $str = "<a href =\"{$url}\" class=\"btn btn-sm  btn-info default act-trash\"  title=\"รายการ\"><i class=\"fa fa-align-justify\"></i> รายการ</a>"; 
    return $str;
}

function action_trash_view($url)
{
    $str = "<a href =\"{$url}\" class=\"btn btn-sm btn-light  default act-trash\"  title=\"ถังขยะ\"><i class=\"fa fa-recycle\"></i> รายการถังขยะ</a>"; 
    return $str;
}

function action_trash_multi($url)
{
    $str = "<a data-method=\"{$url}\" href =\"javascript:;\" class=\"btn btn-sm  default btn-danger trash-multi\"  title=\"ย้ายลงถังขยะ\"><i class=\"fa fa-trash\"></i> ย้ายลงถังขยะ</a>";
    return $str;
}

function table_trash($url)
{
    $str = "<a data-method=\"{$url}\" href =\"javascript:;\" class=\"btn btn-sm  default btn-danger mb-1 tb-action\"  title=\"ย้ายลงถังขยะ\"><i class=\"fa fa-trash\"></i> </a>";
    return $str;
}

function table_delete($url)
{
    $str = "<a href='javascript:;' data-method='{$url}' title='ลบถาวร' class='btn btn-sm  btn-danger default mb-1 tb-action'><i class='fa fa-trash'></i></a>";
    return $str;
}

function table_restore($url)
{
    $str = "<a href='javascript:;' data-method='{$url}' title='กู้รายการ' class='btn btn-sm  btn-warning default mb-1 tb-action'><i class='fa fa-recycle'></i></a>";
    return $str;
}
function table_edit($url)
{
    $str = "<a href =\"{$url}\" class=\"btn btn-light default btn-sm mb-1 tb-edit\" title=\"แก้ไข\"><i style=\"line-height: 1.5;\" class=\"simple-icon-pencil\"></i> </a>";
    return $str;
}

function action_delete_multi($url)
{
    $str = "<a data-method=\"{$url}\" href =\"javascript:;\" class=\"btn btn-sm  default btn-danger trash-multi\" title=\"ย้ายลงถังขยะ\"><i class=\"fa fa-trash\"></i> ลบถาวร</a>";
    return $str;
}

function action_delete($url)
{
    $str = "<a href =\"{$url}\" class=\"btn btn-sm  default btn-danger box-delete\" title=\"ลบถาวร\"><i class=\"fa fa-ban\"></i> </a>";
    return $str;
}


function library_action_delete($url)
{
    $str = "<a href =\"{$url}\" class=\"btn btn-sm  btn-danger default library-delete\"  title=\"ลบ\"><i class=\"fa fa-times\"></i> </a>";
    return $str;
}

function action_order($url)
{
    $str = "<a href =\"{$url}\" class=\"btn btn-sm btn-info   default box-ordering\"  title=\"เรียงลำดับย้ายหมวดหมู่\"><i class=\"glyph-icon iconsminds-align-right\"></i> เรียงลำดับ</a>";
    return $str;
}

function action_listing($url)
{
    $str = "<a href =\"{$url}\" class=\"btn btn-sm   default box-ordering\"  title=\"รายการ\"><i class=\"fa fa-list\"></i> </a>";
    return $str;
}

function action_export($url)
{
    $str = "<a href =\"{$url}\" class=\"btn btn-sm   default box-ordering\"  title=\"นำออก\"><i class=\"fa fa-file-excel-o\"></i> </a>";
    return $str;
}
function toggle_active($active, $url ,$id=null)
{   

    $str = '<div class="custom-switch custom-switch-primary-inverse mb-2 custom-switch-small">
                <input class="custom-switch-input " id="switch'.$id.'" type="checkbox" '.$active.' data-method="'.$url.'">
                <label class="custom-switch-btn" for="switch'.$id.'"></label>
            </div>';
    return $str;
}

function excel_header() {
    $style_header = array(
        'font' => array(
            'bold' => true,
            'color' => array('argb' => '00000000'),
        ),
        'alignment' => array(
            'horizontal' =>  \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        ),
        'borders' => array(
            'top' => array(
                'borderStyle' =>  \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => array('argb' => '00000000'),
            ),
            'right' => array(
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            ),
            'bottom' => array(
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            ),
            'left' => array(
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            ),            
        ),
        'fill' => array(
            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
            'startColor' => array('argb' => 'FFA0A0A0'),
        ),
    );
    return $style_header;
}

function excel_body() {
    $style_body = array(
        'font' => array('color' => array('argb' => '00000000')),
        'alignment' => array(
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
        ),
        'borders' => array(
            'top' => array(
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            ),
            'right' => array(
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            ),            
            'bottom' => array(
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            ),
            'left' => array(
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            ),             
        ),
    );

    return $style_body;
}

function pw_hash($str)
{
    $options = [
        'cost' => 10,
        'salt' => config_item('encryption_key'),
    ];
    return password_hash($str, PASSWORD_BCRYPT, $options);

}



if ( !function_exists('date_language') )
{

    function date_language($date, $time = TRUE, $lang="en")
    {
        //$ci = & get_instance();
//        $lang = $ci->session->userdata('lang');
//        $lang = "thai";
        if ( $date == "" || $date == "0000-00-00 00:00:00" || $date == "0000-00-00" )
        {
            $date = '-';
            return($date);
        }
        elseif ( $lang == "th" )
        {
            $strYear = date("Y", strtotime($date)) + 543;
            $strMonth = date("n", strtotime($date));
            $strDay = date("d", strtotime($date));
            $strHour = date("H", strtotime($date));
            $strMinute = date("i", strtotime($date));
            $strSeconds = date("s", strtotime($date));
            $strMonthCut = Array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
            $strMonthThai = $strMonthCut[$strMonth];
            if ( $time )
            {
                return "$strDay $strMonthThai $strYear, $strHour:$strMinute";
            }
            else
            {
                return "$strDay $strMonthThai $strYear";
            }
        }
        else
        {
            if ( $time )
            {
                return date("d-m-Y, H:i", strtotime($date));
            }
            else
            {
                return date("d M Y", strtotime($date));
            }
        }
        return($date);
    }

}


if ( !function_exists('price_th') )
{

    function price_th($n, $precision = 1 )
    {
        $n = str_replace(",","",$n);

        if ($n < 99999) {
            //ราคา 0 - 900
            $n_format = number_format($n, $precision);
            $suffix = ' บาท';
        } else if ($n < 99999) {
            //ราคา 99,999 บาท
            $n_format = number_format($n / 1, $precision);
            $suffix = ' บาท';

        } else if ($n < 999999) {
            //ราคา 9.99 แสน
            if($n < 100000){
                $n_format = number_format($n / 100000, $precision);
                $suffix = ' แสนบาท';
            }else if($n <= 999000){
                if($n > 99999){
                    $num = number_format($n / 100000, $precision+1);
                    $num_f = $num;
                    if(strlen($num) == 4){
                        if(strlen($num_f/10) == 5){
                            $n_format = number_format($n / 100000, $precision+1);
                        }else{
                            $n_format = number_format($n / 100000, $precision);
                        }
                    }else{
                        $n_format = number_format($n / 100000, $precision+1);
                    }
                    $suffix = ' แสนบาท';
                }
            }

        } else if ($n < 900000000) {
            //ราคา 0.9m-850m
            $num = number_format($n / 1000000, $precision+1);
            $num_f = $num;
            if(strlen($num) == 4){
                if(strlen($num_f/10) == 5){
                    $n_format = number_format($n / 1000000, $precision+1);
                }else{
                    $n_format = number_format($n / 1000000, $precision);
                }
            }elseif (strlen($num) == 5) {
                $n_format = number_format($n / 1000000, $precision+1);
            }else{          
                $n_format = number_format($n / 1000000, $precision);
            }

            $suffix = ' ล้านบาท';
        } else if ($n < 900000000000) {
            //ราคา 0.9b-850b
            $n_format = number_format($n / 1000000000, $precision);
            $suffix = ' พันล้าน';
        } else {
            //ราคา 0.9t+
            $n_format = number_format($n / 1000000000000, $precision);
            $suffix = 'T';
        }
      // ลบค่าที่ไม่จำเป็นออกหลังจากจุดทศนิยม "1.0" -> "1"; "1.00" -> "1"
      // Intentionally does not affect partials, eg "1.50" -> "1.50"
        if ( $precision > 0 ) {
            $dotzero = '.' . str_repeat( '0', $precision );
            $n_format = str_replace( $dotzero, '', $n_format );
        }
        return $n_format . $suffix;
    }

}