<?php defined('BASEPATH') OR exit('No direct script access allowed.');

class Sms_api_libs
{

  private $ci;
  public function __construct()
  {
    $this->ci =& get_instance();
    $this->ci->load->database();

    $this->ci->db->where('sms_id',1);
    $result = $this->ci->db->get('setting_sms')->row_array();

    $this->api_server = $result['sms_url'];
    $this->sms_user = $result['sms_user'];
    $this->sms_pass = $result['sms_pass'];
    //$this->sms_user = 'thaibulksms';
    //$this->sms_pass = 'thisispassword';
    $this->sms_function = $result['sms_function'];
    $this->connect_api = true;

    $headers = @get_headers($this->api_server);
    if(strpos($headers[0],'200')===false){
        $this->connect_api = false;
    }
  }

  public function SendMessesge($param = array())
  {
    $rest_config['server'] = $this->api_server;
    $this->ci->rest->initialize($rest_config);

    $param['username'] = $this->sms_user;
    $param['password'] = $this->sms_pass;
    $param['sender'] = 'SMS';
    $param['ScheduledDelivery'] = '';
    $param['force'] = 'standard';

    //$param['force'] = 'premium';
    //$param['msisdn'] = '0813017109';
    //$param['message'] = '������Ѻ�����������';

    if($this->connect_api===true){
        $result = $this->ci->rest->get($this->sms_function,$param);
        $result = (array)$result;

    }else{//not connect api
        $result = array();
    }
    //arr($result);exit;
    return $result;
  }

  public function GetCreditMessesge()
  {
    $rest_config['server'] = $this->api_server;
    $this->ci->rest->initialize($rest_config);

    $param = array(
        'tag' => 'credit_remain',
    );

    $param['username'] = $this->sms_user;
    $param['password'] = $this->sms_pass;

    if($this->connect_api===true){
        $result = $this->ci->rest->post($this->sms_function,$param);
        //$result = (array)$result;

    }else{//not connect api
        $result = '';
    }
    //arr($result);exit();
    return $result;
  }

  public function ConvertLinkBitly($link)
  {
    $api_server = 'http://api-ssl.bit.ly/';
    $connect_api = true;

    /*$headers = @get_headers($api_server);
    if(strpos($headers[15],'200')===false){
        $connect_api = false;
    }*/

    $rest_config['server'] = $api_server;
    $this->ci->rest->initialize($rest_config);

    $param = array(
        'login' => 'o_2dajkti4a5',
        'apiKey' => 'R_88a25a2989184b7799387ef98a0fd24b',
        'longUrl' => $link
    );

    if($connect_api===true){
        $result = $this->ci->rest->post('v3/shorten',$param);
        $result = (array)$result;
        //arr($result);exit();
        if($result['status_code'] == 200){
            $result = $result['data']->url;
        }else{
            $result = '';
        }

    }else{//not connect api
        $result = '';
    }
    return $result;
  }



}

?>

