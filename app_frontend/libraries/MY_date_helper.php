<?php defined('BASEPATH') OR exit('No direct script access allowed.');

/**
 * CodeIgniter Date Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Philip Sturgeon
 */

// ------------------------------------------------------------------------

function format_date($unix, $format = 'Y-m-d')
{
	if ($unix == '' || ! is_numeric($unix))
	{
		$unix = strtotime($unix);
	}

	return strstr($format, '%') !== FALSE
		? ucfirst(utf8_encode(strftime($format, $unix))) //or? strftime($format, $unix)
		: date($format, $unix);
}

function date_now()
{
	date_default_timezone_set('Asia/Bangkok');
	$date_now=date("Y-m-d H:i:s");
	return $date_now;
}

function DateThai($strDate)
{
	if($strDate != '' && $strDate!='1900-01-01')
	{
		  $strYear = date("Y",strtotime($strDate))+543;
		  $strMonth= date("m",strtotime($strDate));
		  $strDay= date("d",strtotime($strDate));
		return "$strDay-$strMonth-$strYear";
	}else{
		return '-';
	}

}

function DateThai2($strDate)
{
$strDate=explode('-',$strDate);
$strYear = $strDate[0]+543;
$strMonth= $strDate[1];
$strDay= $strDate[2];

return "$strDay-$strMonth-$strYear";
}

function Date_eng($strDate)
{
$ex=explode('-',$strDate);
$strYear = $ex[2]-543;
$strMonth= $ex[1];
$strDay=$ex[0];
return "$strYear-$strMonth-$strDay";
}
//**หาวัน*********************************
function DateDiff($strDate1,$strDate2)
{

return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24

}

  
function DateThaiFull($strDate)
	{
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
		$strMonthThai=$strMonthCut[$strMonth];
		return "วันที่ $strDay $strMonthThai $strYear";
	}
	
function DateThaiFull_not_day($strDate)
{
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strMonthThai $strYear";
}
	

function convert($number){ 
$txtnum1 = array('ศูนย์','หนึ่ง','สอง','สาม','สี่','ห้า','หก','เจ็ด','แปด','เก้า','สิบ'); 
$txtnum2 = array('','สิบ','ร้อย','พัน','หมื่น','แสน','ล้าน','สิบ','ร้อย','พัน','หมื่น','แสน','ล้าน'); 
$number = str_replace(",","",$number); 
$number = str_replace(" ","",$number); 
$number = str_replace("บาท","",$number); 
$number = explode(".",$number); 
if(sizeof($number)>2){ 
return 'ทศนิยมหลายตัวนะจ๊ะ'; 
exit; 
} 
$strlen = strlen($number[0]); 
$convert = ''; 
for($i=0;$i<$strlen;$i++){ 
	$n = substr($number[0], $i,1); 
	if($n!=0){ 
		if($i==($strlen-1) AND $n==1){ $convert .= 'เอ็ด'; } 
		elseif($i==($strlen-2) AND $n==2){  $convert .= 'ยี่'; } 
		elseif($i==($strlen-2) AND $n==1){ $convert .= ''; } 
		else{ $convert .= $txtnum1[$n]; } 
		$convert .= $txtnum2[$strlen-$i-1]; 
	} 
} 

$convert .= 'บาท'; 
if($number[1]=='0' OR $number[1]=='00' OR 
$number[1]==''){ 
$convert .= 'ถ้วน'; 
}else{ 
$strlen = strlen($number[1]); 
for($i=0;$i<$strlen;$i++){ 
$n = substr($number[1], $i,1); 
	if($n!=0){ 
	if($i==($strlen-1) AND $n==1){$convert 
	.= 'เอ็ด';} 
	elseif($i==($strlen-2) AND 
	$n==2){$convert .= 'ยี่';} 
	elseif($i==($strlen-2) AND 
	$n==1){$convert .= '';} 
	else{ $convert .= $txtnum1[$n];} 
	$convert .= $txtnum2[$strlen-$i-1]; 
	} 
} 
$convert .= 'สตางค์'; 
} 
return $convert; 
} 

//ปีงบประมาณ

function fiscalYear($date) {
   // วันที่ที่ต้องการตรวจสอบ
   list($year, $month, $day) = explode("-", $date);
   // วันที่ที่ส่งมา (mktime)
   $cday = mktime(0, 0, 0, $month, $day, $year);
   // ปีงบประมาณตามค่าที่ส่งมา (mktime)
   $d1 = mktime(0, 0, 0, 10, 1, $year);
   // ปีใหม่
   $d2 = mktime(0, 0, 0, 1, 1, $year + 1);
   if ($cday >= $d1 && $cday < $d2) {
     // 1 ตค. - 31 ธค.
     $year++;
   }
   return $year;
}

	function thainumDigit($num)
	{
		return str_replace(array( '0' , '1' , '2' , '3' , '4' , '5' , '6' ,'7' , '8' , '9' ),
		array( "o" , "๑" , "๒" , "๓" , "๔" , "๕" , "๖" , "๗" , "๘" , "๙" ),$num);
	}

function ThaiDate2($strDate) {
	if($strDate!='')
	{
			$strYear = date("Y",strtotime($strDate))+543;
			$strMonth= date("n",strtotime($strDate));
			$strDay= date("j",strtotime($strDate));
			$strHour= date("H",strtotime($strDate));
			$strMinute= date("i",strtotime($strDate));
			$strSeconds= date("s",strtotime($strDate));
			$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
			$strMonthThai=$strMonthCut[$strMonth];
			return "$strDay $strMonthThai $strYear";
	}else{
		return '';
	}
	
}

function thaiMonth($strDate) {
		$strMonth= date("m",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strMonthThai";
	
}

function DateThaiFull_Time($strDate)
	{
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay$strMonthThai$strYear $strHour:$strMinute:$strSeconds";
	}
	
function exdate($start_date, $date_num)
{
	$startdatec = strtotime($start_date);
	$tod = $date_num*86400;
	$ndate = $startdatec + $tod;
	return date("Y-m-d",$ndate);
}

function date_diff_($str_start, $str_end)
{
	if($str_start !='' && $str_end!='')
	{
		$str_start = strtotime($str_start); // ทำวันที่ให้อยู่ในรูปแบบ timestamp
		$str_end = strtotime($str_end); // ทำวันที่ให้อยู่ในรูปแบบ timestamp
		
		$nseconds = $str_end - $str_start; // วันที่ระหว่างเริ่มและสิ้นสุดมาลบกัน
		$ndays = round($nseconds / 86400); // หนึ่งวันมี 86400 วินาที
		
		return $ndays;
	}else{return '';}
}

function thai_date($time){
	$thai_day_arr=array("อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์");
	$thai_month_arr=array(
		"0"=>"",
		"1"=>"มกราคม",
		"2"=>"กุมภาพันธ์",
		"3"=>"มีนาคม",
		"4"=>"เมษายน",
		"5"=>"พฤษภาคม",
		"6"=>"มิถุนายน",	
		"7"=>"กรกฎาคม",
		"8"=>"สิงหาคม",
		"9"=>"กันยายน",
		"10"=>"ตุลาคม",
		"11"=>"พฤศจิกายน",
		"12"=>"ธันวาคม"					
	);
	$thai_date_return="วัน".$thai_day_arr[date("w",$time)];
	$thai_date_return.=	"ที่ ".date("j",$time);
	$thai_date_return.=" ".$thai_month_arr[date("n",$time)];
	$thai_date_return.=	" พ.ศ.".(date("Y",$time)+543);
	//$thai_date_return.=	"  ".date("H:i",$time)." น.";
	return $thai_date_return;
}

function ThaiLongDate($strDate)
{
	  $thai_day_arr=array("อาทิตย์ที่","จันทร์ที่","อังคารที่","พุธที่","พฤหัสบดีที่","ศุกร์ที่","เสาร์ที่");  	
	  $strYear = date("Y",strtotime($strDate))+543;
	  $strMonth= date("n",strtotime($strDate));
	  $strDay= date("j",strtotime($strDate));
	  $strHour= date("H",strtotime($strDate));
	  $strMinute= date("i",strtotime($strDate));
	  $strSeconds= date("s",strtotime($strDate));
	  $thai_date_return="วัน".$thai_day_arr[date("w",strtotime($strDate))];  
	  $strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
	  $strMonthThai=$strMonthCut[$strMonth];
	  return "$thai_date_return $strDay $strMonthThai $strYear";
  
	
}


function Day_inweek($strDate)
{
	$a= date('w',strtotime($strDate));
	
	$b= array('อาทิตย์','จันทร์','อังคาร','พุธ','พฤหัสบดี','ศุกร์','เสาร์');
	
	return $c= $b[$a];
}


function Time_sec($time)
{
$time_data=explode(':',$time);
$strH = $time_data[0]*60*60;
$strI = $time_data[1]*60;
$strS = $time_data[2];

$sec=$strH+$strI+$strS;
return $sec;
}

//rand_string----------------------
function rand_string( $length ) {
	
	$str="";
	$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";	

	$size = strlen( $chars );
	for( $i = 0; $i < $length; $i++ ) {
		$str .= $chars[ rand( 0, $size - 1 ) ];
	}

	return $str;
}



?>  

