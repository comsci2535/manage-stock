<?php defined('BASEPATH') OR exit('No direct script access allowed.');

class Data_api_webex
{

  private $ci;
  public function __construct()
  {

  }

  public function CreateMeetingData($app_id=null)
  {
        $result = array();
        $doc = new DOMDocument();
        $file =site_url('webex/CreateMeeting/'.$app_id);
        $doc->load($file);
        $meetingkey = $doc->getElementsByTagName('meetingkey');

        foreach($meetingkey as $meetingkey){
            $result['meetingkey'] = $meetingkey->textContent;
        }
        return $result;
  }

  public function GetMeetingData($meetingkey=null,$webex_id=null)
  {
        $result = array();
        $doc = new DOMDocument();
        $file =site_url('webex/GetMeeting/'.$meetingkey.'/'.$webex_id);
        $doc->load($file);
        $meetingName = $doc->getElementsByTagName('confName');
        $meetingPassword = $doc->getElementsByTagName('meetingPassword');
        $hostKey = $doc->getElementsByTagName('hostKey');
        $meetingLink = $doc->getElementsByTagName('meetingLink');
        $meetingkey = $doc->getElementsByTagName('meetingkey');

        foreach($meetingName as $meetingName){
            $result['meetingName'] = $meetingName->textContent;
        }

        foreach($meetingPassword as $meetingPassword){
            $result['meetingPassword'] = $meetingPassword->textContent;
        }

        foreach($hostKey as $hostKey){
            $result['hostKey'] = $hostKey->textContent;
        }

        foreach($meetingLink as $meetingLink){
            $result['meetingLink'] = $meetingLink->textContent;
        }

        foreach($meetingkey as $meetingkey){
            $result['meetingkey'] = $meetingkey->textContent;
        }

        return $result;
  }

}

?>

