<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_Authen
{
	private $CI;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->database();
		$this->CI->load->library('session');
		log_message('debug', "Authen Class Initialized");
	}

	/**
	 * Validaten and sets session variables
	 *
	 * @access	public
	 * @param	string (member_id)
	 * @return	bool
	 */
	public function validate($id)
	{
		//Check if already logged in
		if($this->CI->session->userdata('member_id') == $id)
		{
			$data['authen_msg'] = "existedSignin";
			return $data;
		}

		//Check against user table
		$this->CI->db->where('member_id',$id);
		$query = $this->CI->db->get('member');
		$row=$query->row();
		if($query->num_rows() === 0)
		{
			 $data['authen_msg'] = "notMatch";
			 return $data ;
		}
		else
		{
			if($query->num_rows() === 1)
			{
				if($row->member_status == 0)
				{
					 $data['authen_msg'] = "notActive";
			 		 return $data ;
				}

                //Member Session
				$member_data = array(
					'SessMemberID' => $row->member_id,
                    'SessMemberUser' => $row->member_user,
					'SessMemberFirstname' => $row->member_firstname,
                    'SessMemberLastname' => $row->member_lastname,
                    'SessMemberType' => $row->member_type,
                    'SessPermissionID' => $row->permission_id,
                    'SessMemberApiID' => $row->member_api_id,
					'is_logged_in' => TRUE,
				);

                //SET Session member_data
 				$this->CI->session->set_userdata($member_data);

                //Update loginlog
				$log=array(
				    'member_id' => $row->member_id,
                    'online_status'=>1,
					'login_date' => date('Y-m-d'),
                    'login_time' => date('H:i:s'),
					'ip_address'=>$_SERVER['REMOTE_ADDR']
				);
				$this->CI->db->insert('log_login',$log);

				$data['authen_msg'] = "signinPass";
			 	return $data ;

			}
		}

	}

	public function logout()
	{
		//Unset session data
        $this->CI->session->sess_destroy();
		return true;
	}

    /**
	 * Checking session user is logged in
	 *
	 * @access	public
	 * @return		bool
	 */
	public function is_logged_in()
	{
		$is_logged_in = $this->CI->session->userdata('is_logged_in');

		if(!isset($is_logged_in) || $is_logged_in != TRUE)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

}
?>