<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Pagination Class
*/

class Page
{

	private $CI;
	private $baseUrl = "";
  	private $totalRows = "";
  	private $perPage = "";
  	private $offSet = "";
  	private $uriSegment = "";
  	private $fullTagOpen = "";
  	private $fullTagClose = "";
  	private $numTagOpen = "";
  	private $numTagClose = "";
  	private $curTagOpen = "";
  	private $curTagClose = "";
  	private $nextLink = "";
  	private $nextTagOpen = "";
  	private $nextTagClose = "";
  	private $prevLink = "";
  	private $prevTagOpen = "";
  	private $prevTagClose = "";
  	private $firstLink = "";
  	private $firstTagOpen = "";
  	private $firstTagClose = "";
  	private $lastLink = "";
  	private $lastTagOpen = "";
  	private $lastTagClose = "";
  	private $displayPages = "";
  	private $usePageNumber = "";


	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->library('pagination');
		$this->_default();
	}

	private function _default()
	{
		$this->fullTagOpen = '<ul class="pagination pagination-sm no-margin pull-right">';
		$this->fullTagClose = '</ul>';
		$this->numTagOpen = '<li>';
		$this->numTagClose = '</li>';
		$this->curTagOpen = '<li class="active"><a href="#">';
		$this->curTagClose = '</a></li>';
		$this->nextLink = '&raquo;';
		$this->nextTagOpen = '<li>';
		$this->nextTagClose = '</li>';
		$this->prevLink = '&laquo;';
		$this->prevTagOpen = '<li>';
		$this->prevTagClose = '</li>';
		$this->firstLink = '&laquo;&laquo;';
		$this->firstTagOpen = '<li>';
		$this->firstTagClose = '</li>';
		$this->lastLink = '&raquo;&raquo;';
		$this->lastTagOpen = '<li>';
		$this->lastTagClose = '</li>';
		$this->displayPages = TRUE;
		$this->usePageNumber = TRUE;
	}

	public function setBaseUrl($baseUrl="")
	{
		$this->baseUrl = $baseUrl;
		return $this;
	}

	public function setTotalRows($totalRows="")
	{
		$this->totalRows = $totalRows;
		return $this;
	}

	public function setPerPage($perPage="")
	{
		$this->perPage = $perPage;
		return $this;
	}

	public function setOffSet($offSet="")
	{
		$this->offSet = $offSet;
		return $this;
	}

	public function setUriSegment($uriSegment="")
	{
		$this->uriSegment = $uriSegment;
		return $this;
	}

	public function setFullTagOpen($fullTagOpen="")
	{
		$this->fullTagOpen = $fullTagOpen;
		return $this;
	}

	public function setFullTagClose($fullTagClose="")
	{
		$this->fullTagClose = $fullTagClose;
		return $this;
	}

	public function setNumTagOpen($numTagOpen="")
	{
		$this->numTagOpen = $numTagOpen;
		return $this;
	}

	public function setNumTagClose($numTagClose="")
	{
		$this->numTagClose = $numTagClose;
		return $this;
	}

	public function setCurTagOpen($curTagOpen="")
	{
		$this->curTagOpen = $curTagOpen;
		return $this;
	}

	public function setCurTagClose($curTagClose="")
	{
		$this->curTagClose = $curTagClose;
		return $this;
	}

	public function setNextLink($nextLink="")
	{
		$this->nextLink = $nextLink;
		return $this;
	}

	public function setNextTagOpen($nextTagOpen="")
	{
		$this->nextTagOpen = $nextTagOpen;
		return $this;
	}

	public function setNextTagClose($nextTagClose="")
	{
		$this->nextTagClose = $nextTagClose;
		return $this;
	}

	public function setPrevLink($prevLink="")
	{
		$this->prevLink = $prevLink;
		return $this;
	}

	public function setPrevTagOpen($prevTagOpen="")
	{
		$this->prevTagOpen = $prevTagOpen;
		return $this;
	}

	public function setPrevTagClose($prevTagClose="")
	{
		$this->prevTagClose = $prevTagClose;
		return $this;
	}

	public function setFirstLink($firstLink="")
	{
		$this->firstLink = $firstLink;
		return $this;
	}

	public function setFirstTagOpen($firstTagOpen="")
	{
		$this->firstTagOpen = $firstTagOpen;
		return $this;
	}

	public function setFirstTagClose($firstTagClose="")
	{
		$this->firstTagClose = $firstTagClose;
		return $this;
	}

	public function setLastLink($lastLink="")
	{
		$this->lastLink = $lastLink;
		return $this;
	}

	public function setLastTagOpen($lastTagOpen="")
	{
		$this->lastTagOpen = $lastTagOpen;
		return $this;
	}

	public function setLastTagClose($lastTagClose="")
	{
		$this->lastTagClose = $lastTagClose;
		return $this;
	}

	public function setDisplayPages($displayPages="")
	{
		$this->displayPages = $displayPages;
		return $this;
	}

	public function setConfig()
	{
		$config = array(
			'base_url' => $this->baseUrl,
			'total_rows' => $this->totalRows,
			'per_page' => $this->perPage,
			'uri_segment' => $this->uriSegment,
			'full_tag_open' => $this->fullTagOpen,
			'full_tag_close' => $this->fullTagClose,
			'num_tag_open' => $this->numTagOpen,
			'num_tag_close' => $this->numTagClose,
			'cur_tag_open' => $this->curTagOpen,
			'cur_tag_close' => $this->curTagClose,
			'next_link' => $this->nextLink,
			'next_tag_open' => $this->nextTagOpen,
			'next_tag_close' => $this->nextTagClose,
			'prev_link' => $this->prevLink,
			'prev_tag_open' => $this->prevTagOpen,
			'prev_tag_close' => $this->prevTagClose,
			'first_link' => $this->firstLink,
			'first_tag_open' => $this->firstTagOpen,
			'first_tag_close' => $this->firstTagClose,
			'last_link' => $this->lastLink,
			'last_tag_open' => $this->lastTagOpen,
			'last_tag_close' => $this->lastTagClose,
			'display_pages' => $this->displayPages,
			'use_page_number' => $this->usePageNumber
		);
		return $config;	
	}

}