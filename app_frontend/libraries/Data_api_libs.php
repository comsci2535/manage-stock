<?php defined('BASEPATH') OR exit('No direct script access allowed.');

class Data_api_libs
{

  private $ci;
  public function __construct()
  {
    $this->ci =& get_instance();
    $this->ci->load->database();

    $this->api_server = $this->ci->config->item('api_server');
    $this->connect_api = true;

    $this->token_id = $this->ci->session->userdata('SessTokenID');

    $headers = @get_headers($this->api_server);
    if(strpos($headers[0],'200')===false){
        $this->connect_api = false;
    }

    $rest_config['server'] = $this->api_server;
    $this->ci->rest->initialize($rest_config);

    $token = $this->getToken();
    if($this->token_id == ''){
        $this->token_id = $token->Token_id;
    }

  }

  public function getToken()
  {
    $APIKey = 'ddefd8fe-b999-44df-be23-9ee0e139e673';
    $Password = 'qkq2FDkA';
    $param = array('APIKey'=>$APIKey , 'Password'=>$Password);
    if($this->connect_api===true){
        $result = $this->ci->rest->get('mirorsite/Webservice/api/GetToken',$param);
        $result = (array)$result;
        if(!empty($result)&&$result['Result']===false){
            $result = array();
        }else{
            $result = $result['Message'];
        }
    }else{//not connect api
        $result = array();
    }
    //arr($result);exit();
    return $result;
  }

  //param = array('username'=>'' , 'Password'=>'');
  public function loginStaff($param = array())
  {
    if($this->connect_api===true){
        $result = $this->ci->rest->get('mirorsite/Webservice/api/UserLogin',$param);
        $result = (array)$result;
        if(!empty($result)&&$result['Result']===false){
            $result = array();
        }else{
            $result = $result['Message'][0];
        }
    }else{//not connect api
        $result = array();
    }
    //arr($result);exit();
    return $result;
  }

  //param = array('IdentityID'=>'' , 'Password'=>'');
  public function loginCustomer($param = array())
  {
    if($this->connect_api===true){
        $result = $this->ci->rest->get('mirorsite/Webservice/api/CustomerLogin',$param);
        $result = (array)$result;
        if(!empty($result)&&$result['Result']===false){
            $result = array();
        }else{
            $result = $result['Message'][0];
        }
    }else{//not connect api
        $result = array();
    }
    //arr($result);exit();
    return $result;
  }

  //param = $id;
  public function getCustomerDetails($id)
  {
    $param = array('tokenId'=>$this->token_id , 'id'=>$id);
    if($this->connect_api===true){
        $result = $this->ci->rest->get('mirorsite/Webservice/api/CustomerDetails',$param);
        $result = (array)$result;
        if(!empty($result)&&$result['Result']===false){
            $result = array();
        }else{
            $result = $result['Message'][0];
            if($result->Tel == 'null'){
                $result->Tel = '';
            }
            if($result->Tel_ext == 'null'){
                $result->Tel_ext = '';
            }
        }
    }else{//not connect api
        $result = array();
    }
    //arr($result);exit();
    return $result;
  }

  //param = $CompanyID;
  public function getCompanyDetail($CompanyID)
  {
    $param = array('tokenId'=>$this->token_id , 'CompanyID'=>$CompanyID);
    if($this->connect_api===true){
        $result = $this->ci->rest->get('mirorsite/Webservice/api/CompanyDetail',$param);
        $result = (array)$result;
        if(!empty($result)&&$result['Result']===false){
            $result = array();
        }else{
            $result = $result['Message'];
            $result->Fname = $result->Company_Name;
            $result->Lname = '';
            $result->Tel = $result->Person_Comp_tel;
            $result->Tel_ext = $result->Person_Comp_mobile;
            $result->Fax = $result->Person_Comp_fax;
            $result->Email = $result->Person_Comp_Email;
            $result->ZipCode = $result->PostCode;

            $this->ci->db->where('member_api_id',$CompanyID);
            $this->ci->db->where('member_type',3);
            $member = $this->ci->db->get('member')->row_array();

            if(!empty($member)){
                $result->company_appoint_fullname = $member['company_appoint_fullname'];
                $result->company_appoint_tel = $member['company_appoint_tel'];
                $result->company_appoint_email = $member['company_appoint_email'];
            }else{
                $result->company_appoint_fullname = '';
                $result->company_appoint_tel = '';
                $result->company_appoint_email = '';
            }
        }
    }else{//not connect api
        $result = array();
    }
    //arr($result);exit();
    return $result;
  }

  //param = array();
  public function postCompanyDetail($param = array())
  {
    if($this->connect_api===true){
        $result = $this->ci->rest->post('mirorsite/Webservice/api/CompanyDetail',$param);
        $result = (array)$result;
    }else{//not connect api
        $result = array();
    }
    //arr($result);exit();
    return $result;
  }

  //param = $UserID;
  public function getComplaintlist($UserID)
  {
    $param = array('tokenId'=>$this->token_id , 'UserID'=>$UserID);
    if($this->connect_api===true){
        $result = $this->ci->rest->get('mirorsite/Webservice/api/GetComplaintlist',$param);
        $result = (array)$result;
        if(!empty($result)&&$result['Result']===false){
            $result = array();
        }else{
            $result = $result['Message'];
        }
    }else{//not connect api
        $result = array();
    }
    //arr($result);exit();
    return $result;
  }

  //param = $Case_Id;
  public function getCaseDetail($Case_Id)
  {
    $param = array('tokenId'=>$this->token_id , 'Case_Id'=>$Case_Id);
    if($this->connect_api===true){
        $result = $this->ci->rest->get('mirorsite/Webservice/api/GetCaseDetail',$param);
        $result = (array)$result;
        if(!empty($result)&&$result['Result']===false){
            $result = array();
        }else{
            $result = $result['Message'][0];
        }
    }else{//not connect api
        $result = array();
    }
    //arr($result);exit();
    return $result;
  }

  public function getMasterStatus()
  {
    if($this->connect_api===true){
        $result = $this->ci->rest->get('mirorsite/Webservice/api/MasterStatus');
        $result = (array)$result;
        if(!empty($result)&&$result['Result']===false){
            $result = array();
        }else{
            $result = $result['Message'][0]->Substatus;
        }
    }else{//not connect api
        $result = array();
    }
    //arr($result);exit();
    return $result;
  }

  //param = array();
  public function postUpdateStatus($param = array())
  {
    if($this->connect_api===true){
        $result = $this->ci->rest->post('mirorsite/Webservice/api/UpdateStatus',$param);
        $result = (array)$result;
    }else{//not connect api
        $result = array();
    }
    //arr($result);exit();
    return $result;
  }

  public function GetProvince()
  {
    if($this->connect_api===true){
        $result = $this->ci->rest->get('mirorsite/Webservice/api/GetProvince');
        $result = (array)$result;
        if(!empty($result)&&$result['Result']===false){
            $result = array();
        }else{
            $result = $result['Message'];
        }
    }else{//not connect api
        $result = array();
    }
    //arr($result);exit();
    return $result;
  }

  public function Getprefecture($Province_ID)
  {
    $param = array('Province_ID'=>$Province_ID);
    if($this->connect_api===true && $Province_ID != ''){
        $result = $this->ci->rest->get('mirorsite/Webservice/api/Getprefecture',$param);
        $result = (array)$result;
        if(!empty($result)&&$result['Result']===false){
            $result = array();
        }else{
            $result = $result['Message'];
        }
    }else{//not connect api
        $result = array();
    }
    //arr($result);exit();
    return $result;
  }

  public function Getdistrict($Province_ID,$Prefecture_ID)
  {
    $param = array('Province_ID'=>$Province_ID,'Prefecture_ID'=>$Prefecture_ID);
    if($this->connect_api===true && $Province_ID != '' && $Prefecture_ID != ''){
        $result = $this->ci->rest->get('mirorsite/Webservice/api/Getdistrict',$param);
        $result = (array)$result;
        if(!empty($result)&&$result['Result']===false){
            $result = array();
        }else{
            $result = $result['Message'];
        }
    }else{//not connect api
        $result = array();
    }
    //arr($result);exit();
    return $result;
  }

}

?>

