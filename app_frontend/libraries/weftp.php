<?php
ob_start();
/**
 * This class provides some functions that implements client access to FTP serves.
 *
 * @author		Mr.Happy
 * @license		http://www.ifutureapp.com	GNU Lesser General Public License
 * @version		1.0
 *
 */
class WeFTP
{
	private $host;
	private $user;
	private $pass;

	/**
	 * Constructor
	 *
	 * @param	string	$host	hostname or ip address
	 * @param	string	$user	username
	 * @param	string	$pass	password
	 */
/*	public function __construct($host, $user, $pass)
	{
		$this->host = $host;
		$this->user = $user;
		$this->pass = $pass;
	}*/
	
	public function ftpcon($host, $user, $pass)
	{
		$this->host = $host;
		$this->user = $user;
		$this->pass = $pass;
	}

	/**
	 * Stores a file on the FTP server, showing remaining size, progress bar and uploading speed
	 *
	 * @param	string	$filename	file name
	 * @return	bool	true if transfer success
	 */
	public function upload($file_name, $tmp)
	{			
		if(!file_exists($tmp))
		{
			echo "File: $tmp not exists.";
			return false;
		}
		
		set_time_limit(0);
		$tmp='aa2.mp4';
		$fp = fopen($tmp, 'r');
		$filesize = filesize($tmp) / 1000;
		$elem = rand(0,10000);
			
		//ob_start();

		echo 'Uploading file: <b id="progression'.$elem.'">0</b> / <b>'.ceil($filesize).' KB</b>...<br />'.
			 'Speed: <b id="speed'.$elem.'">0</b> KB/s <br />' .
			 '<span class="progressBar" id="elem'.$elem.'">0</span><br />'.
			 '<script type="text/javascript">$("#elem'.$elem.'").progressBar();</script>';
		ob_flush();
		flush();

		$conn_id = ftp_connect($this->host);
		if(!$conn_id)
		{
			echo "Timeout. Server unreachable.";
			return false;
		}

		if(!ftp_login($conn_id, $this->user, $this->pass))
		{
			echo "Wrong username and/or password.";
			return false;
		}

		// change directory if necessary
		//ftp_chdir($conn_id, "incoming");

		$time_start = microtime(true);
	//	$ret = ftp_put($conn_id, $file_name, $tmp_file, FTP_BINARY);
		$ret = ftp_nb_fput($conn_id, $tmp, $fp, FTP_BINARY);
		
		$prec = 0;

		while ($ret == FTP_MOREDATA)
		{
			$curr_kb = (ftell($fp) / 1000);
			$percent = ceil(($curr_kb / $filesize) * 100);

			if($percent != $prec)
			{
				$prec = $percent;

				$time_end = microtime(true);
				$time = $time_end - $time_start;

				echo "<script>document.getElementById('progression".$elem."').innerHTML = ".ceil($curr_kb).";</script>" .
					 "<script>$('#elem$elem').progressBar($percent);</script>" .
					 "<script>document.getElementById('speed".$elem."').innerHTML = ".ceil($curr_kb/$time).";</script>";

				ob_flush();
				flush();
			}

			$ret = ftp_nb_continue($conn_id);
		}

		if ($ret == FTP_FINISHED)
		{
			$time_end = microtime(true);
			$time = $time_end - $time_start;

			// sleeps for 0.5 seconds (workaround: with small files the percentage could be not set to 100% at the end)
			usleep(500000);

			echo "<script>document.getElementById('progression$elem').innerHTML = ".ceil($filesize)."; " .
				 "document.getElementById('progression$elem').style.color = 'green'; </script>" .
				 "<script>$('#elem$elem').progressBar(100);</script>" .
				 "<span>Execution time: <b>$time</b></span> seconds<br />" .
				 "<span>Average speed: <b>".ceil($filesize/$time)."</b> KB/s</span>";
			ob_flush();
			flush();
		}
		else
		{
			echo "Error uploading file.";
			return false;
		}

		ob_flush();
		flush();
		fclose($fp);
		return true;
	}

}