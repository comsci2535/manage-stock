<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Depreciate Class
 *
 *
 *@package		CodeIgniter
 *@subpackage	Libraries
 *@category		Depreciate
 *@author		Jaruwat Duanjaem
 *@since		Version 1.0
 */

class Depreciate 
{
	private $CI;

	/** Constructor **/

	public function __construct()
	{
		$this->CI =& get_instance();

		log_message('debug', "Depreciate Class Initialized");
	}

	public function getDepreciateDay($price='',$year='',$day='')
	{
		return ($price/$year)*($day/365);
	}

	public function getDepreciateDayLast($price='',$year='',$day='')
	{
		return ($price/$year)*($day/(365-1));
	}

	public function getDepreciate($price='',$year='')
	{
		return ($price/$year);
	}
	
}