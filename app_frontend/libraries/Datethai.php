<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * DateThai Class
 *
 * Makes DateThai simple
 *
 * An open source application development codeigniter framework 2.2.6 for PHP 5.4.16 or newer
 *
 *@package		CodeIgniter
 *@subpackage	Libraries
 *@category		Datetime
 *@author		Jaruwat Duanjaem
 *@since		Version 1.1
 */

class Datethai 
{
	private $CI;
	private $timestamp;
	private $thaiDay;
	private $thaiMonth;
	private $thaiMonthShort;
	private $thaiDate;

	/** Constructor **/

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->thaiDay = array("อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์");     
		$this->thaiMonth=array(     
		    "0"		=>	"",     
		    "1"		=>	"มกราคม",     
		    "2"		=>	"กุมภาพันธ์",     
		    "3"		=>	"มีนาคม",     
		    "4"		=>	"เมษายน",     
		    "5"		=>	"พฤษภาคม",     
		    "6"		=>	"มิถุนายน",      
		    "7"		=>	"กรกฎาคม",     
		    "8"		=>	"สิงหาคม",     
		    "9"		=>	"กันยายน",     
		    "10"	=>	"ตุลาคม",     
		    "11"	=>	"พฤศจิกายน",     
		    "12"	=>	"ธันวาคม"                       
		);     
		$this->thaiMonthShort=array(     
		    "0"		=>	"",     
		    "1"		=>	"ม.ค.",     
		    "2"		=>	"ก.พ.",     
		    "3"		=>	"มี.ค.",     
		    "4"		=>	"เม.ย.",     
		    "5"		=>	"พ.ค.",     
		    "6"		=>	"มิ.ย.",      
		    "7"		=>	"ก.ค.",     
		    "8"		=>	"ส.ค.",     
		    "9"		=>	"ก.ย.",     
		    "10"	=>	"ต.ค.",     
		    "11"	=>	"พ.ย.",     
		    "12"	=>	"ธ.ค."                       
		);     
		log_message('debug', "Datethai Class Initialized");
	}

	public function setDateTime($timestamp)
	{
		$this->timestamp = strtotime($timestamp);
	}

	public function getDateTime()
	{
		return $this->timestamp;
	}

	public function getThaiMonth($month = "")
	{
		return $this->thaiMonth[$month];
	}

	public function getThaiMonthFull()
	{
		return $this->thaiMonth;
	}

	public function getThaiDateTime($timestamp)
	{   
		/* เช็ต datetime จากตัวแปรภายนอก ให้เป็น Timestamp */
		$this->setDateTime($timestamp);
	    $timestamp = date("j",$this->getDateTime());     
	    $timestamp .= " ".$this->thaiMonth[date("n",$this->getDateTime())];     
	    $timestamp .= " ".(date("Y",$this->getDateTime())+543);     
	    $timestamp .= " เวลา ".date("H:i:s",$this->getDateTime());
	    $this->thaiDate = $timestamp;

	    /***************************
	    24 พฤษภาคม 2559 เวลา 15:35:35
	    ***************************/  

	    return $this->thaiDate;
	}

	public function getThaiDateTimeShort($timestamp)
	{   
		/* เช็ต datetime จากตัวแปรภายนอก ให้เป็น Timestamp */
		$this->setDateTime($timestamp);     
	    $timestamp = date("j",$this->getDateTime());     
	    $timestamp .= " ".$this->thaiMonthShort[date("n",$this->getDateTime())];     
	    $timestamp .= " ".(date("Y",$this->getDateTime())+543);     
	    $timestamp .= " ".date("H:i:s",$this->getDateTime()); 
	    $this->thaiDate = $timestamp;

	    /***************************
	    24  พ.ค. 2559 15:35:35
	    ***************************/  

	    return $this->thaiDate;
	}

	public function getDateShort($timestamp)
	{   
		/* เช็ต datetime จากตัวแปรภายนอก ให้เป็น Timestamp */
		$this->setDateTime($timestamp);     
	    $timestamp = date("j",$this->getDateTime());     
	    $timestamp .= " ".$this->thaiMonthShort[date("n",$this->getDateTime())];     
	    $timestamp .= " ".(date("Y",$this->getDateTime())+543);
	    $this->thaiDate = $timestamp;

	    /***************************
	    24  พ.ค. 2559
	    ***************************/

	    return $this->thaiDate;   
	}

	public function getThaiDateFullMonth($timestamp)
	{   
		/* เช็ต datetime จากตัวแปรภายนอก ให้เป็น Timestamp */
		$this->setDateTime($timestamp);
	    $timestamp = date("j",$this->getDateTime());     
	    $timestamp .= " ".$this->thaiMonth[date("n",$this->getDateTime())];     
	    $timestamp .= " ".(date("Y",$this->getDateTime())+543);
	    $this->thaiDate = $timestamp;

	    /***************************
	    24 ธันวาคม 2559 
	    ***************************/

	    return $this->thaiDate;
	}

	public function getThaiDateShortNumber($timestamp)
	{   
		/* เช็ต datetime จากตัวแปรภายนอก ให้เป็น Timestamp */
		$this->setDateTime($timestamp);
	    $timestamp = date("d",$this->getDateTime());     
	    $timestamp .= "-".date("m",$this->getDateTime());     
	    $timestamp .= "-".substr((date("Y",$this->getDateTime())+543),-2); 
	    $this->thaiDate = $timestamp;

	    /***************************
	    24-05-59
	    ***************************/
    
	    return $this->thaiDate;     
	}  

	public function addTime($time='')
	{
		$time = strtotime(date("Y-m-d H:i",strtotime($time)));
		$endTime = date("Y-m-d H:i", strtotime('+30 minutes', $time));
		return $endTime;
	}

	public function addDate($toDate='',$date='')
	{
		$time = strtotime(date("Y-m-d H:i",strtotime($toDate)));
		$endTime = date("Y-m-d H:i", strtotime("+$date days", $time));
		return $endTime;
	}

	public function addHour($type='',$time='')
	{
		$time = strtotime(date("Y-m-d H:i",strtotime($time)));
		switch ($type) {
			case '1':
				$endTime = date("Y-m-d H:i", strtotime('+6 hours', $time));
				break;
			case '2':
				$endTime = date("Y-m-d H:i", strtotime('+4 hours', $time));
				break;
			case '3':
				$endTime = date("Y-m-d H:i", strtotime('+2 hours', $time));
				break;
			default:
				$endTime = date("Y-m-d H:i", strtotime('+6 hours', $time));
				break;
		}
		return $endTime;
	}

	public function diffTime($timeStart='',$timeEnd='')
	{
		$timeStart = new DateTime($timeStart);
		$timeEnd = new DateTime($timeEnd);
		$interval = $timeStart->diff($timeEnd);
		$elapsed = $interval->format('%i นาที');
		return $elapsed;
	}

	public function diffHour($timeStart='',$timeEnd='')
	{
		$timeStart = new DateTime($timeStart);
		$timeEnd = new DateTime($timeEnd);
		$interval = $timeStart->diff($timeEnd);
		$elapsed = $interval->format('%d วัน %H ชั่วโมง %i นาที');
		return $elapsed;
	}

	public function diffMinute($timeStart='',$timeEnd='')
	{
		$timeStart = new DateTime($timeStart);
		$timeEnd = new DateTime($timeEnd);
		$interval = $timeStart->diff($timeEnd);
		$elapsed = (($interval->format('%d')*24*60) + ($interval->format('%H')*60) + ($interval->format('%i')));
		return $elapsed;
	}

	public function diffDayLast($timeStart='',$timeEnd='')
	{
		$timeStart = new DateTime($timeStart);
		$timeEnd = new DateTime($timeEnd);
		$interval = $timeStart->diff($timeEnd);
		$elapsed = $interval->format('%a');
		return $elapsed;
	}

	public function diffHourLast($timeStart='',$timeEnd='')
	{
		$timeStart = new DateTime($timeStart);
		$timeEnd = new DateTime($timeEnd);
		$interval = $timeStart->diff($timeEnd);
		$elapsed = $interval->format('%H');
		return $elapsed;
	}

	public function getEndBudgetYearThai($timestamp='')
	{
		$this->setDateTime($timestamp);     
	    $timestamp = "30";     
	    $timestamp .= " ".$this->thaiMonthShort[9];     
	    $timestamp .= " ".(date("Y",$this->getDateTime())+543);
	    $this->thaiDate = $timestamp;
		return $this->thaiDate;

	}

	public function getLastBudgetYearThai($timestamp='',$year=0)
	{
		$this->setDateTime($timestamp);     
	    $timestamp = (date("j",$this->getDateTime())+1);     
	    $timestamp .= " ".$this->thaiMonthShort[date("n",$this->getDateTime())];     
	    $timestamp .= " ".(date("Y",$this->getDateTime())+543+$year);
	    $this->thaiDate = $timestamp;
		return $this->thaiDate;

	}

	public function getEndBudgetYear($timestamp='')
	{
		$this->setDateTime($timestamp);     
	    $timestamp = date("Y",$this->getDateTime())."-09-30";
		return $timestamp;

	}

	public function getEndBudgetYearPlus($timestamp='',$totalYear=0)
	{
		$this->setDateTime($timestamp);     
	    $timestamp = (date("Y",$this->getDateTime())+$totalYear)."-09-30";
		return $timestamp;

	}

	public function getEndBudgetYearPlusThai($timestamp='',$totalYear=0)
	{
		$this->setDateTime($timestamp);   
		$timestamp = "30";  
	    $timestamp .= " ".$this->thaiMonthShort[9];     
	    $timestamp .= " ".(date("Y",$this->getDateTime())+543+$totalYear);
		return $timestamp;

	}
	
}