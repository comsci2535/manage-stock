<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * EquipmentUtility Class
 *
 *
 *@package		CodeIgniter
 *@subpackage	Libraries
 *@category		EquipmentUtility
 *@author		Jaruwat Duanjaem
 *@since		Version 1.0
 */

class EquipmentUtility 
{
	private $CI;

	/** Constructor **/

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->database();
		$this->CI->load->library('session');

		log_message('debug', "EquipmentUtility Class Initialized");
	}

	public function getPurchaseTotal($budgetYear = '', $materialID = '' , $price = '')
	{
		$query = $this->CI->db->select('sum(input_detail_value) as detail_value')
			->join('eq_input_detail','eq_input_detail.input_id = eq_input.input_id')
			->where('input_budget_year',$budgetYear)
			->where('eq_input_detail.material_id',$materialID)
			->where('eq_input_detail.input_detail_tax_price',$price)
			->get('eq_input')
			->row_array();
		if (!empty($query)) {
			return $query['detail_value'] != "" ? $query['detail_value'] : 0;
		}
		return "-";
	}

	public function getInput($budgetYear = array(), $materialID = '')
	{
		$query = $this->CI->db->select('material_id,input_detail_tax_price,SUM(input_detail_value) AS detail_value,SUM(input_detail_use) AS detail_use')
			->join('eq_input','eq_input.input_id = eq_input_detail.input_id')
			// ->where_in('eq_input_detail.input_detail_tax_price'," select input_detail_tax_price from eq_input_detail group by input_detail_tax_price having count(*) > 1",false)
			->where_in('input_budget_year',$budgetYear)
			->where('eq_input_detail.material_id',$materialID)
			->group_by('material_id,input_detail_tax_price');
		/* Pemission Access */
		if($this->CI->session->userdata('MemberViewData')=='all'){
			
		}else if($this->CI->session->userdata('MemberViewData')=='all_zone'){
			$query = $query->where('eq_input.zone_id',$this->CI->session->userdata('MemberZone'));
			
		}else if($this->CI->session->userdata('MemberViewData')=='all_lv1'){
			$query = $query->where('eq_input.org_lv1',$this->CI->session->userdata('MemberOrg_lv1'));
			
		}else if($this->CI->session->userdata('MemberViewData')=='all_lv2'){
			$query = $query->where('eq_input.org_lv2',$this->CI->session->userdata('MemberOrg_lv2'));

		}else if($this->CI->session->userdata('MemberViewData')=='all_lv3'){
			$query = $query->where('eq_input.org_lv3',$this->CI->session->userdata('MemberOrg_lv3'));

		}else if($this->CI->session->userdata('MemberViewData')=='single'){
			$query = $query->where('eq_input.org_lv1',$this->CI->session->userdata('MemberOrg_lv1'));

		}
		$query = $query->get('eq_input_detail');
		return $query;
	}

	public function getQuotedTotal($budgetYear = '', $materialID = '', $price = '')
	{
		$budgetYear = $budgetYear - 1;
		$year = $budgetYear - 543;
		$date = $year."-09-30";
		$query = $this->CI->db->select('SUM(input_detail_value) AS detail_value,SUM(input_detail_use) AS detail_use')
			->join('eq_input_detail','eq_input_detail.input_id = eq_input.input_id')
			->where('input_budget_year',$budgetYear)
			->where('eq_input_detail.material_id',$materialID)
			->where('eq_input_detail.input_detail_tax_price',$price)
			->get('eq_input')
			->row_array();
		$order = $this->CI->db->select('sum(order_detail_recieve) as detail_recieve')
			->join('eq_order_detail','eq_order_detail.order_id = eq_order.order_id')
			->where('order_datetime <=',$date)
			->where('eq_order_detail.material_id',$materialID)
			->get('eq_order')
			->row_array();
		if (!empty($query) && !empty($order)) {
			return $query['detail_value'] - $order['detail_recieve'] != "" ? $query['detail_value'] - $order['detail_recieve'] : 0;
		}
		return "-";
	}

	public function getUsedTotal($budgetYear = '', $materialID = '')
	{
		$order = $this->CI->db->select('sum(order_detail_recieve) as detail_recieve')
			->join('eq_order_detail','eq_order_detail.order_id = eq_order.order_id')
			->where('order_budget_year',$budgetYear)
			->where('eq_order_detail.material_id',$materialID)
			->get('eq_order')
			->row_array();
		if (!empty($order)) {
			return $order['detail_recieve'] != "" ? $order['detail_recieve'] : 0;
		}
		return "-";
	}

	public function getPricePerUnit($budgetYear = '', $materialID = '')
	{
		$query = $this->CI->db->select('input_detail_price')
			->join('eq_input_detail','eq_input_detail.input_id = eq_input.input_id')
			->where('input_budget_year',$budgetYear)
			->where('eq_input_detail.material_id',$materialID)
			->get('eq_input')
			->row_array();
		if (!empty($query)) {
			return $query['input_detail_price'] != "" ? $query['input_detail_price'] : 0;
		}
		return 0;
	}

	public function getUsedTotalByOrderID($orderID = '', $materialID = '')
	{
		$order = $this->CI->db->select('sum(order_detail_recieve) as detail_recieve')
			->join('eq_order_detail','eq_order_detail.order_id = eq_order.order_id')
			->where('eq_order_detail.material_id',$materialID)
			->where('eq_order_detail.order_id',$orderID)
			->get('eq_order')
			->row_array();
		if (!empty($order)) {
			return $order['detail_recieve'] != "" ? $order['detail_recieve'] : 0;
		}
		return "-";
	}

	public function getStockByMaterialID($materialID = '')
	{
		$stock = $this->CI->db->select('*')
			->where('material_id',$materialID)
			->get('eq_stock')
			->row_array();
		if (!empty($stock)) {
			return $stock['stock_avalible'] - $stock['stock_use'];
		}
		return 0;
	}

	/*public function getBringMonth($materialID = '' , $month = '' , $price = '')
	{
		$inputs = $this->CI->db->select('SUM(input_detail_value) AS input_total')
			->join('eq_input_detail','eq_input_detail.input_id = eq_input.input_id')
			->join('eq_order_detail','eq_order_detail.material_id = eq_input_detail.material_id','left')
			->join('eq_order','eq_order.order_id = eq_order_detail.order_id','left')
			->where('eq_input_detail.material_id',$materialID)
			->where('eq_order.order_datetime <',date('Y-m-d'))
			->where('eq_input_detail.input_detail_tax_price',$price)
			->get('eq_input')
			->result();
		echo "<pre>";
		echo $this->CI->db->last_query();
		echo "</pre>";
		$total = 0;
		if (!empty($inputs)) {
			foreach ($inputs as $key => $input) {
				$total = $total + $input->input_total;	
			}
			return $total;
		}
		return "-";
	}*/

	# Old Function For Restore
	public function getBringMonth($materialID = '' , $month = '' , $price = '')
	{
		// $inputs = $this->CI->db->select('SUM(input_detail_value - input_detail_use) AS input_total')
		$inputs = $this->CI->db->select('SUM(input_detail_value) AS input_total')
			->join('eq_input_detail','eq_input_detail.input_id = eq_input.input_id')
			->where('eq_input_detail.material_id',$materialID)
			// ->where('MONTH(eq_input.input_datetime) <',$month)
			->where('eq_input_detail.input_detail_tax_price',$price)
			->get('eq_input')
			->result();
		$total = 0;
		if (!empty($inputs)) {
			foreach ($inputs as $key => $input) {
				$total = $total + $input->input_total;	
			}
			return $total;
		}
		return "-";
	}

	public function getBringMonthAll($materialID = '' , $dateStart = '', $dateEnd = '')
	{
		$query = $this->CI->db->select('SUM(input_detail_value) AS input_total')
			->join('eq_input_detail','eq_input_detail.input_id = eq_input.input_id')
			->where('eq_input_detail.material_id',$materialID)
			->where('eq_input.input_datetime <',$dateStart);
		if($this->CI->session->userdata('MemberViewData')=='all'){
			
		}else if($this->CI->session->userdata('MemberViewData')=='all_zone'){
			$query = $query->where('eq_input.zone_id',$this->CI->session->userdata('MemberZone'));
			
		}else if($this->CI->session->userdata('MemberViewData')=='all_lv1'){
			$query = $query->where('eq_input.org_lv1',$this->CI->session->userdata('MemberOrg_lv1'));
			
		}else if($this->CI->session->userdata('MemberViewData')=='all_lv2'){
			$query = $query->where('eq_input.org_lv2',$this->CI->session->userdata('MemberOrg_lv2'));

		}else if($this->CI->session->userdata('MemberViewData')=='all_lv3'){
			$query = $query->where('eq_input.org_lv3',$this->CI->session->userdata('MemberOrg_lv3'));

		}else if($this->CI->session->userdata('MemberViewData')=='single'){
			$query = $query->where('eq_input.org_lv1',$this->CI->session->userdata('MemberOrg_lv1'));

		}
		$query = $query->get('eq_input')
			->result();
		$inputs = $query;
		$total = 0;
		if (!empty($inputs)) {
			foreach ($inputs as $key => $input) {
				$total = $total + $input->input_total;	
			}
				$queryOrder = $this->CI->db->select('SUM(order_detail_recieve) AS order_recieve')
				->join('eq_order_detail','eq_order_detail.order_id = eq_order.order_id')
				->where('eq_order_detail.material_id',$materialID)
				->where('eq_order.order_datetime <',$dateStart);
			if($this->CI->session->userdata('MemberViewData')=='all'){
				
			}else if($this->CI->session->userdata('MemberViewData')=='all_zone'){
				$queryOrder = $queryOrder->where('eq_order.zone_id',$this->CI->session->userdata('MemberZone'));
				
			}else if($this->CI->session->userdata('MemberViewData')=='all_lv1'){
				$queryOrder = $queryOrder->where('eq_order.org_lv1',$this->CI->session->userdata('MemberOrg_lv1'));
				
			}else if($this->CI->session->userdata('MemberViewData')=='all_lv2'){
				$queryOrder = $queryOrder->where('eq_order.org_lv2',$this->CI->session->userdata('MemberOrg_lv2'));

			}else if($this->CI->session->userdata('MemberViewData')=='all_lv3'){
				$queryOrder = $queryOrder->where('eq_order.org_lv3',$this->CI->session->userdata('MemberOrg_lv3'));

			}else if($this->CI->session->userdata('MemberViewData')=='single'){
				$queryOrder = $queryOrder->where('eq_order.org_lv1',$this->CI->session->userdata('MemberOrg_lv1'));

			}
			$queryOrder = $queryOrder->get('eq_order')
				->result();
			$use = 0;
			if (!empty($queryOrder)) {
				foreach ($queryOrder as $key => $queryOrderItem) {
					$use = $use + $queryOrderItem->order_recieve;	
				}
			}
			return $total - $use;
		}
		return "-";
	}

	public function getInputMonth($materialID = '' , $month = '' , $price = '', $budgetYear = '')
	{
		$inputs = $this->CI->db->select('SUM(input_detail_value) AS input_total')
			->join('eq_input_detail','eq_input_detail.input_id = eq_input.input_id')
			->where('eq_input_detail.material_id',$materialID)
			->where('MONTH(eq_input.input_datetime)',$month)
			->where('eq_input.input_budget_year',$budgetYear)
			->where('eq_input_detail.input_detail_tax_price',$price)
			->get('eq_input')
			->result();
		$total = 0;
		if (!empty($inputs)) {
			foreach ($inputs as $key => $input) {
				$total = $total + $input->input_total;	
			}
			return $total;
		}
		return "-";
	}

	public function getOrderMonth($materialID = '' , $dateEnd = '')
	{
		$query = $this->CI->db->select('SUM(order_detail_recieve) AS input_total')
			->join('eq_order_detail','eq_order_detail.order_id = eq_order.order_id')
			->where('eq_order_detail.material_id',$materialID)
			->where('eq_order.order_datetime <=',$dateEnd);
		if($this->CI->session->userdata('MemberViewData')=='all'){
			
		}else if($this->CI->session->userdata('MemberViewData')=='all_zone'){
			$query = $query->where('eq_order.zone_id',$this->CI->session->userdata('MemberZone'));
			
		}else if($this->CI->session->userdata('MemberViewData')=='all_lv1'){
			$query = $query->where('eq_order.org_lv1',$this->CI->session->userdata('MemberOrg_lv1'));
			
		}else if($this->CI->session->userdata('MemberViewData')=='all_lv2'){
			$query = $query->where('eq_order.org_lv2',$this->CI->session->userdata('MemberOrg_lv2'));

		}else if($this->CI->session->userdata('MemberViewData')=='all_lv3'){
			$query = $query->where('eq_order.org_lv3',$this->CI->session->userdata('MemberOrg_lv3'));

		}else if($this->CI->session->userdata('MemberViewData')=='single'){
			$query = $query->where('eq_order.org_lv1',$this->CI->session->userdata('MemberOrg_lv1'));

		}
		$query = $query->get('eq_order')
			->result();
		$inputs = $query;
		$total = 0;
		if (!empty($inputs)) {
			foreach ($inputs as $key => $input) {
				$total = $total + $input->input_total;	
			}
			return $total;
		}
		return "-";
	}

	public function getRemainMonth($materialID = '' , $month = '' , $price = '')
	{
		$inputs = $this->CI->db->select('SUM(input_detail_value) AS input_total')
			->join('eq_input_detail','eq_input_detail.input_id = eq_input.input_id')
			->where('eq_input_detail.material_id',$materialID)
			->where('eq_input_detail.input_detail_tax_price',$price)
			->get('eq_input')
			->result();
		$total = 0;
		if (!empty($inputs)) {
			foreach ($inputs as $key => $input) {
				$total = $total + $input->input_total;	
			}
			$orders = $this->CI->db->select('SUM(order_detail_recieve) AS input_total')
				->join('eq_order_detail','eq_order_detail.order_id = eq_order.order_id')
				->where('eq_order_detail.material_id',$materialID)
				->where('MONTH(eq_order.order_datetime)',$month)
				->get('eq_order')
				->result();
			$remain = 0;
			if (!empty($orders)) {
				foreach ($orders as $key => $order) {
					$remain = $remain + $order->input_total;	
				}
				return $total - $remain;
			}
			return $total;
		}
		return "-";
	}
	
}