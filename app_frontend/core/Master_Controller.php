<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Master_Controller extends MX_Controller
{
	public function __construct() {
		parent::__construct();
		$this->template->set_layout('template/layout/template')
	        //***** CSS *****
			->css('assets/admin/bootstrap/css/bootstrap.min.css')
	        ->css('assets/admin/font-awesome-4.7.0/css/font-awesome.min.css')
			->css('assets/admin/dist/css/AdminLTE.css')
			->css('assets/admin/dist/css/skins/_all-skins.min.css')
	        ->css('assets/admin/plugins/jquery-ui/css/jquery-ui.min.css')
	        ->css('assets/admin/plugins/toast/css/jquery.toast.css')
	        ->css('assets/admin/dist/css/switch-field.css')
	        ->css('assets/admin/dist/css/group-buttons.css')
	        ->css('assets/admin/plugins/checkbox-style/animated-radios-amp-checkboxes-no.css')
	        ->css('assets/admin/fancyapps-fancyBox/source/jquery.fancybox.css?v=2.1.5')
	        ->css('assets/admin/fancyapps-fancyBox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5')
	        ->css('assets/admin/fancyapps-fancyBox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7')
	        ->css('assets/admin/dist/css/reset.css')
	        ->css('assets/admin/dist/css/header.css')
	        ->css('assets/admin/dist/css/base_font.css')
	        
	        //***** JAVA SCRIPT Plugins *****
	        ->js('assets/admin/plugins/jquery/jquery-2.1.4.min.js')
	        ->js('assets/admin/plugins/jquery-ui/js/jquery-ui.min.js')
	        ->js('assets/admin/plugins/toast/js/jquery.toast.js')
	        ->js('assets/admin/fancyapps-fancyBox/lib/jquery.mousewheel-3.0.6.pack.js')
	        ->js('assets/admin/fancyapps-fancyBox/source/jquery.fancybox.js?v=2.1.5')
	        ->js('assets/admin/fancyapps-fancyBox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5')
	        ->js('assets/admin/fancyapps-fancyBox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7')
	        ->js('assets/admin/fancyapps-fancyBox/js/plugin_lightbox.js')
	        ->js('assets/admin/dist/js/demo.js')
	        ->js_foot('assets/admin/bootstrap/js/bootstrap.min.js')
	        ->js_foot('assets/admin/dist/js/app.min.js')
	        ->js_foot('assets/admin/plugins/alert-bootbox/bootbox.min.js')

	        //***** Include Template *****
		    ->set_view('header_right','template/header_right')
			->set_view('notices','template/notices')
			->set_view('footer','template/footer');
	}
}