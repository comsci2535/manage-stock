<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/* The MX_Controller class is autoloaded as required */
class MY_Controller extends MX_Controller
{
	function __construct() {
		parent::__construct();
		
		if(!$this->admin_authen->is_logged_in()){
			redirect('admin/login');
		}
	}
	
	protected function pagin($uri, $total, $segment, $per_page = 30, $num_links = 8) {
        $config['base_url'] = site_url($uri);
        $config['per_page'] = $per_page;
        $config['total_rows'] = $total;
        $config['uri_segment'] = $segment;
		$config['num_links'] = $num_links;
		$config['full_tag_open'] = '<div class="pagination"><ul><li>';
		$config['full_tag_close'] = '</li></ul></div>';
		$config['cur_tag_open'] = '<a class="active">';
		$config['cur_tag_close'] = '</a>';
		$config['first_link'] = '« First';
		$config['last_link'] = 'Last »';
	 	$config['next_link'] = '&gt;';
		$config['prev_link'] = '&lt;';

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }
	
	public function webfile()
	{
	  //file strore image and doc
		$webfile_url_=base_url();
		return $webfile_url_;
	}
	
	public function pathfile()
	{
	  //file strore image and doc
		$webfile_url_=FCPATH;
		return $webfile_url_;
	}
}
/*END*/
