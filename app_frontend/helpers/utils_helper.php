<?php defined('BASEPATH') OR exit('No direct script access allowed.');

/**
 * CodeIgniter Date Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Philip Sturgeon
 */

// ------------------------------------------------------------------------


function engWeekDay($date){

	$weekDayEng = date("D", strtotime($date));

	return $weekDayEng;
}

function thaiWeekDay($date){

	$weekDayEng = engWeekDay($date);
	$thai_day_arr=array(
		"Sun"=>"อาทิตย์",
		"Mon" => "จันทร์",
		"Tue" => "อังคาร",
		"Wed" => "พุธ",
		"Thu" => "พฤหัสบดี",
		"Fri" => "ศุกร์",
		"Sat" => "เสาร์");


	return "วัน".$thai_day_arr[$weekDayEng];
}



function check_date($date){

	$explode = explode("-", $date);
	if(count($explode) != 3){
		return false;
	} else {
		return checkdate($explode[1],$explode[2],$explode[0]);
	}

}

function format_date($unix, $format = 'Y-m-d')
{
	if ($unix == '' || ! is_numeric($unix))
	{
		$unix = strtotime($unix);
	}

	return strstr($format, '%') !== FALSE
		? ucfirst(utf8_encode(strftime($format, $unix))) //or? strftime($format, $unix)
		: date($format, $unix);
}

function date_now()
{
	date_default_timezone_set('Asia/Bangkok');
	$date_now=date("Y-m-d H:i:s");
	return $date_now;
}

function yearThai_only($strDate)
{
	$strYear = date("Y",strtotime($strDate))+543;
	return " $strYear";
}

function DateThai($strDate)
{
	if($strDate != '' && $strDate!='1900-01-01'&&$strDate!="0000-00-00 00:00:00")
	{
		  $strYear = date("Y",strtotime($strDate));
		  $strMonth= date("m",strtotime($strDate));
		  $strDay= date("d",strtotime($strDate));
		return "$strDay-$strMonth-$strYear";
	}else{
		return '-';
	}

}

function general_date($strDate)
{
	if($strDate != '' && $strDate!='1900-01-01'&&$strDate!="0000-00-00 00:00:00")
	{
	   return date('Y-m-d H:i:s',strtotime($strDate));
	}else{
		return '0000-00-00 00:00:00';
	}
}

function general_date_list($strDate)
{
	if($strDate != '' && $strDate!='1900-01-01'&&$strDate!="0000-00-00 00:00:00")
	{
	   return date('Y-m-d H:i:s',strtotime($strDate));
	}else{
		return '';
	}
}

function date_list($strDate)
{
	if($strDate != '' && $strDate!='1900-01-01'&&$strDate!="0000-00-00 00:00:00")
	{
	  // return date('d/m/Y H:i',strtotime($strDate));
	    $strYear = date("Y",strtotime($strDate))+543;
		  $strMonth= date("m",strtotime($strDate));
		  $strDay= date("d",strtotime($strDate));
		 return "$strDay/$strMonth/$strYear";
	}else{
		return '';
	}
}

function DateThai2($strDate)
{
	if(!empty($strDate)){
		$strDate=explode('-',$strDate);
		$strYear = trim($strDate[0]+543);
		$strMonth= trim($strDate[1]);
		$strDay= trim($strDate[2]);

		return "$strDay/$strMonth/$strYear";
	}else{
		return '';
	}
}

function Date_eng($strDate)
{
	if(!empty($strDate)){
		$ex=explode('-',$strDate);
		$strYear = $ex[2]-543;
		$strMonth= $ex[1];
		$strDay=$ex[0];
		return "$strYear-$strMonth-$strDay";
	}else{
		return '';
	}
}

function Date_eng2($strDate)
{
	if(!empty($strDate)){
		$ex=explode('/',$strDate);
		$strYear = $ex[2]-543;
		$strMonth= $ex[1];
		$strDay=$ex[0];
		return "$strYear-$strMonth-$strDay";
	}else{
		return '';
	}
}
//**หาวัน*********************************
function DateDiff($strDate1,$strDate2)
{
    return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
}

function DateThaiFull($strDate)
{
  $strYear = date("Y",strtotime($strDate))+543;
  $strMonth= date("n",strtotime($strDate));
  $strDay= date("j",strtotime($strDate));
  $strHour= date("H",strtotime($strDate));
  $strMinute= date("i",strtotime($strDate));
  $strSeconds= date("s",strtotime($strDate));
  $strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
  $strMonthThai=$strMonthCut[$strMonth];
  return " $strDay $strMonthThai $strYear";
}

function DateEngFull($strDate)
{
	$strYear = date("Y",strtotime($strDate));
	$strMonth= date("n",strtotime($strDate));
	$strDay= date("j",strtotime($strDate));
	$strHour= date("H",strtotime($strDate));
	$strMinute= date("i",strtotime($strDate));
	$strSeconds= date("s",strtotime($strDate));
	$strMonthCut = Array("","January","February","March","April","May","June","July","August","September","October","November","December");
	$strMonthThai=$strMonthCut[$strMonth];
	return " $strDay $strMonthThai $strYear";
}
	
function DateThaiFull_not_day($strDate)
{
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strMonthThai $strYear";
}
	


//ปีงบประมาณ

function fiscalYear($date) {
   // วันที่ที่ต้องการตรวจสอบ
   list($year, $month, $day) = explode("-", $date);
   // วันที่ที่ส่งมา (mktime)
   $cday = mktime(0, 0, 0, $month, $day, $year);
   // ปีงบประมาณตามค่าที่ส่งมา (mktime)
   $d1 = mktime(0, 0, 0, 10, 1, $year);
   // ปีใหม่
   $d2 = mktime(0, 0, 0, 1, 1, $year + 1);
   if ($cday >= $d1 && $cday < $d2) {
     // 1 ตค. - 31 ธค.
     $year++;
   }
   return $year;
}

function thainumDigit($num)
{
	return str_replace(array( '0' , '1' , '2' , '3' , '4' , '5' , '6' ,'7' , '8' , '9' ),
	array( "๐" , "๑" , "๒" , "๓" , "๔" , "๕" , "๖" , "๗" , "๘" , "๙" ),$num);
}

function ThaiDate2($strDate) {
	if($strDate!='')
	{
			$strYear = date("Y",strtotime($strDate))+543;
			$strMonth= date("n",strtotime($strDate));
			$strDay= date("j",strtotime($strDate));
			$strHour= date("H",strtotime($strDate));
			$strMinute= date("i",strtotime($strDate));
			$strSeconds= date("s",strtotime($strDate));
			$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
			$strMonthThai=$strMonthCut[$strMonth];
			return "$strDay $strMonthThai $strYear";
	}else{
		return '';
	}
	
}

function thaiMonth($mm) {
		$strMonth= intval($mm);
		$strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
		$strMonthThai=$strMonthCut[$strMonth];
		 return "$strMonthThai";
}

function EnglishMonth($mm) {
		$strMonth= intval($mm);
		$strMonthCut = Array("","January","February","March","April","May","June","July","August","September","October","November","December");
		$strMonthThai=$strMonthCut[$strMonth];
		 return "$strMonthThai";
}

function DateThaiFull_Time($strDate)
{
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay$strMonthThai$strYear $strHour:$strMinute:$strSeconds";
}
	
function exdate($start_date, $date_num)
{
	$startdatec = strtotime($start_date);
	$tod = $date_num*86400;
	$ndate = $startdatec + $tod;
	return date("Y-m-d",$ndate);
}

function date_diff_($str_start, $str_end)
{
	if($str_start !='' && $str_end!='')
	{
		$str_start = strtotime($str_start); // ทำวันที่ให้อยู่ในรูปแบบ timestamp
		$str_end = strtotime($str_end); // ทำวันที่ให้อยู่ในรูปแบบ timestamp
		
		$nseconds = $str_end - $str_start; // วันที่ระหว่างเริ่มและสิ้นสุดมาลบกัน
		$ndays = round($nseconds / 86400); // หนึ่งวันมี 86400 วินาที

		return $ndays;
	}else{return '';}
}

function thai_date($time){
	$thai_day_arr=array("อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์");
	$thai_month_arr=array(
		"0"=>"",
		"1"=>"มกราคม",
		"2"=>"กุมภาพันธ์",
		"3"=>"มีนาคม",
		"4"=>"เมษายน",
		"5"=>"พฤษภาคม",
		"6"=>"มิถุนายน",	
		"7"=>"กรกฎาคม",
		"8"=>"สิงหาคม",
		"9"=>"กันยายน",
		"10"=>"ตุลาคม",
		"11"=>"พฤศจิกายน",
		"12"=>"ธันวาคม"					
	);
	$thai_date_return="วัน".$thai_day_arr[date("w",$time)];
	$thai_date_return.=	"ที่ ".date("j",$time);
	$thai_date_return.=" ".$thai_month_arr[date("n",$time)];
	$thai_date_return.=	" พ.ศ.".(date("Y",$time)+543);
	//$thai_date_return.=	"  ".date("H:i",$time)." น.";
	return $thai_date_return;
}

function ThaiLongDate($strDate)
{
	  $thai_day_arr=array("อาทิตย์ที่","จันทร์ที่","อังคารที่","พุธที่","พฤหัสบดีที่","ศุกร์ที่","เสาร์ที่");
	  $strYear = date("Y",strtotime($strDate))+543;
	  $strMonth= date("n",strtotime($strDate));
	  $strDay= date("j",strtotime($strDate));
	  $strHour= date("H",strtotime($strDate));
	  $strMinute= date("i",strtotime($strDate));
	  $strSeconds= date("s",strtotime($strDate));
	  $thai_date_return="วัน".$thai_day_arr[date("w",strtotime($strDate))];
	  $strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
	  $strMonthThai=$strMonthCut[$strMonth];
	  return "$thai_date_return $strDay $strMonthThai $strYear";


}

function ThaiLongDate2($strDate)
{
	  $strYear = date("Y",strtotime($strDate))+543;
      $strMonth= date("n",strtotime($strDate));
      $strDay= date("j",strtotime($strDate));
      $strHour= date("H",strtotime($strDate));
      $strMinute= date("i",strtotime($strDate));
      $strSeconds= date("s",strtotime($strDate));
      $strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
      $strMonthThai=$strMonthCut[$strMonth];
      return " $strDay เดือน $strMonthThai พ.ศ. $strYear";
}

function ThaiDate($strDate)
{
	  $strYear = date("Y",strtotime($strDate))+543;
      $strMonth= date("n",strtotime($strDate));
      $strDay= date("j",strtotime($strDate));
      $strHour= date("H",strtotime($strDate));
      $strMinute= date("i",strtotime($strDate));
      $strSeconds= date("s",strtotime($strDate));
      $strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
      $strMonthThai=$strMonthCut[$strMonth];
      return " $strDay $strMonthThai $strYear";
}


function Day_inweek($strDate)
{
	$a= $strDate;
	
	$b= array('','Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
	
	return $c= $b[$a];
}

function Day_inweekEng($strDate)
{
	$a= $strDate;
	
	$b= array('','Sun','Mon','Tue','Wed','Thu','Fri','Sat');
	
	return $c= $b[$a];
}

function Day_inweekEngFull($strDate)
{
	$a= $strDate;
	
	$b= array('','Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
	
	return $c= $b[$a];
}

function Time_sec($time)
{
	if($time!=""){
		$time_img=explode(':',$time);
		$strH = $time_img[0]*60*60;
		$strI = $time_img[1]*60;
		$strS = $time_img[2];
		
		$sec=$strH+$strI+$strS;
		return $sec;
	}else{
		return 0;
	}
}

//rand_string----------------------
function rand_string( $length ) {
	
	$str="";
	$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";	

	$size = strlen( $chars );
	for( $i = 0; $i < $length; $i++ ) {
		$str .= $chars[ rand( 0, $size - 1 ) ];
	}

	return $str;
}

if( ! function_exists('htmlencode'))
{
	function htmlencode($str) 
	{
		return htmlspecialchars(stripcslashes(trim($str)), ENT_QUOTES, 'UTF-8');
	}
}

if( ! function_exists('htmldecode'))
{
	function htmldecode($str) 
	{
		return html_entity_decode($str);
	}
}

function d2m($month) { 
	$year = date("Y"); 
return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31); 
}


function dayInWeek($day=0,$month=0,$year=0)
{
	// ต้นเดือนจะเป็นวันที่ 1 เสมอ
	$first = 1;
	 
	// หาวันสุดท้ายของเดือน
	$last = cal_days_in_month(CAL_GREGORIAN, $month, $year);
	 
	// สร้างตัวแปรไว้เก็บวันจันทร์
	$target_day = array();
	 
	for ($i = $first; $i <= $last; $i++) {
		//เช็กว่าเป็นวันจันทร์หรือเปล่า ถ้าใช่เก็บไว้ในตัวแปร
		$cur = strtotime($month . "/" . $i . "/" . $year);
	 
		if (date("D", $cur) == $day) {
			$target_day[] = $i;
		}
	}
	return $target_day; 
	// แสดงวันที่ๆเป็นวันจันทร์ ทั้งหมดออกมา
}

function convertDate($date)
{
	$array = array('0'=>"",'1'=>'Sun','2'=>'Mon','3'=>'Tue','4'=>'Wed','5'=>'Thu','6'=>'Fri','7'=>'Sat');

     $key = array_search($date,$array); // $key = 2;
	 return $key;
}

function banText($arr1,$arr2,$text){
	$chat_sid='';
	$block=array($arr1);
	$block2=array($arr2);
	for($i=0;$i<count($block);$i++)
	{
		$chat_sid = str_ireplace($block[$i], $block2[$i],$text);
	}
	return  $chat_sid;
}

function check_Thumbnail($param = '',$type=NULL) {
    if ($param != '') {
        return $param;
    } else {
    	switch ($type) {
    		case 'banner': return base_url() . '/assets/public_images/defaultpic/bannerDefault.png';break;
    		default: return base_url() . '/assets/public_images/defaultpic/bannerDefault.png'; break;
    	}
        
    }
}


function dayimg_inweek()
{
	$d= array('0'=>'Sunday','1'=>'Monday','2'=>'Tuesday','3'=>'Wednesday','4'=>'Thursday','5'=>'Friday','6'=>'Saturday');
	return $d;
}



if (!function_exists('arrx')) {

    function arrx($arr, $arr2 = array()) {
        header('Content-Type: text/html; charset=utf-8');
        echo "<pre>";
        print_r($arr);
        if (!empty($arr2))
            print_r($arr2);
        echo "</pre>";
        exit();
    }

}

if (!function_exists('arr')) {

    function arr($arr, $arr2 = array()) {
        echo "<pre>";
        print_r($arr);
        if (!empty($arr2))
            print_r($arr2);
        echo "</pre>";
    }

}

function Atth_pic( $file ) 
{
	$extendsion = explode(".", $file);
	$extendsion = $extendsion[(count($extendsion) - 1)];
	switch ($extendsion) {
	case "jpg":  $img ='<img src="'.base_url('assets/public_images/iconfile/jpg.png').'">' ;      break;
    case "jpeg": $img ='<img src="'.base_url('assets/public_images/iconfile/jpeg.png').'">';  break;
    case "gif":  $img ='<img src="'.base_url('assets/public_images/iconfile/gif.png').'">';  break;
    case "png":  $img ='<img src="'.base_url('assets/public_images/iconfile/png.png').'">';  break;
    case "doc":  $img ='<img src="'.base_url('assets/public_images/iconfile/doc.png').'">'; break;
    case "docx": $img ='<img src="'.base_url('assets/public_images/iconfile/docx.png').'">'; break;
    case "ppt":  $img ='<img src="'.base_url('assets/public_images/iconfile/ppt.png').'">';break;
	case "pptx": $img ='<img src="'.base_url('assets/public_images/iconfile/pptx.png').'">'; break; 
    case "xls":  $img ='<img src="'.base_url('assets/public_images/iconfile/xls.png').'">'; break;
    case "xlsx": $img ='<img src="'.base_url('assets/public_images/iconfile/xlsx.png').'">'; break;
    case "rar":  $img ='<img src="'.base_url('assets/public_images/iconfile/rar.png').'">'; break;
    case "zip":  $img ='<img src="'.base_url('assets/public_images/iconfile/zip.png').'">'; break;
    case "pdf":  $img ='<img src="'.base_url('assets/public_images/iconfile/pdf.png').'">'; break;                                           
    default:$img ='<img src="'.base_url('assets/public_images/iconfile/doc.png').'">'; break;
    } 
    return $img;
}
 
function tagu($l , $w=""){
	$t = $w;
	$w = str_replace('ี',"",$w);
	$w = str_replace("ั","",$w);
	$w = str_replace("่","",$w);
	$strl = mb_strlen($w);
	// echo ($strl);
	$l = $l - $strl;
	$txt = "<u>";
	for ( $i=1; $i<=$l; $i++ ) {
		if ( $i == 2 ) $txt .= "{$t}";
		$txt .= "&nbsp;";
	}
	$txt .= "</u>";
	return $txt;
}

function NumberThaiCharacter($number)
{
    //$number = number_format($number, 2, '.', '');
    $numberx = $number;
    $txtnum1 = array('ศูนย์','หนึ่ง','สอง','สาม','สี่','ห้า','หก','เจ็ด','แปด','เก้า','สิบ');
    $txtnum2 = array('','สิบ','ร้อย','พัน','หมื่น','แสน','ล้าน','สิบ','ร้อย','พัน','หมื่น','แสน','ล้าน');
    $number = str_replace(",","",$number);
    $number = str_replace(" ","",$number);
    $number = str_replace("บาท","",$number);
    $number = explode(".",$number);

    if(sizeof($number)>2){
    return 'ทศนิยมหลายตัวนะจ๊ะ';
    exit;
    }
    $strlen = strlen($number[0]);
    $convert = '';
    for($i=0;$i<$strlen;$i++){
    	$n = substr($number[0], $i,1);
    	if($n!=0){
    		if($i==($strlen-1) AND $n==1){ $convert .= 'เอ็ด'; }
    		elseif($i==($strlen-2) AND $n==2){  $convert .= 'ยี่'; }
    		elseif($i==($strlen-2) AND $n==1){ $convert .= ''; }
    		else{ $convert .= $txtnum1[$n]; }
    		$convert .= $txtnum2[$strlen-$i-1];
    	}
    }

    $convert .= 'บาท';
    if(empty($number[1]) || $number[1]=='0' || $number[1]=='00'){
    $convert .= 'ถ้วน';
    }else{
    $strlen = strlen($number[1]);
    for($i=0;$i<$strlen;$i++){
    $n = substr($number[1], $i,1);
    	if($n!=0){
    	if($i==($strlen-1) AND $n==1){$convert
    	.= 'เอ็ด';}
    	elseif($i==($strlen-2) AND
    	$n==2){$convert .= 'ยี่';}
    	elseif($i==($strlen-2) AND
    	$n==1){$convert .= '';}
    	else{ $convert .= $txtnum1[$n];}
    	$convert .= $txtnum2[$strlen-$i-1];
    	}
    }
    $convert .= 'สตางค์';
    }
    //แก้ต่ำกว่า 1 บาท ให้แสดงคำว่าศูนย์ แก้ ศูนย์บาท
    if($numberx < 1)
    {
    	$convert = "ศูนย์" .  $convert;
    }

    //แก้เอ็ดสตางค์
    $len = strlen($numberx);
    $lendot1 = $len - 2;
    $lendot2 = $len - 1;
    if(($numberx[$lendot1] == 0) && ($numberx[$lendot2] == 1))
    {
    	$convert = substr($convert,0,-10);
    	$convert = $convert . "หนึ่งสตางค์";
    }

    //แก้เอ็ดบาท สำหรับค่า 1-1.99
    if($numberx >= 1)
    {
    	if($numberx < 2)
    	{
    		$convert = substr($convert,4);
    		$convert = "หนึ่ง" .  $convert;
    	}
    }
    return $convert;
}

function NumberTH($number=NULL)
{
  return str_replace(array( '0' , '1' , '2' , '3' , '4' , '5' , '6' ,'7' , '8' , '9' ),
  array( "o" , "๑" , "๒" , "๓" , "๔" , "๕" , "๖" , "๗" , "๘" , "๙" ),
  $number);
}



?>  

