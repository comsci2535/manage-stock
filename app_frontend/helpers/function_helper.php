<?php defined('BASEPATH') OR exit('No direct script access allowed.');


function MonthFullThai($numofmont){

	$numofmont = (int)$numofmont;

	$strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");

	if(array_key_exists($numofmont, $strMonthCut)){
		return $strMonthCut[$numofmont];
	} else {
		return "";
	}

}


function DateFormatThai($strDate)
{

	// $strDate = "2051-10-01";

	if($strDate != '' && $strDate!='1900-01-01')
	{
          $date = new DateTime($strDate);
          $strYear = $date->format('Y')+543;
		  $strMonth= $date->format('m');
		  $strDay= $date->format('d');
		return "$strDay/$strMonth/$strYear";
	}

}

function DateFormatSQL($strDate)
{
	if($strDate != '' && $strDate!='01/01/2443')
	{
	    list($dd,$mm,$yyyy) = explode("/",$strDate);
		$yyyy -= 543;
		$enddate = $yyyy."-".$mm."-".$dd;
		return $enddate;
	}

}

function Removespecialchars($str){
    $str = preg_replace("#[^ก-๙a-zA-Z0-9 <>=/\:;-]#u", '', $str);
    $string = str_replace('"','',$str);
    return str_replace("'",'',$str);
}

function remove_specialchars($str){
    $str = preg_replace("#[^ก-๙a-zA-Z0-9 <>=/\:;-]#u", '', $str);
    $string = str_replace('"','',$str);
    return str_replace("'",'',$str);
}

function ms_escape_string($data) {
    if ( !isset($data) or empty($data) ) return '';
    if ( is_numeric($data) ) return $data;

    $non_displayables = array(
        '/%0[0-8bcef]/',            // url encoded 00-08, 11, 12, 14, 15
        '/%1[0-9a-f]/',             // url encoded 16-31
        '/[\x00-\x08]/',            // 00-08
        '/\x0b/',                   // 11
        '/\x0c/',                   // 12
        '/[\x0e-\x1f]/'             // 14-31
    );
    foreach ( $non_displayables as $regex )
    $data = preg_replace( $regex, '', $data );
    $data = str_replace("'", "''", $data );
    return $data;
}

function calWorkAge($startWorkdate){
	// $bday = new DateTime($startWorkdate); // Your date of birth
	// $today = new Datetime(date('Y-m-d'));
	// $diff = $today->diff($bday);

	$diff = calDiffAge($startWorkdate, date('Y-m-d'));

	return $diff;
}

function calDiffAge($starDate, $endDate){

	$explode = explode("-", $starDate);
	$startM = (int)trim($explode[1]);
	$startTextM =  $startM < 10 ? "0".$startM : $startM;
	$startD = (int)trim($explode[2]);
	$startTextD =  $startD < 10 ? "0".$startD : $startD;
	$startY = (int)trim($explode[0]);

	$starDate = $startY."-".$startTextM."-".$startTextD;

	$explode2 = explode("-", $endDate);
	$endM = (int)trim($explode2[1]);
	$endTextM =  $endM < 10 ? "0".$endM : $endM;
	$endD = (int)trim($explode2[2]);
	$endTextD =  $endD < 10 ? "0".$endD : $endD;
	$endY = (int)trim($explode2[0]);
	$endDate = $endY."-".$endTextM."-".$endTextD;

	$isDate =  checkdate ( $startM , $startD , $startY );	
	$isDate2 =  checkdate ( $endM , $endD , $endY );

	// print $starDate."<br>";
	// print $endDate."<br><br>";

	if(!$isDate){
		// $diff
		$diff = array();
		$diff['y'] = 0;
		$diff['d'] = 0;
		$diff['m'] = 0;
		$diff['h'] = 0;
		$diff['i'] = 0;
		$diff['s'] = 0;
		$diff['weekday'] = 0;
		$diff['weekday_behavior'] = 0;
		$diff['first_last_day_of'] = 0;
		$diff['invert'] = 0;
		$diff['days'] = 0;
		$diff['special_type'] = 0;
		$diff['special_amount'] = 0;
		$diff['have_weekday_relative'] = 0;
		$diff['have_special_relative'] = 0;
		
		return (object)$diff;
	} else {

		$bday = new DateTime($starDate); // Your date of birth
		$today = new Datetime($endDate);
		$diff = $today->diff($bday);

		// print "<pre>";
		// print_r($diff);
		// exit();
		return $diff;
	}
}

function calDiffAgeText($starDate, $endDate){
	
	$diff = calDiffAge($starDate, $endDate);
	$text = ""; 




	if($diff->y > 0){
		$text .= $diff->y." ปี ";
	}

	if($diff->m > 0){
		$text .= $diff->m." เดือน ";
	}

	$text .= $diff->d." วัน";


	return $text;

}


function calWorkAgeText($startWorkdate){
	
	$diff = calWorkAge($startWorkdate);
	$text = ""; 


	if($diff->y > 0){
		$text .= $diff->y." ปี ";
	}

	if($diff->m > 0){
		$text .= $diff->m." เดือน ";
	}

	$text .= $diff->d." วัน";


	return $text;
	
}

function number_exp($num=0)
	{  $num1=0;
	    $num2="";
		$num_round=0;
		if($num){
			$num_round=sprintf("%.2f",$num);
			$num_arr=explode(".",$num_round);
			if(!empty($num_arr)){
				if(!empty($num_arr[1])&&$num_arr[1]!="00"){
					$num2='.'.$num_arr[1];
				}
				$num1=$num_arr[0].''.$num2;
			}
		}
		return $num1;

	}

function helDiffFullTime($start='00:00:00',$end='00:00:00')
{
    $date = new DateTime('1970-1-1 '.$start);
    $date2 = new DateTime('1970-1-1 '.$end);

    $diffInSeconds = $date2->getTimestamp() - $date->getTimestamp();

    $thistime = $diffInSeconds;
    $hour = floor($thistime/3600);
    $T_minute = $thistime % 3600;

    $minute = floor($T_minute / 60);
    $second = $T_minute % 60;

    $fullTime = '';
    if($hour != 0){
      $fullTime .= $hour.' ชั่วโมง ';
    }
    if($minute != 0 || $second != 0){
      $fullTime .= $minute.' นาที ';
    }
    if($second != 0){
      $fullTime .= $second.' วินาที';
    }

    return $fullTime;
}

function trim_string($str='')
{
    $newStr = preg_replace('/[[:space:]]+/', '', trim($str));
    return $newStr;
}


?>

