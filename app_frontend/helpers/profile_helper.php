<?php defined('BASEPATH') OR exit('No direct script access allowed.');

function profile_member($member_id=null)
{
    $CI=&get_instance();
    $CI->load->database();
    if($member_id == NULL){
        $member_id = ($CI->session->userdata('SessMemberID'))? $CI->session->userdata('SessMemberID') : 0;
    }

    $_where = NULL;
    $_where .= "WHERE member_id = '".$member_id."'";
    $sql = "SELECT CONCAT(member_firstname, ' ', member_lastname) AS fullname FROM member ".$_where;
    $result = $CI->db->query($sql)->row_array();

    if(!empty($result) && $result['fullname'] != ''){
        return $result['fullname'];
    }else{
        return '';
    }
}

function profile_avatar($member_id=null)
{
    $CI=&get_instance();
    $CI->load->database();
    if($member_id == NULL){
        $member_id = ($CI->session->userdata('SessMemberID'))? $CI->session->userdata('SessMemberID') : 0;
    }

    $_where = NULL;
    $_where .= "WHERE member_id = '".$member_id."'";
    $sql = "SELECT member_avatar FROM member ".$_where;
    $result = $CI->db->query($sql)->row_array();

    if(!empty($result) && $result['member_avatar'] != ''){
        return base_url('uploads/avatar/'.$result['avatar']);
    }else{
        return base_url('assets/icon_logo/avatar/user.png'); ;
    }
}

?>  

