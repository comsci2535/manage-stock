<?php defined('BASEPATH') OR exit('No direct script access allowed.');

function web_title1()
{
    /*$CI=&get_instance();
    $CI->load->database();
    $result = $CI->db->get("setting_web")->row_array();
    if(!empty($result) && $result['setting_title1'] != ''){
        return $result['setting_title1'];
    }else{
        return '';
    }*/
    return 'LMS ระบบการจัดการเรียนรู้';
}

function web_title2()
{
    /*$CI=&get_instance();
    $CI->load->database();
    $result = $CI->db->get("setting_web")->row_array();
    if(!empty($result) && $result['setting_title2'] != ''){
        return $result['setting_title2'];
    }else{
        return '';
    }*/

    return '';
}

function web_meta_keyword()
{
    $CI=&get_instance();
    $CI->load->database();
    $result = $CI->db->get("setting_web")->row_array();
    if(!empty($result) && $result['setting_meta_keyword'] != ''){
        return $result['setting_meta_keyword'];
    }else{
        return '';
    }
}

function web_meta_description()
{
    $CI=&get_instance();
    $CI->load->database();
    $result = $CI->db->get("setting_web")->row_array();
    if(!empty($result) && $result['setting_meta_description'] != ''){
        return $result['setting_meta_description'];
    }else{
        return '';
    }
}

function web_logo()
{
    $CI=&get_instance();
    $CI->load->database();
    $result = $CI->db->get("setting_web")->row_array();
    if(!empty($result) && $result['setting_logo'] != ''){
        return 'uploads/logo/'.$result['setting_logo'];
    }else{
        return '';
    }
}

function web_icon()
{
    $CI=&get_instance();
    $CI->load->database();
    $result = $CI->db->get("setting_web")->row_array();
    if(!empty($result) && $result['setting_logo'] != ''){
        return 'uploads/logo/'.$result['setting_logo'];
    }else{
        return '';
    }
}

// array : module , ref_id , action
function insert_log_work($data=null)
{
    $CI=&get_instance();
    $CI->load->database();
    $SessMemberID = ($CI->session->userdata('SessMemberID'))? $CI->session->userdata('SessMemberID') : null;

    if($data['action'] == 'update'){
        $data['action_txt'] = 'แก้ไขข้อมูล';
    }else if($data['action'] == 'save'){
        $data['action_txt'] = 'เพิ่มข้อมูล';
    }else if($data['action'] == 'hide'){
        $data['action_txt'] = 'ลบข้อมุล';
    }else if($data['action'] == 'change_active'){
        $data['action_txt'] = 'แก้ไขสถานะใช้งาน';
    }else if($data['action'] == 'change_order'){
        $data['action_txt'] = 'จัดลำดับใหม่';
    }else{
        $data['action_txt'] = 'อื่นๆ';
    }

    $data['member_id'] = $SessMemberID;
    $data['log_datetime'] = date("Y-m-d H:i:s");
    $data['ip_address'] = $_SERVER['REMOTE_ADDR'];

    if($CI->db->insert('log_work', $data)){
        return true;
    }
}

function web_user_online()
{
    $CI=&get_instance();
    $CI->load->database();

    $sql = "SELECT LPAD(COUNT(DISTINCT(member_id)),7,0) AS total
    FROM log_login
    WHERE login_date = '".date("Y-m-d")."'
    AND online_status = 1";

    $result = $CI->db->query($sql)->row_array();
    if(!empty($result) && $result['total'] != ''){
        return $result['total'];
    }else{
        return '';
    }
}

function web_views()
{
    $CI=&get_instance();
    $CI->load->database();

    $sql = "SELECT LPAD(COUNT(DISTINCT(id)),7,0) AS total
    FROM log_work";

    $result = $CI->db->query($sql)->row_array();
    if(!empty($result) && $result['total'] != ''){
        return $result['total'];
    }else{
        return '';
    }
}

function check_permission($menu_main=null,$menu_sub=array(),$type=null)
{
    $CI=&get_instance();
    $CI->load->database();
    $SessPermissionID = ($CI->session->userdata('SessPermissionID'))? $CI->session->userdata('SessPermissionID') : 0;

    $CI->db
    ->select('permission_accessible')
    ->where('permission_id',$SessPermissionID);
    $result = $CI->db->get('permission')->row_array();

    if(!empty($result)){
        $jsonText = $result['permission_accessible'];
        $decodedText = html_entity_decode($jsonText);
        $accessibleArray = json_decode($decodedText, true);
        foreach($menu_sub as $value){
            $accessible = explode(",",$accessibleArray[$menu_main][$value]);
            if(!empty($accessible)){
                if($type=='view' && $accessible[0]==1){//แสดง
                    return true;exit();
                }else if($type=='manage' && $accessible[1]==1){//จัดการข้อมุล
                    return true;exit();
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

    }else{
        return false;
    }
}

//parameter : array('module'=>'','type'='','size'=>'');
function image_size($param=array())
{
    $CI=&get_instance();
    $CI->load->database();

    $CI->db->where('SizeImgWork',$param['module']);
    $result = $CI->db->get('size_image')->row_array();
    if(!empty($result)){
        if($param['type'] == 'W'){
            if($param['size'] == 'xl'){
                return $result['SizeImgWidth'];
            }else if($param['size'] == 'sm'){
                return $result['SizeImgResizeWidth'];
            }
        }else if($param['type'] == 'H'){
            if($param['size'] == 'xl'){
                return $result['SizeImgHight'];
            }else if($param['size'] == 'sm'){
                return $result['SizeImgResizeHight'];
            }
        }else{
            return '';
        }
    }else{
        return '';
    }
}
?>

