<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo web_title1().' '.web_title2();?> | <?php echo $template['title'];?></title>
    <link rel="icon" href="<?php echo base_url();?>uploads/logo/ico/favicon.ico" type="image/x-icon">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

    <base href="<?php echo base_url();?>"/>
    <link href="<?php echo base_url();?>assets/admin/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/dist/css/AdminLTE.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/dist/css/skins/_all-skins.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/plugins/toast/css/jquery.toast.css" rel="stylesheet" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/plugins/switch-field/switch-field.css" rel="stylesheet" rel="stylesheet">

    <?php echo $template['css'];?>
    <link href="<?php echo base_url();?>assets/admin/dist/css/reset_style_master.css" rel="stylesheet">

    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/jquery/jquery-3.1.1.min.js"></script>
    <?php echo $template['js'];?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="hold-transition sidebar-collapse skin-black-light">

  <div class="wrapper">
      <header class="main-header">
        <a class="logo" title="<?php echo web_title1().' '.web_title2();?>">
            <img src="<?php echo base_url();?>uploads/logo/logo.png" title="">
        </a>
        <nav class="navbar navbar-static-top" role="navigation">
        </nav>
      </header>



      <div class="content-wrapper">
        <section class="content">
            <?php echo $template['breadcrumb'];?>
            <div class="row">
              <!-- left column -->
              <div class="col-md-12">
                <!-- general form elements -->
                <div class="box">
                  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                  <?php echo $template['body'];?>
                </div>
                <!-- /.box -->
              </div>
            </div>
        </section>
      </div>

      <footer class="main-footer">
        <?php echo $template['footer'];?>
      </footer>
  </div>

  <script type="text/javascript" src="<?php echo base_url();?>assets/admin/bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/alert-bootbox/bootbox.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/toast/js/jquery.toast.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/sparkline/jquery.sparkline.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/fastclick/fastclick.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/admin/dist/js/app.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/admin/dist/js/pages/dashboard.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/admin/dist/js/demo.js"></script>
  <script type="text/javascript">
    $('a').tooltip({boundary: 'window' });
    $('button').tooltip();
  </script>
  <?php echo $template['js_foot'];?>
  <?php echo $template['dev_script'];?>

</body>

</html>