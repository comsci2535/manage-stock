<div id="shortcuts"></div>
<?php if ($this->session->flashdata('error')): ?>
<script>
$.toast({
    heading: 'เกิดข้อผิดพลาด',
    text: '<span style="font-size: 14px"><?php echo $this->session->flashdata("error"); ?></span>',
    showHideTransition: 'fade',
    icon: 'warning',
    position: 'bottom-right',
    hideAfter: 2000,
    toastBg: '#dd4b39',
    loader: false,
});
</script>
<?php endif; ?>

<?php if ($this->session->flashdata('info')): ?>
<script>
$.toast({
    heading: 'เกิดข้อผิดพลาด',
    text: '<span style="font-size: 14px"><?php echo $this->session->flashdata("info"); ?></span>',
    showHideTransition: 'fade',
    icon: 'warning',
    position: 'bottom-right',
    hideAfter: 2000,
    toastBg: '#dd4b39',
    loader: false,
});
</script>
<?php endif; ?>

<?php if ($this->session->flashdata('success')){ ?>
<script>
$.toast({
    heading: 'สำเร็จ',
    text: '<span style="font-size: 14px"><?php echo $this->session->flashdata("success"); ?></span>',
    showHideTransition: 'fade',
    icon: 'success',
    position: 'bottom-right',
    hideAfter: 2000,
    toastBg: '#00a65a',
    loader: false,
});
</script>
<?php } ?>

<?php if ( ! empty($messages['error'])): ?>
<script>
$.toast({
    heading: 'Error',
    text: '<span style="font-size: 14px"><?php echo $messages["error"]; ?></span>',
    showHideTransition: 'fade',
    icon: 'warning',
    position: 'bottom-right',
    hideAfter: 2000,
    toastBg: '#dd4b39',
    loader: false,
});
</script>
<?php endif; ?>

<?php if ( ! empty($messages['success'])): ?>
<script>
$.toast({
    heading: 'สำเร็จ',
    text: '<span style="font-size: 14px"><?php echo $messages["success"]; ?></span>',
    showHideTransition: 'fade',
    icon: 'success',
    position: 'bottom-right',
    hideAfter: 2000,
    toastBg: '#5bc0de',
    loader: false,
});
</script>
<?php endif; ?>

<!--MODAL-->

<style type="text/css">
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}

.my_width .modal-dialog{
    width:25%;
}
</style>

<div class="modal fade" id="modal_formLoad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content" style="border: 0px solid rgba(0,0,0,.2);text-align: center;box-shadow: 0 0px 0px rgba(0,0,0,0.5);background-color: transparent;">
            <img src="<?php echo base_url();?>assets/icon_logo/loading/spinner.gif" style="width: 2.2em;">
            <p style="color: #fff;font-size: 1.2em;"><strong>กำลังโหลดข้อมูล </strong>,กรุณารอสักครู่...</p>
          </div>
      </div>
</div>

<div class="modal fade" id="modal_formSave" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content" style="border: 0px solid rgba(0,0,0,.2);text-align: center;box-shadow: 0 0px 0px rgba(0,0,0,0.5);background-color: transparent;">
            <img src="<?php echo base_url();?>assets/icon_logo/loading/spinner.gif" style="width: 2em;">
            <p style="color: #fff;font-size: 1.2em;"><strong>กำลังบันทึกข้อมูล </strong>,กรุณารอสักครู่...</p>
          </div>
      </div>
</div>

<div class="modal fade" id="modal_formRemove" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content" style="border: 0px solid rgba(0,0,0,.2);text-align: center;box-shadow: 0 0px 0px rgba(0,0,0,0.5);background-color: transparent;">
            <img src="<?php echo base_url();?>assets/icon_logo/loading/spinner.gif" style="width: 2em;">
            <p style="color: #fff;font-size: 1.2em;"><strong>กำลังลบข้อมูล </strong>,กรุณารอสักครู่...</p>
          </div>
      </div>
</div>
<!--MODAL-->