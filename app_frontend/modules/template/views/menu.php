<section class="sidebar">
  <ul class="sidebar-menu">
    <li>
      <a href="<?php echo site_url();?>" target="_blank">
      	<i class="fa fa-home"></i><span> เข้าสู่หน้าเว็บไซต์</span>
      </a>
    </li>

    <?php if(check_permission('backend',array('setting_web','about','vision','faq','social','setting_mail'),'view') == true){//helpers/setting_web ?>
    <li class="treeview <?php if(!empty($menu_active)&&$menu_active=='setting'){echo 'active';}?>">
      <a href="#">
        <i class="fa fa-cog"></i> <span>ตั้งค่าเริ่มต้น</span> <i class="fa fa-angle-left pull-right"></i>
      </a>
      <ul class="treeview-menu">
        <?php if(check_permission('backend',array('setting_web'),'view') == true){//helpers/setting_web ?>
        <li class="<?php if(!empty($submenu_active)&&$submenu_active=='setting_web'){echo 'active';}?>"><a href="<?php echo site_url('admin/setting_web/index/1');?>"><i class="fa fa-circle-o"></i> ตั้งค่าเว็บไซต์</a></li>
        <?php } ?>
        <?php if(check_permission('backend',array('about'),'view') == true){//helpers/setting_web ?>
        <li class="<?php if(!empty($submenu_active)&&$submenu_active=='about'){echo 'active';}?>"><a href="<?php echo site_url('admin/about');?>"><i class="fa fa-circle-o"></i> เกี่ยวกับเรา</a></li>
        <?php } ?>
        <?php if(check_permission('backend',array('vision'),'view') == true){//helpers/setting_web ?>
        <li class="<?php if(!empty($submenu_active)&&$submenu_active=='vision'){echo 'active';}?>"><a href="<?php echo site_url('admin/vision');?>"><i class="fa fa-circle-o"></i> วิสัยทัศน์และพันธกิจ</a></li>
        <?php } ?>
        <?php if(check_permission('backend',array('faq'),'view') == true){//helpers/setting_web ?>
        <li class="<?php if(!empty($submenu_active)&&$submenu_active=='faq'){echo 'active';}?>"><a href="<?php echo site_url('admin/faq');?>"><i class="fa fa-circle-o"></i> จัดการข้อมูล FAQs</a></li>
        <?php } ?>
        <?php if(check_permission('backend',array('social'),'view') == true){//helpers/setting_web ?>
        <li class="<?php if(!empty($submenu_active)&&$submenu_active=='social'){echo 'active';}?>"><a href="<?php echo site_url('admin/social');?>"><i class="fa fa-circle-o"></i> ลิงค์ที่เกี่ยวข้อง</a></li>
        <?php } ?>
        <?php if(check_permission('backend',array('setting_mail'),'view') == true){//helpers/setting_web ?>
        <li class="<?php if(!empty($submenu_active)&&$submenu_active=='setting_mail'){echo 'active';}?>"><a href="<?php echo site_url('admin/setting_mail/index/1');?>"><i class="fa fa-circle-o"></i> ตั้งค่า Email</a></li>
        <?php } ?>

      </ul>
    </li>
    <?php } ?>

    <?php //if(check_permission('backend',array('information'),'view') == true){//helpers/setting_web ?>
    <li class="treeview <?php if(!empty($menu_active)&&$menu_active=='information'){echo 'active';}?>">
      <a href="#">
        <i class="fa fa-folder"></i> <span>ข้อมูลการศึกษา</span> <i class="fa fa-angle-left pull-right"></i>
      </a>
      <ul class="treeview-menu">
        <li class="<?php if(!empty($submenu_active)&&$submenu_active=='term'){echo 'active';}?>"><a href="<?php echo site_url('admin/term');?>"><i class="fa fa-circle-o"></i> เทอม</a></li>
        <li class="<?php if(!empty($submenu_active)&&$submenu_active=='faculty'){echo 'active';}?>"><a href="<?php echo site_url('admin/faculty');?>"><i class="fa fa-circle-o"></i> คณะ</a></li>
        <li class="<?php if(!empty($submenu_active)&&$submenu_active=='major'){echo 'active';}?>"><a href="<?php echo site_url('admin/major');?>"><i class="fa fa-circle-o"></i> สาขาวิชา</a></li>
        <li class="divider"></li> 
        <li class="<?php if(!empty($submenu_active)&&$submenu_active=='period'){echo 'active';}?>"><a href="<?php echo site_url('admin/period');?>"><i class="fa fa-circle-o"></i> คาบการเรียนการสอน</a></li>
        <li class="<?php if(!empty($submenu_active)&&$submenu_active=='classroom'){echo 'active';}?>"><a href="<?php echo site_url('admin/classroom');?>"><i class="fa fa-circle-o"></i> ห้องเรียน</a></li>
        <li class="divider"></li>
        <li class="<?php if(!empty($submenu_active)&&$submenu_active=='course'){echo 'active';}?>"><a href="<?php echo site_url('admin/course');?>"><i class="fa fa-circle-o"></i> หลักสูตร</a></li>
        <li class="<?php if(!empty($submenu_active)&&$submenu_active=='subjects'){echo 'active';}?>"><a href="<?php echo site_url('admin/subjects');?>"><i class="fa fa-circle-o"></i> รายวิชา</a></li>
      </ul>
    </li>
    <?php //} ?>

    <?php //if(check_permission('backend',array('poll'),'view') == true){//helpers/setting_web ?>
    <li class="<?php if(!empty($menu_active)&&$menu_active=='poll'){echo 'active';}?>">
      <a href="<?php echo site_url('admin/poll');?>">
      	<i class="fa fa-star-half-o"></i><span> แบบสำรวจความคิดเห็น</span>
      </a>
    </li>
    <?php //} ?>



  </ul>
</section>