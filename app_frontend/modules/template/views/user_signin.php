<ul class="nav navbar-nav">
  <!-- User Account: style can be found in dropdown.less -->
  <li class="dropdown user user-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
      <img src="<?php echo profile_avatar();?>" class="user-image">
      <span class="hidden-xs"><?php echo $this->session->userdata('SessMemberFirstname').' '.$this->session->userdata('SessMemberLastname');?></span>
      <b class="caret"></b>
    </a>
    <ul class="dropdown-menu">
      <!-- User image -->
      <li class="user-header">
        <img src="<?php echo profile_avatar();?>" class="img-circle">
        <p>
          <?php echo $this->session->userdata("SessMemberFirstname").' '.$this->session->userdata("SessMemberLastname");?><br>
        </p>
      </li>
      <!-- Menu Footer-->
      <li class="user-footer">
        <div class="pull-left">
          <a href="<?php echo site_url('admin/profile');?>" class="btn btn-default btn-flat"><i class="fa fa-user-circle-o"></i> ข้อมูลส่วนตัว</a>
        </div>
        <div class="pull-right">
          <a href="<?php echo site_url('authen/logout');?>" class="btn btn-default btn-flat"><i class="fa fa-power-off"></i> ออกจากระบบ</a>
        </div>
      </li>
    </ul>
  </li>
  <!-- Control Sidebar Toggle Button -->
</ul>