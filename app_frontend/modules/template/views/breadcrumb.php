<div id="breadcrumb-wrap">
  <ol class="breadcrumb">
  <?php if(!empty($setting_bread['bread_name'])){ ?>

  <?php foreach ($setting_bread['bread_name'] as $bread_index => $bread_item){
        if($bread_index==0){
          $active =  ($setting_bread['bread_active'] == $bread_index)? 'active' : '';
          $color_active = ($active == '' )? "style='color: #3c8dbc;'" : '';
          echo "<i class='".$page_icon." ".$active."' ".$color_active."></i>&nbsp;";
        }
        if($setting_bread['bread_active'] == $bread_index){
          echo "<li class='active'><span style='font-weight: bold;'> ";
        }else{
          echo "<li>";
          echo "<a href=".$setting_bread['bread_link'][$bread_index]." style='font-weight: bold;'>";
        }
        echo $bread_item;
        if($setting_bread['bread_active'] != $bread_index){
          echo "</a>";
        }
        echo"</span></li>";
      }
  ?>
  <?php } ?>
  </ol>
</div>
