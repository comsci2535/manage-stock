<script type="text/javascript">
$(document).ready(function() {
    var site_url = '<?php echo site_url();?>';
    $('#bt_login').click(function(){
      $('#icon-loading').html('<img src="<?php echo base_url();?>assets/icon_logo/loading/loading.gif" style="width:18%;">&nbsp;&nbsp;');
      var txt_username = $('input[name="txt_username"]').val();
      var txt_password = $('input[name="txt_password"]').val();
      $('div[id="message_authen"]').html('');
      $('div[id="message_username"]').html('');
      $('div[id="message_password"]').html('');

            $.post('<?php echo site_url('authen/check_authen');?>',{txt_username:txt_username,txt_password:txt_password,csrf_test_name:$('input[name="csrf_test_name"]').val()},function(data){
              $('#icon-loading').html('');
              if(data.message_authen != ''){
                  $('div[id="message_authen"]').html(data.message_authen);
              }else if(data.message_username != '' && data.message_password != ''){
                  $('div[id="message_username"]').html(data.message_username);
                  $('div[id="message_password"]').html(data.message_password);
              }else if(data.message_username != ''){
                  $('div[id="message_username"]').html(data.message_username);
              }else if(data.message_password != ''){
                  $('div[id="message_password"]').html(data.message_password);
              }else{
                  window.location.href= site_url+"authen/authen_member/"+data.member_id;
              }
            },'json');
            return false
  });
});

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

function validator()
{
    var txt_username = $('input[name="txt_username"]').val();
    var txt_password = $('input[name="txt_password"]').val();
    if(txt_username != ''){
        $('div[id="message_username"]').html('');
    }
    if(txt_password != ''){
        $('div[id="message_password"]').html('');
    }
}

$(document).on('click', '#bt_confirm', function() {
    var site_url = '<?php echo site_url();?>';
    /*$("#modal_formLoad").modal({
        "backdrop"  : "static",
        "keyboard"  : true,
        "show"      : true
    });*/

    $.ajax({
       url: site_url+'authen/set_cookieAgreement',
       type: 'POST',
       data: {
          csrf_test_name: $('input[name=csrf_test_name]').val(),
       },
       success:function(rs){
        $('#myModalAgreement').modal('hide');
        //$('#modal_formLoad').modal('hide');
       }
    });
});

function alert_box(text){
  bootbox.dialog({
    message: "<span class='bigger-110'><i class='fa fa-exclamation-circle text-warning'></i> "+text+"</span>",
    className : "my_width",
    buttons:
    {
    	"success" :
    	  {
    		"label" : "<i class='fa fa-check'></i> ตกลง",
    			"className" : "btn-sm btn-default",
    			"callback": function() {
    			}
    	 }
    }
  });
}
</script>
