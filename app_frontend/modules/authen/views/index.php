<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo web_title1();?> | เข้าสู่ระบบ</title>
  <link rel="icon" href="<?php echo base_url();?>uploads/logo/ico/logo_web.ico" type="image/x-icon">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/ionicframework/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="hold-transition login-page">
<div class="login-box">
  <!-- /.login-logo -->
  <div class="login-box-body">
    <div class="login-logo">
      <span class="logo-lg"><img src="<?php echo web_logo();?>" style="width: 45%;"></span>
      <h3 class="login-box-msg" style="padding: 0px 0px 20px 0px;"> <?php echo web_title1().'<br>('.web_title2().')';?></h3>
    </div>

    <h4 class="login-box-msg"><i class="fa fa-user-circle-o"></i> ลงชื่อเข้าสู่ระบบ</h4>

    <?php
    $form_login = array(
      "id"=>"form_login",
      "name"=>"form_login",
      "class"=>"top15",
    );
    echo form_open('',$form_login);
    echo form_hidden("baseURL",base_url());
    ?>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" id="txt_username" name="txt_username" onkeyup="validator();">
        <div id="message_username"></div>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" id="txt_password" name="txt_password" onkeyup="validator();">
        <div id="message_password"></div>
      </div>
      <div class="row">
        <div class="col-xs-6">
            <button type="submit" class="btn btn-success btn-block btn-flat" id="bt_login" name="bt_login"><span id="icon-loading"></span>เข้าสู่ระบบ</button>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
            <button type="button" class="btn btn-block btn-flat" onclick="window.location.reload()">ยกเลิก</button>
        </div>
        <!-- /.col -->
      </div>
      <div class="row">
        <div class="col-xs-12">
            <div id="message_authen"></div>
        </div>
      </div>
    <?php echo form_close();?>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->


<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/jquery/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/bootstrap/js/bootstrap.min.js"></script>
<?php $this->view('dev_script/index.js.php');?>

</body>
</html>
