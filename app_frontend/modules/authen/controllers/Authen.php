<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authen extends MX_Controller {

    public function __construct()
    {
        parent::__construct();

        //***** load Model *****
        $this->load->model('authen_model');

    }

	public function index()
	{
	    if($this->admin_authen->is_logged_in()){
            redirect("admin/profile");
        }else{
            $this->load->view('authen/index');
        }
	}

    public function check_authen()
  	{
  	  if($this->admin_authen->is_logged_in()){
          redirect("appointments");
      }
      $username = $this->input->post('txt_username');
      $password = $this->input->post('txt_password');
      $member_id = '';
   	  $message_authen = '';
      $message_username = '';
      $message_password = '';

      if($username != '' && $password != ''){ //Check Login
        $result = $this->authen_model->get_login($username,do_hash($password));
        $member_id = (!empty($result['member_id']))? $result['member_id'] : '';

        if(empty($result)){ //login false
            $message_authen .= '<font required color="red" style="font-size: 0.8em;"><i class="fa fa-exclamation-circle"></i>&nbsp;Username / Password ไม่ถูกต้อง.</font>';
        }else{ //login true
            $message_authen .= "";
        }
      }else{ //Parameter Null
        if($username == ''){
            $message_username .= '<font required color="red" style="font-size: 0.8em;"><i class="fa fa-exclamation-circle"></i>&nbsp;กรุณากรอก Username.</font>';
        }

        if($password == ''){
            $message_password .= '<font required color="red" style="font-size: 0.8em;"><i class="fa fa-exclamation-circle"></i>&nbsp;กรุณากรอก Password.</font>';
        }
      }

      $array=array('message_authen'=>$message_authen,'message_username'=>$message_username,'message_password'=>$message_password,'member_id'=>$member_id);
  	  echo json_encode($array);
  }

  public function authen_member($id)
  {
        if($this->admin_authen->is_logged_in()){
            redirect("appointments");
        }
 		$profile = $this->authen_model->get_profile($id);

  		if(!empty($profile))
  		{
  			$authen_status = $this->admin_authen->validate($id);
  			if($authen_status['authen_msg'] == "notMatch")
  			{
  				$this->session->set_flashdata('login_msg', 'notMatch');
  				redirect($this->input->post("self_url"));
  			}

  			if($authen_status['authen_msg'] == "notActive")
  			{
  				$this->session->set_flashdata('login_msg', 'notActive');
  				redirect($this->input->post("self_url"));
  			}

            if($authen_status['authen_msg'] == "existedSignin")
  			{
                $this->session->set_flashdata('login_msg', 'existedSignin');
  			    redirect(site_url());
  			}

  			if($authen_status['authen_msg'] == "signinPass")
  			{
                $this->session->set_flashdata('login_msg', 'signinPass');
  			    redirect("admin/profile");
  			}

  		}
  		else
  		{
              redirect(site_url());
  		}

  }

  public function set_cookieAgreement()
  {
    $cookie = array(
      'name'   => 'cookieAgreement',
      'value'  => TRUE,
      'expire' => 0,
    );
    $this->input->set_cookie($cookie);
  }

  public function logout()
  {
      $this->admin_authen->logout();
      redirect(site_url());

  }
}
