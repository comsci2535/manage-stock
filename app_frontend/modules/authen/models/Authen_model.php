<?php if(!defined('BASEPATH'))exit('No direct script allowed');

class Authen_model extends CI_model{

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function get_login($user='',$pass='')
  {
    $_where = NULL;
    $_where .= " WHERE delete_status = '0' ";
    $_where .= " AND member_status = '1' ";
    $_where .= " AND member_user = '".$user."' ";
    $_where .= " AND member_pass_encrypt = '".$pass."' ";
    $sql = "SELECT member_id FROM member ".$_where;
    $query = $this->db->query($sql);
    return $query->row_array();
  }

  public function get_profile($id)
  {
    return $this->db
    ->where('member_id',$id)
    ->get('member')->row_array();
  }

  public function insertData($data="")
  {
    $this->db->insert('member', $data);
    return $this->db->insert_id();
  }

  public function updateData($id=0,$data="")
  {
    return $this->db->where('member_id',$id)->update('member',$data);
  }

  public function update_online_status($id=0,$data='')
  {
    return $this->db->where('member_id',$id)->update('log_login',$data);
  }

  public function get_member_api_key($id,$member_type)
  {
    return $this->db
    ->select('member_id')
    ->where('member_api_id',$id)
    ->where('member_type',$member_type)
    ->get('member')->row_array();
  }

  public function get_setting_info($info_id)
  {
    return $this->db
    ->where('info_id',$info_id)
    ->where('info_status',1)
    ->get('setting_info')->row_array();
  }

  public function getMyModal()
  {
      $txt = '';
      $txt .='$("#myModal").modal({';
      $txt .='"backdrop"  : "static",';
      $txt .='"keyboard"  : true,';
      $txt .='"show"      : true';
      $txt .='});';

      return  $txt;
  }

  public function getMyModalAgreement()
  {
      $txt = '';
      $txt .='$("#myModalAgreement").modal({';
      $txt .='"backdrop"  : "static",';
      $txt .='"keyboard"  : true,';
      $txt .='"show"      : true';
      $txt .='});';

      return  $txt;
  }

}
