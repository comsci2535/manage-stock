<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quiz extends MX_Controller {

    private $title = "แบบทดสอบ";
    public function __construct()
    {
        parent::__construct();

        //***** load Model *****
        //$this->load->model('admin/quiz_model');

        //***** Include Template *****
        $this->template
        ->set_layout('template/layout/template')
        ->set_view('notices','template/notices')
       // ->set_view('user_signin','template/user_signin')
        ->set_view('footer','template/footer');
    }

	public function index($navbar_active=0)
	{
            //***** DATA MENU Active *****
            $text_header = $this->title;
            $data_menu['menu_active'] = "quiz";
            $data_menu['submenu_active'] = "";

            //*****DATA Breadcrumb*****
            $data_bread['page_icon'] = "fa fa-book";
            $data_bread['setting_bread']["bread_name"] = array($text_header);
            $data_bread['setting_bread']["bread_link"] = array();
            $data_bread['setting_bread']["bread_active"] = 0;

            if($navbar_active == 1){
                $data['navbar_tab1'] = 'active';
                $data['navbar_tab2'] = '';
            }else if($navbar_active == 2){
                $data['navbar_tab1'] = '';
                $data['navbar_tab2'] = 'active';
            }else{
                $data['navbar_tab1'] = 'active';
                $data['navbar_tab2'] = '';
            }

            //*****DATA Build*****
            $result = array();
            $result[] = array(
                'QUIZ_ID' => '1001',
                'QUIZ_NAME' => 'ส่วนที่ 1 : เป็นการทำการทดสอบแบบตัวเลือกโดยเลือกเฉพาะ 1 ตัวเลือกเท่านั้น',
                'QUIZ_TYPE' => 'ปรนัย',
                'QUIZ_QUANTITY' => '10 ข้อ',
                'QUIZ_SCORE' => '8 - 10 ผ่าน',
                'QUIZ_TIME' => '1 ชั่วโมง',
            );
            $result[] = array(
                'QUIZ_ID' => '1002',
                'QUIZ_NAME' => 'ส่วนที่ 2 : เป็นการทำการทดสอบแบบจับคู่ตัวเลือกและคำตอบ',
                'QUIZ_TYPE' => 'อัตนัย',
                'QUIZ_QUANTITY' => '5 ข้อ',
                'QUIZ_SCORE' => '3 - 5 ผ่าน',
                'QUIZ_TIME' => 'ไม่จำกัดเวลา',
            );
            $result[] = array(
                'QUIZ_ID' => '1003',
                'QUIZ_NAME' => 'ส่วนที่ 3 : เป็นการทำการทดสอบแบบโดยเลือกคำตอบให้ถูกต้องกับช่องว่างของโจทย์',
                'QUIZ_TYPE' => 'จับคู่',
                'QUIZ_QUANTITY' => '5 ข้อ',
                'QUIZ_SCORE' => '3 - 5 ผ่าน',
                'QUIZ_TIME' => '40 นาที',
            );

            $result[] = array(
                'QUIZ_ID' => '1004',
                'QUIZ_NAME' => 'ส่วนที่ 4 : เป็นการเขียนบรรยาย (ต้องรอผลการตรวจประเมินจากทางผูสอน)',
                'QUIZ_TYPE' => 'เติมคำในช่องว่าง',
                'QUIZ_QUANTITY' => '5 ข้อ',
                'QUIZ_SCORE' => '3 - 5 ผ่าน',
                'QUIZ_TIME' => '30 นาที',
            );

            $data['quiz'] = $result;

            //***** Template *****
            $this->template
            //***** Title *****
            ->title($text_header)

             //***** Plugins Jquery-ui *****
             ->css('assets/plugins/jquery-ui/css/jquery-ui.min.css')
             ->js_foot('assets/plugins/jquery-ui/js/jquery-ui.min.js')

            //***** Plugins Validator *****
            ->css('assets/plugins/bootstrap-validator/css/bootstrapValidator.css')
            ->js_foot('assets/plugins/bootstrap-validator/js/bootstrapValidator.js')

            ->css('assets/admin/dist/css/reset_style.css')

            //***** DEV JAVA SCRIPT *****
            ->set_view('dev_script','quiz/dev_script/index.js.php')

            //***** Response *****
            //->set_view('menu','template/menu', $data_menu)
            ->set_view('breadcrumb','template/breadcrumb',$data_bread)
            ->build('quiz/index',$data);
	}

    public function exams($id)
    {
            //***** DATA MENU Active *****
            $text_header = $this->title;
            $data_menu['menu_active'] = "quiz";
            $data_menu['submenu_active'] = "";

            if($id == 1001){
                $form = 'form_exams1';
                $title_active = 'ส่วนที่ 1 : เป็นการทำการทดสอบแบบตัวเลือกโดยเลือกเฉพาะ 1 ตัวเลือกเท่านั้น';
            }else if($id == 1002){
                $form = 'form_exams2';
                $title_active = 'ส่วนที่ 2 : เป็นการทำการทดสอบแบบจับคู่ตัวเลือกและคำตอบ';
            }else if($id == 1003){
                $form = 'form_exams3';
                $title_active = 'ส่วนที่ 3 : เป็นการทำการทดสอบแบบโดยเลือกคำตอบให้ถูกต้องกับช่องว่างของโจทย์';
            }else if($id == 1004){
                $form = 'form_exams4';
                $title_active = 'ส่วนที่ 4 : เป็นการเขียนบรรยาย (ต้องรอผลการตรวจประเมินจากทางผูสอน)';
            }

            //*****DATA Breadcrumb*****
            $data_bread['page_icon'] = "fa fa-book";
            $data_bread['setting_bread']["bread_name"] = array($text_header,$title_active);
            $data_bread['setting_bread']["bread_link"] = array(site_url('quiz'));
            $data_bread['setting_bread']["bread_active"] = 1;

            $data = $form;

            //***** Template *****
            $this->template
            //***** Title *****
            ->title($text_header)

            //***** Plugins Datatables *****
            ->css('assets/plugins/datatables/dataTables.bootstrap.css')
            ->js_foot('assets/plugins/datatables/jquery.dataTables.min.js')
            ->js_foot('assets/plugins/datatables/dataTables.bootstrap.min.js')

             //***** Plugins Jquery-ui *****
             ->css('assets/plugins/jquery-ui/css/jquery-ui.min.css')
             ->js_foot('assets/plugins/jquery-ui/js/jquery-ui.min.js')

            //***** Plugins Wizard *****
            ->css('assets/plugins/bootstrap-wizard/prettify.css')
            ->css('assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.css')
            ->js_foot('assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js')
            ->js_foot('assets/plugins/bootstrap-wizard/prettify.js')

            //***** Plugins timeto *****
            ->css('assets/plugins/time_to/timeto.css')
            ->js_foot('assets/plugins/time_to/jquery.time-to.js')

            //***** Plugins un-timeout *****
            ->css('assets/plugins/fun-timeout/timeout.css')
            ->js_foot('assets/plugins/fun-timeout/jquery.simple.timer.js')
            ->js_foot('assets/plugins/fun-timeout/timeout.js')

            //***** Plugins Validator *****
            ->css('assets/plugins/bootstrap-validator/css/bootstrapValidator.css')
            ->js_foot('assets/plugins/bootstrap-validator/js/bootstrapValidator.js')

            //***** Plugins bootstrap-fileupload *****
            ->css('assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css')
            ->js_foot('assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js')

            ->css('assets/admin/dist/css/reset_style_exams.css')

            //***** DEV JAVA SCRIPT *****
            ->set_view('dev_script','quiz/dev_script/'.$form.'.js.php',$data)

            //***** Response *****
            //->set_view('menu','template/menu', $data_menu)
            ->set_view('breadcrumb','template/breadcrumb',$data_bread)
            ->build('quiz/'.$form,$data);
    }
}
