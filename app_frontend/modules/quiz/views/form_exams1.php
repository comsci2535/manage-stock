<style type="text/css">
label {
  font-weight: unset;
  padding-left: 20px;
}

.timer, .timer-done, .timer-loop {
    font-size: 26px;
    background-color: #f1ebeb;
    padding: 6px 10px 6px 10px;
}

</style>

<div class="box-body">
  <div class="wrap-form clearfix ">
    <?php
    $form = array("id"=>"form_data","name"=>"form_data","accept-charset"=>"utf-8","class"=>"form-horizontal");
    echo (! empty($item) ?  form_open_multipart('quiz/update', $form) :  form_open_multipart('quiz/save', $form));
    ?>
    <section id="wizard">
        <div id="rootwizard">
          <div class="col-sm-12">
            <div class="page-header">
                <h2>
                  <small>
                    จำนวนข้อสอบ 10 ข้อ | เวลาที่ใช้สอบ 1 ชั่วโมง | เกณฑ์คะแนน	 8 - 10 คะแนน
                    <div style="float: right;" class="timer" data-seconds-left=3600><i class="fa fa-clock-o" style="float: left;">&nbsp;</i></div>

                  </small>
                </h2>
            </div>
          </div>
          <div class="col-sm-8">
            <div class="tab-content">

               <div class="tab-pane active" id="tab1">
                 <div class="panel">
                   <div class="panel-heading">
                    <h4 class="panel-title"><strong>ข้อที่ 1 บุคคลใดต่อไปนี้ที่จัดอยู่ในวัยทอง ?</strong></h4>
                   </div>
                   <div class="panel-body" style="padding-top: 0px;">
                      <label class="control-label">
                        <input type="radio" id="QUIZ_1" name="QUIZ_1">
                        ป้าสายพิณขี้บ่น
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_1" name="QUIZ_1">
                        ป้าสุดา มีอายุ ๔๕ ปี
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_1" name="QUIZ_1">
                        ลุงสมาน มีอายุ ๖๐ ปี
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_1" name="QUIZ_1">
                        ลุงสมหวังมักหงุดหงิดบ่อย
                      </label>
                   </div>
                 </div>
               </div>

               <div class="tab-pane" id="tab2">
                  <div class="panel">
                   <div class="panel-heading">
                    <h4 class="panel-title"><strong>ข้อที่ 2 ปรากฏการณ์ใดที่มักเกิดขึ้นก่อนการหมดประจำเดือน ?</strong></h4>
                   </div>
                   <div class="panel-body" style="padding-top: 0px;">
                      <label class="control-label">
                        <input type="radio" id="QUIZ_2" name="QUIZ_2">
                        อารมณ์เสียบ่อย
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_2" name="QUIZ_2">
                        มักปวดท้องเมื่อมีประจำเดือน
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_2" name="QUIZ_2">
                        ประจำเดือนมาไม่ตรงเวลา
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_2" name="QUIZ_2">
                        ประจำเดือนไม่มาเป็นเวลา ๑ ปี
                      </label>
                   </div>
                 </div>
               </div>

               <div class="tab-pane" id="tab3">
                  <div class="panel">
                   <div class="panel-heading">
                    <h4 class="panel-title"><strong>ข้อที่ 3 ผู้สูงอายุมักมีกระดูกที่บางและเปราะง่าย เนื่องจากสาเหตุข้อใด ?</strong></h4>
                   </div>
                   <div class="panel-body" style="padding-top: 0px;">
                      <label class="control-label">
                        <input type="radio" id="QUIZ_3" name="QUIZ_3">
                        ขาดวิตามินดี
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_3" name="QUIZ_3">
                        กระดูกสันหลังโค้งงอ
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_3" name="QUIZ_3">
                        กล้ามเนื้อไม่แข็งแรง
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_3" name="QUIZ_3">
                        การดูดซึมของแคลเซียมลดลง
                      </label>
                   </div>
                 </div>
               </div>

               <div class="tab-pane" id="tab4">
                 <div class="panel">
                   <div class="panel-heading">
                    <h4 class="panel-title"><strong>ข้อที่ 4 คนวัยสูงอายุมักมีโอกาสเสี่ยงต่อการเกิดโรคเบาหวาน เพราะสาเหตุใด ?</strong></h4>
                   </div>
                   <div class="panel-body" style="padding-top: 0px;">
                      <label class="control-label">
                        <input type="radio" id="QUIZ_4" name="QUIZ_4">
                        ฮอร์โมนอินซูลินลดลง
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_4" name="QUIZ_4">
                        ร่างกายสะสมน้ำตาลมากขึ้น
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_4" name="QUIZ_4">
                        มักชอบรับประทานอาหารประเภทแป้งและน้ำตาล
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_4" name="QUIZ_4">
                        ร่างกายทำงานน้อยลง จึงทำให้ขับน้ำตาลออกมาได้น้อย
                      </label>
                   </div>
                 </div>
               </div>

               <div class="tab-pane" id="tab5">
                 <div class="panel">
                   <div class="panel-heading">
                    <h4 class="panel-title"><strong>ข้อที่ 5 โรคใดต่อไปนี้สามารถถ่ายทอดทางพันธุกรรมได้ ?</strong></h4>
                   </div>
                   <div class="panel-body" style="padding-top: 0px;">
                      <label class="control-label">
                        <input type="radio" id="QUIZ_5" name="QUIZ_5">
                        โรคหัด
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_5" name="QUIZ_5">
                        โรคไข้จับสั่น
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_5" name="QUIZ_5">
                        โรคเบาหวาน
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_5" name="QUIZ_5">
                        โรคมือ เท้า ปากเปื่อย
                      </label>
                   </div>
                 </div>
               </div>

               <div class="tab-pane" id="tab6">
                 <div class="panel">
                   <div class="panel-heading">
                    <h4 class="panel-title"><strong>ข้อที่ 6 ปัจจัยด้านสิ่งแวดล้อมที่มีผลต่อพัฒนาการของมนุษย์มีกี่ประการ อะไรบ้าง ?</strong></h4>
                   </div>
                   <div class="panel-body" style="padding-top: 0px;">
                      <label class="control-label">
                        <input type="radio" id="QUIZ_6" name="QUIZ_6">
                        2 ประการ ได้แก่ ตนเอง และครอบครัว
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_6" name="QUIZ_6">
                        3 ประการ ได้แก่ ตนเอง ครอบครัว และสังคม
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_6" name="QUIZ_6">
                        4 ประการ ได้แก่ ตนเอง ครอบครัว สังคม และชุมชน
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_6" name="QUIZ_6">
                        5 ประการ ได้แก่ ตนเอง ครอบครัว สังคม ชุมชน และประเทศชาติ
                      </label>
                   </div>
                 </div>
               </div>

               <div class="tab-pane" id="tab7">
                 <div class="panel">
                   <div class="panel-heading">
                    <h4 class="panel-title"><strong>ข้อที่ 7 ตามหลักสากลผู้สูงอายุจะต้องมีอายุเท่าใดขึ้นไป ?</strong></h4>
                   </div>
                   <div class="panel-body" style="padding-top: 0px;">
                      <label class="control-label">
                        <input type="radio" id="QUIZ_7" name="QUIZ_7">
                        57 ปีขึ้นไป
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_7" name="QUIZ_7">
                        60 ปีขึ้นไป
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_7" name="QUIZ_7">
                        61 ปีขึ้นไป
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_7" name="QUIZ_7">
                        65 ปีขึ้นไป
                      </label>
                   </div>
                 </div>
               </div>

               <div class="tab-pane" id="tab8">
                 <div class="panel">
                   <div class="panel-heading">
                    <h4 class="panel-title"><strong>ข้อที่ 8 ปัญหาด้านสังคมของผู้สูงอายุในข้อใดมีความรุนแรงที่สุด ?</strong></h4>
                   </div>
                   <div class="panel-body" style="padding-top: 0px;">
                      <label class="control-label">
                        <input type="radio" id="QUIZ_8" name="QUIZ_8">
                        บทบาทหน้าที่ลดลง
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_8" name="QUIZ_8">
                        การพบปะสังสรรค์น้อยลง
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_8" name="QUIZ_8">
                        สมรรถภาพในการทำงานลดลง
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_8" name="QUIZ_8">
                        มีปัญหาทางด้านเศรษฐกิจ และบุตรหลานทอดทิ้ง
                      </label>
                   </div>
                 </div>
               </div>

               <div class="tab-pane" id="tab9">
                 <div class="panel">
                   <div class="panel-heading">
                    <h4 class="panel-title"><strong>ข้อที่ 9 การเปลี่ยนแปลงทางด้านอารมณ์ของวัยรุ่นชายและหญิงในข้อใดที่ตรงกัน ?</strong></h4>
                   </div>
                   <div class="panel-body" style="padding-top: 0px;">
                      <label class="control-label">
                        <input type="radio" id="QUIZ_9" name="QUIZ_9">
                        ใจเย็น รอบคอบ
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_9" name="QUIZ_9">
                        อารมณ์เปลี่ยนแปลงง่าย
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_9" name="QUIZ_9">
                        มีความคิดเป็นผู้ใหญ่เกินวัย
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_9" name="QUIZ_9">
                        มีเหตุผลในการแก้ปัญหามากขึ้น
                      </label>
                   </div>
                 </div>
               </div>

               <div class="tab-pane" id="tab10">
                 <div class="panel">
                   <div class="panel-heading">
                    <h4 class="panel-title"><strong>ข้อที่ 10 เมื่อวัยรุ่นเกิดปัญหา ควรแก้ไขอย่างไรจึงจะเหมาะสมที่สุด ?</strong></h4>
                   </div>
                   <div class="panel-body" style="padding-top: 0px;">
                      <label class="control-label">
                        <input type="radio" id="QUIZ_10" name="QUIZ_10">
                        ปรึกษาคนที่ไว้ใจได้
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_10" name="QUIZ_10">
                        ทำตัวให้สนุกสนานร่าเริง
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_10" name="QUIZ_10">
                        พยายามข่มความรู้สึกเอาไว้
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_10" name="QUIZ_10">
                        ระบายออกตามใจที่ตนเองอยากจะปฏิบัติ
                      </label>
                   </div>
                 </div>
               </div>

            </div>
          </div>

          <div class="col-sm-4">
            <div class="navbar" style="width: 75%;">
             <div class="navbar-inner">
                <ul class="nav nav-wizard">
                  <li class="active" style=""><a href="#tab1" class="tab1" data-toggle="tab" title="ข้อที่ 1.">1</a></li>
                  <li style=""><a href="#tab2" class="tab2" data-toggle="tab" title="ข้อที่ 2.">2</a></li>
                  <li style=""><a href="#tab3" class="tab3" data-toggle="tab" title="ข้อที่ 3.">3</a></li>
                  <li style=""><a href="#tab4" class="tab4" data-toggle="tab" title="ข้อที่ 4.">4</a></li>
                  <li style=""><a href="#tab5" class="tab5" data-toggle="tab" title="ข้อที่ 5.">5</a></li>
                  <li style=""><a href="#tab6" class="tab6" data-toggle="tab" title="ข้อที่ 6.">6</a></li>
                  <li style=""><a href="#tab7" class="tab7" data-toggle="tab" title="ข้อที่ 7.">7</a></li>
                  <li style=""><a href="#tab8" class="tab8" data-toggle="tab" title="ข้อที่ 8.">8</a></li>
                  <li style=""><a href="#tab9" class="tab9" data-toggle="tab" title="ข้อที่ 9.">9</a></li>
                  <li style=""><a href="#tab10" class="tab10" data-toggle="tab" title="ข้อที่ 10.">10</a></li>
                </ul>
             </div>
            </div>
            <div class="col-sm-12">
              <span class="label" style="background-color: #605ca8;font-size: 12px;"><i class="fa fa-pencil-square-o"></i> ข้อที่เลือก</span>
              &nbsp;<span class="label" style="background-color: #f1f1f1;font-size: 12px;color: #333;"><i class="fa fa-square-o"></i> ยังไม่ได้ทำ</span>
              &nbsp;<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check-square-o"></i> ทำแล้ว</span>
            </div>
          </div>

          <div class="col-sm-offset-1 col-sm-5">
            <ul class="pager wizard">
               <div id="form-error-div" class="text-danger"></div>
               <li class="previous"><button type="button" class="btn btn-default button-previous" name="previous" value="Prev"> <i class="fa fa-chevron-left"></i> ข้อก่อนหน้า </button></li>
               <li class="next">
                <button type="button" class="btn btn-primary button-next" name="next" value="Next"> ข้อถัดไป <i class="fa fa-chevron-right"></i></button>
                <button type="button" id="submit_form" class="btn btn-success" title="เสร็จสิ้น" style="display:none"><i class="fa fa-check"></i>&nbsp;เสร็จสิ้น</button>
               </li>
            </ul>
          </div>

        </div>
      </section>
      <?php echo form_close();?>

  </div>
</div>

