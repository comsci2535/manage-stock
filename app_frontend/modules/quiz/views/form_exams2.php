<style type="text/css">
label {
  font-weight: unset;
  padding-left: 20px;
}

.timer, .timer-done, .timer-loop {
    font-size: 26px;
    background-color: #f1ebeb;
    padding: 6px 10px 6px 10px;
}

</style>

<div class="box-body">
  <div class="wrap-form clearfix ">
    <?php
    $form = array("id"=>"form_data","name"=>"form_data","accept-charset"=>"utf-8","class"=>"form-horizontal");
    echo (! empty($item) ?  form_open_multipart('quiz/update', $form) :  form_open_multipart('quiz/save', $form));
    ?>
    <section id="wizard">
        <div id="rootwizard">
          <div class="col-sm-12">
            <div class="page-header">
                <h2>
                  <small>
                    จำนวนข้อสอบ 5 ข้อ | เวลาที่ใช้สอบ (ไม่จำกัดเวลา) | เกณฑ์คะแนน	 3 - 5 คะแนน
                  </small>
                </h2>
            </div>
          </div>
          <div class="col-sm-8">
            <div class="tab-content">

               <div class="tab-pane active" id="tab1">
                 <div class="panel">
                   <div class="panel-heading">
                    <h4 class="panel-title"><strong>กรุณาจับคู่อาการป่วยให้ตรงกับยาที่ใช้รักษา ?</strong></h4>
                   </div>
                   <div class="panel-body" style="padding-top: 0px;">

                     <div class="col-sm-8">
                      <table id="" class="table table-bordered table-striped">
                        <thead>
                			<tr>
                			    <th class="text-center" width="30%">คำถาม</th>
                			</tr>
    		            </thead>
                        <tbody>
                            <tr>
                              <td class="text-left">
                                <span class="form-control" style="border: 0px;background-color: #f5efef;">  ปวดศีรษะไมเกรน ?</span>
                              </td>
	                        </tr>
                            <tr>
                              <td class="text-left">
                                <span class="form-control" style="border: 0px;background-color: #f5efef;">อาหารเป็นพิษ ?</span>
                              </td>
	                        </tr>
                            <tr>
                              <td class="text-left">
                                <span class="form-control" style="border: 0px;background-color: #f5efef;">โรคกระเพราะ ?</span>
                              </td>
	                        </tr>
                            <tr>
                              <td class="text-left">
                               <span class="form-control" style="border: 0px;background-color: #f5efef;">ภูมิแพ้ ?</span>
                              </td>
	                        </tr>
                            <tr>
                              <td class="text-left">
                               <span class="form-control" style="border: 0px;background-color: #f5efef;">ปวดหลัง ?</span>
                              </td>
                          </tr>
                        </tbody>
    				</table>
                   </div>

                   <div class="col-sm-4">
                    <table id="" class="table table-bordered table-striped">
                        <thead>
                			<tr>
                				<th class="text-center" width="10%">จับคู่คำตอบ</th>
                			</tr>
    		            </thead>
                        <tbody>
                            <tr>
                              <td>
                                <select class="form-control" name="" id="">
                                    <option value=""></option>
                                    <option value="1">ดื่มเกลือแร่</option>
                                    <option value="2">ยาสเตียรอยด์</option>
                                    <option value="3">ยาพาราเซตามอล</option>
                                    <option value="4">ยาธาตุน้ําขาว</option>
                                    <option value="5">ยาคลายกล้ามเนื้อ</option>
                                </select>
                              </td>
	                        </tr>
                            <tr>
                              <td>
                                <select class="form-control" name="" id="">
                                    <option value=""></option>
                                    <option value="1">ดื่มเกลือแร่</option>
                                    <option value="2">ยาสเตียรอยด์</option>
                                    <option value="3">ยาพาราเซตามอล</option>
                                    <option value="4">ยาธาตุน้ําขาว</option>
                                    <option value="5">ยาคลายกล้ามเนื้อ</option>
                                </select>
                              </td>
	                        </tr>
                            <tr>
                              <td>
                                <select class="form-control" name="" id="">
                                    <option value=""></option>
                                    <option value="1">ดื่มเกลือแร่</option>
                                    <option value="2">ยาสเตียรอยด์</option>
                                    <option value="3">ยาพาราเซตามอล</option>
                                    <option value="4">ยาธาตุน้ําขาว</option>
                                    <option value="5">ยาคลายกล้ามเนื้อ</option>
                                </select>
                              </td>
	                        </tr>
                            <tr>
                              <td>
                                <select class="form-control" name="" id="">
                                    <option value=""></option>
                                    <option value="1">ดื่มเกลือแร่</option>
                                    <option value="2">ยาสเตียรอยด์</option>
                                    <option value="3">ยาพาราเซตามอล</option>
                                    <option value="4">ยาธาตุน้ําขาว</option>
                                    <option value="5">ยาคลายกล้ามเนื้อ</option>
                                </select>
                              </td>
	                        </tr>
                            <tr>
                              <td>
                                <select class="form-control" name="" id="">
                                    <option value=""></option>
                                    <option value="1">ดื่มเกลือแร่</option>
                                    <option value="2">ยาสเตียรอยด์</option>
                                    <option value="3">ยาพาราเซตามอล</option>
                                    <option value="4">ยาธาตุน้ําขาว</option>
                                    <option value="5">ยาคลายกล้ามเนื้อ</option>
                                </select>
                              </td>
                          </tr>
                        </tbody>
    				</table>
                   </div>

                  </div>
                 </div>
               </div>

            </div>
          </div>

          <div class="col-sm-offset-3 col-sm-5">
            <ul class="pager wizard" style="margin: 0px 0;">
              <li class="next">
                <button type="button" id="submit_form" class="btn btn-success" title="เสร็จสิ้น" ><i class="fa fa-check"></i>&nbsp;เสร็จสิ้น</button>
              </li>
            </ul>
          </div>

        </div>
      </section>
      <?php echo form_close();?>

  </div>
</div>

