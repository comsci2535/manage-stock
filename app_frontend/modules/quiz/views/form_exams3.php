<style type="text/css">
label {
  font-weight: unset;
  padding-left: 20px;
}

.timer, .timer-done, .timer-loop {
    font-size: 26px;
    background-color: #f1ebeb;
    padding: 6px 10px 6px 10px;
}

</style>

<div class="box-body">
  <div class="wrap-form clearfix ">
    <?php
    $form = array("id"=>"form_data","name"=>"form_data","accept-charset"=>"utf-8","class"=>"form-horizontal");
    echo (! empty($item) ?  form_open_multipart('quiz/update', $form) :  form_open_multipart('quiz/save', $form));
    ?>
    <section id="wizard">
        <div id="rootwizard">
          <div class="col-sm-12">
            <div class="page-header">
                <h2>
                  <small>
                    จำนวนข้อสอบ 5 ข้อ | เวลาที่ใช้สอบ 40 นาที | เกณฑ์คะแนน	 3 - 5 คะแนน
                    <div style="float: right;" class="timer" data-seconds-left=2400><i class="fa fa-clock-o" style="float: left;">&nbsp;</i></div>

                  </small>
                </h2>
            </div>
          </div>
          <div class="col-sm-8">
            <div class="tab-content">

               <div class="tab-pane active" id="tab1">
                 <div class="panel">
                   <div class="panel-heading">
                    <h4 class="panel-title"><strong>ข้อที่ 1 บุคคลในวัย&nbsp;<input type="input" id="INPUT_QUIZ_1" style="width: 20%;color: #605ca8;text-align: center;"/>&nbsp;จะอยู่ในช่วงวัยเจริญพันธุ์</strong></h4>
                   </div>
                   <div class="panel-body" style="padding-top: 0px;">
                      <label class="control-label">
                        <input type="radio" id="QUIZ_1" name="QUIZ_1" value="11 - 12 ปี   ">
                        11 - 12 ปี
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_1" name="QUIZ_1" value="16 - 25 ปี">
                        16 - 25 ปี
                      </label><br>
                      <label class="control-label">
                      </label><br>
                   </div>
                 </div>
               </div>

               <div class="tab-pane" id="tab2">
                  <div class="panel">
                   <div class="panel-heading">
                    <h4 class="panel-title"><strong>ข้อที่ 2 มนุษย์จะมี&nbsp;<input type="input" id="INPUT_QUIZ_2" style="width: 20%;color: #605ca8;text-align: center;"/>&nbsp;เป็นจุดเริ่มต้นมนุษยสัมพันธ์</strong></h4>
                   </div>
                   <div class="panel-body" style="padding-top: 0px;">
                      <label class="control-label">
                        <input type="radio" id="QUIZ_2" name="QUIZ_2" value="พ่อแม่  ผู้ปกครอง">
                        พ่อแม่  ผู้ปกครอง
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_2" name="QUIZ_2" value="เพื่อน">
                        เพื่อน
                      </label>
                   </div>
                 </div>
               </div>

               <div class="tab-pane" id="tab3">
                  <div class="panel">
                   <div class="panel-heading">
                    <h4 class="panel-title"><strong>ข้อที่ 3 มนุษย์ช่วง&nbsp;<input type="input" id="INPUT_QUIZ_3" style="width: 20%;color: #605ca8;text-align: center;"/>&nbsp;ซึ่งเป็นช่วงวัยที่ควรแนะนำและคอยเตือนมากที่สุก</strong></h4>
                   </div>
                   <div class="panel-body" style="padding-top: 0px;">
                      <label class="control-label">
                        <input type="radio" id="QUIZ_3" name="QUIZ_3" value="วัยเด็ก">
                        วัยเด็ก
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_3" name="QUIZ_3" value="วัยรุ่น">
                        วัยรุ่น
                      </label>
                   </div>
                 </div>
               </div>

               <div class="tab-pane" id="tab4">
                 <div class="panel">
                   <div class="panel-heading">
                    <h4 class="panel-title"><strong>ข้อที่ 4 สถานการณ์อาศัยอยู่&nbsp;<input type="input" id="INPUT_QUIZ_4" style="width: 20%;color: #605ca8;text-align: center;"/>&nbsp;มีโอกาสเสียงต่อการมีเพศสัมพันธ์สูงมาก</strong></h4>
                   </div>
                   <div class="panel-body" style="padding-top: 0px;">
                      <label class="control-label">
                        <input type="radio" id="QUIZ_4" name="QUIZ_4" value="ที่บ้าน">
                        ที่บ้าน
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_4" name="QUIZ_4" value="ในหอพักตามลำพัง">
                        ในหอพักตามลำพัง
                      </label>
                   </div>
                 </div>
               </div>

               <div class="tab-pane" id="tab5">
                 <div class="panel">
                   <div class="panel-heading">
                    <h4 class="panel-title"><strong>ข้อที่ 5 วันที่&nbsp;<input type="input" id="INPUT_QUIZ_5" style="width: 20%;color: #605ca8;text-align: center;"/>&nbsp;เป็นวันสิ่งแวดล้อมของไทย</strong></h4>
                   </div>
                   <div class="panel-body" style="padding-top: 0px;">
                      <label class="control-label">
                        <input type="radio" id="QUIZ_5" name="QUIZ_5" value="4 ธันวาคมของทุกปี">
                        4 ธันวาคมของทุกปี
                      </label><br>
                      <label class="control-label">
                        <input type="radio" id="QUIZ_5" name="QUIZ_5" value="31 ธันวาคมของทุกปี">
                        31 ธันวาคมของทุกปี
                      </label>
                   </div>
                 </div>
               </div>

            </div>
          </div>

          <div class="col-sm-4">
            <div class="navbar" style="width: 50%;">
             <div class="navbar-inner">
                <ul class="nav nav-wizard">
                  <li class="active" style=""><a href="#tab1" class="tab1" data-toggle="tab" title="ข้อที่ 1.">1</a></li>
                  <li style=""><a href="#tab2" class="tab2" data-toggle="tab" title="ข้อที่ 2.">2</a></li>
                  <li style=""><a href="#tab3" class="tab3" data-toggle="tab" title="ข้อที่ 3.">3</a></li>
                  <li style=""><a href="#tab4" class="tab4" data-toggle="tab" title="ข้อที่ 4.">4</a></li>
                  <li style=""><a href="#tab5" class="tab5" data-toggle="tab" title="ข้อที่ 5.">5</a></li>

                </ul>
             </div>
            </div>
            <div class="col-sm-12">
              <span class="label" style="background-color: #605ca8;font-size: 12px;"><i class="fa fa-pencil-square-o"></i> ข้อที่เลือก</span>
              &nbsp;<span class="label" style="background-color: #f1f1f1;font-size: 12px;color: #333;"><i class="fa fa-square-o"></i> ยังไม่ได้ทำ</span>
              &nbsp;<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check-square-o"></i> ทำแล้ว</span>
            </div>
          </div>

          <div class="col-sm-offset-1 col-sm-5">
            <ul class="pager wizard">
               <div id="form-error-div" class="text-danger"></div>
               <li class="previous"><button type="button" class="btn btn-default button-previous" name="previous" value="Prev"> <i class="fa fa-chevron-left"></i> ข้อก่อนหน้า </button></li>
               <li class="next">
                <button type="button" class="btn btn-primary button-next" name="next" value="Next"> ข้อถัดไป <i class="fa fa-chevron-right"></i></button>
                <button type="button" id="submit_form" class="btn btn-success" title="เสร็จสิ้น" style="display:none"><i class="fa fa-check"></i>&nbsp;เสร็จสิ้น</button>
               </li>
            </ul>
          </div>

        </div>
      </section>
      <?php echo form_close();?>

  </div>
</div>

