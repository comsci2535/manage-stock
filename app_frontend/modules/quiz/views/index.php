<div class="box-body">
  <div class="wrap-form clearfix ">
  <?php
  $form = array("id"=>"form_data","name"=>"form_data","accept-charset"=>"utf-8","class"=>"form-horizontal");
  echo (! empty($item) ?  form_open_multipart('quiz/update', $form) :  form_open_multipart('quiz/save', $form));
  ?>
        <div class="navbar">
         <div class="navbar-inner">
            <ul class="nav nav-pills nav-justified">
                <li role="presentation" class="<?php echo $navbar_tab1;?>"><a href="#tab1" data-toggle="tab">คำอธิบายแบบทดสอบ</a></li>
                <li role="presentation" class="<?php echo $navbar_tab2;?>"><a href="#tab2" data-toggle="tab">ประวัติคะแนนการทำข้อสอบ</a></li>
            </ul>
         </div>
        </div>

        <div class="tab-content">

           <div class="tab-pane <?php echo $navbar_tab1;?>" id="tab1">
                <div class="portlet portlet-basic">
                    <div class="portlet-body">
    					<div class="row">
    					    <div class="col-sm-8">
                                <div class="wrap-text clearfix">
                                  <div class="panel-body">
                                    <dl class="dl-horizontal">
                                      <dt style="text-overflow: unset;">หลักสูตร : </dt>
                                      <dd>การดูแลผู้ป่วยสูงอายุ</dd>
                                      <br>
                                      <dt style="text-overflow: unset;">วิชา : </dt>
                                      <dd>การดูแลผู้ป่วย</dd>
                                      <br>
                                      <dt style="text-overflow: unset;">คำอธิบายรายวิชา : </dt>
                                      <dd>นุษย์มีการพัฒนาสมบูรณ์สูงสุดเมื่ออายุ 20-25 ปี จากนั้นประสิทธิภาพการทำงานของร่างกายและอวัยวะจะเริ่มถดถอยลงซึ่งจะเห็นได้ชัดเจนเมื่อถึงวัยสูงอายุ ทำให้ผู้สูงอายุจะมีปัญหาสุขภาพทั้งร่างกาย จิตใจและโรคต่างๆ ซึ่งพบได้บ่อยกว่าในวัยอื่นๆ ปัญหาที่พบบ่อยในผู้สูงอายุ</dd>
                                    </dl>
                                  </div>
                                </div>
    					    </div>
    						<div class="col-sm-12">
    							<table id="" class="table table-bordered table-striped">
                                    <thead>
    									<tr>
    									    <th class="text-center" width="40%">ข้อสอบ</th>
    										<th class="text-center" width="10%">จำนวนข้อ</th>
    										<th class="text-center" width="10%">เกณฑ์คะแนน</th>
                                            <th class="text-center" width="10%">เวลาที่ใช้สอบ</th>
                                            <th class="text-center" width="10%">เลือกข้อสอบ</th>
    									</tr>
    								</thead>
                                    <tbody>
                                        <?php foreach($quiz as $quiz_data){ ?>
                                        <tr>
                                          <td class="text-left">
                                            <?php echo $quiz_data['QUIZ_NAME'];?>
                                          </td>
                                          <td class="text-center">
                                            <?php echo $quiz_data['QUIZ_QUANTITY'];?>
                                          </td>
                                          <td class="text-center">
                                            <?php echo $quiz_data['QUIZ_SCORE'];?>
                                          </td>
                                          <td class="text-center">
                                            <?php echo $quiz_data['QUIZ_TIME'];?>
                                          </td>
                                          <td class="text-center">
                                            <a href="<?php echo site_url('quiz/exams/'.$quiz_data['QUIZ_ID']);?>" class="btn btn-info" target="_blank"><i class="fa fa-pencil-square-o"></i> เริ่มทำแบบทดสอบ</a>
                                          </td>
    									</tr>
                                        <?php } ?>
                                    </tbody>

        						</table>
    						</div>
    					</div>
    				</div>

                </div>
           </div>

           <div class="tab-pane <?php echo $navbar_tab2;?>" id="tab2">
                <div class="portlet portlet-basic">
                    <div class="portlet-body">
    					<div class="row">
    					    <div class="col-sm-8">
                                <div class="wrap-text clearfix">
                                  <div class="panel-body">
                                    <dl class="dl-horizontal">
                                      <dt style="text-overflow: unset;">หลักสูตร : </dt>
                                      <dd>การดูแลผู้ป่วยสูงอายุ</dd>
                                      <br>
                                      <dt style="text-overflow: unset;">วิชา : </dt>
                                      <dd>การดูแลผู้ป่วย</dd>
                                      <br>
                                      <dt style="text-overflow: unset;">คำอธิบายรายวิชา : </dt>
                                      <dd>นุษย์มีการพัฒนาสมบูรณ์สูงสุดเมื่ออายุ 20-25 ปี จากนั้นประสิทธิภาพการทำงานของร่างกายและอวัยวะจะเริ่มถดถอยลงซึ่งจะเห็นได้ชัดเจนเมื่อถึงวัยสูงอายุ ทำให้ผู้สูงอายุจะมีปัญหาสุขภาพทั้งร่างกาย จิตใจและโรคต่างๆ ซึ่งพบได้บ่อยกว่าในวัยอื่นๆ ปัญหาที่พบบ่อยในผู้สูงอายุ</dd>
                                    </dl>
                                  </div>
                                </div>
    					    </div>
    						<div class="col-sm-12">
    						    <div class="nav-tabs-custom">
                                  <ul class="nav nav-tabs">
                                    <li class="active"><a href="#type1" data-toggle="tab" aria-expanded="false"><strong>แบบทดสอบส่วนที่ 1</strong></a></li>
                                  </ul>
                                  <div class="tab-content">
                                    <div class="tab-pane active" id="type1">
                                        <table id="" class="table table-bordered table-striped">
                                            <thead>
            									<tr>
            									    <th class="text-center" width="10%">ครั้งที่</th>
            										<th class="text-center" width="15%">วันที่สอบ</th>
                                                    <th class="text-center" width="15%">จำนวนข้อ</th>
            										<th class="text-center" width="15%">เกณฑ์คะแนน</th>
                                                    <th class="text-center" width="15%">คะแนนที่ได้</th>
            										<th class="text-center" width="15%">ผลสอบ</th>
            									</tr>
            								</thead>
            								<tbody>
                                                <tr>
            									    <td class="text-center">1</td>
            										<td class="text-center">02/03/2562 | 12:00 น</td>
                                                    <td class="text-center">10 ข้อ</td>
            										<td class="text-center">8 - 10 ผ่าน</td>
                                                    <td class="text-center">8 คะแนน</td>
            										<td class="text-center"><strong style="color: green;">ผ่าน</strong></td>
            									</tr>
                                                <tr>
            									    <td class="text-center">2</td>
            										<td class="text-center">05/03/2562 | 14:30 น</td>
                                                    <td class="text-center">10 ข้อ</td>
            										<td class="text-center">8 - 10 ผ่าน</td>
                                                    <td class="text-center">10 คะแนน</td>
            										<td class="text-center"><strong style="color: green;">ผ่าน</strong></td>
            									</tr>
            								</tbody>
                						</table>
                                    </div>
                                  </div>
                                </div>

                                <div class="nav-tabs-custom">
                                  <ul class="nav nav-tabs">
                                    <li class="active"><a href="#type2" data-toggle="tab" aria-expanded="false"><strong>แบบทดสอบส่วนที่ 2</strong></a></li>
                                  </ul>
                                  <div class="tab-content">
                                    <div class="tab-pane active" id="type2">
                                        <table id="" class="table table-bordered table-striped">
                                            <thead>
            									<tr>
            									    <th class="text-center" width="10%">ครั้งที่</th>
            										<th class="text-center" width="15%">วันที่สอบ</th>
                                                    <th class="text-center" width="15%">จำนวนข้อ</th>
            										<th class="text-center" width="15%">เกณฑ์คะแนน</th>
                                                    <th class="text-center" width="15%">คะแนนที่ได้</th>
            										<th class="text-center" width="15%">ผลสอบ</th>
            									</tr>
            								</thead>
            								<tbody>
                                                <tr>
            									    <td class="text-center">1</td>
            										<td class="text-center">02/03/2562 | 15:00 น</td>
                                                    <td class="text-center">5 ข้อ</td>
            										<td class="text-center">3 - 5 ผ่าน</td>
                                                    <td class="text-center">2 คะแนน</td>
            										<td class="text-center"><strong style="color: red;">ไม่ผ่าน</strong></td>
            									</tr>
                                                <tr>
            									    <td class="text-center">2</td>
            										<td class="text-center">06/03/2562 | 11:00 น</td>
                                                    <td class="text-center">5 ข้อ</td>
            										<td class="text-center">3 - 5 ผ่าน</td>
                                                    <td class="text-center">3 คะแนน</td>
            										<td class="text-center"><strong style="color: green;">ผ่าน</strong></td>
            									</tr>
            								</tbody>
                						</table>
                                    </div>
                                  </div>
                                </div>

                                <div class="nav-tabs-custom">
                                  <ul class="nav nav-tabs">
                                    <li class="active"><a href="#type3" data-toggle="tab" aria-expanded="false"><strong>แบบทดสอบส่วนที่ 3</strong></a></li>
                                  </ul>
                                  <div class="tab-content">
                                    <div class="tab-pane active" id="type3">
                                        <table id="" class="table table-bordered table-striped">
                                            <thead>
            									<tr>
            									    <th class="text-center" width="10%">ครั้งที่</th>
            										<th class="text-center" width="15%">วันที่สอบ</th>
                                                    <th class="text-center" width="15%">จำนวนข้อ</th>
            										<th class="text-center" width="15%">เกณฑ์คะแนน</th>
                                                    <th class="text-center" width="15%">คะแนนที่ได้</th>
            										<th class="text-center" width="15%">ผลสอบ</th>
            									</tr>
            								</thead>
            								<tbody>
                                                <tr>
            									    <td class="text-center">1</td>
            										<td class="text-center">02/03/2562 | 09:00 น</td>
                                                    <td class="text-center">5 ข้อ</td>
            										<td class="text-center">3 - 5 ผ่าน</td>
                                                    <td class="text-center">4 คะแนน</td>
            										<td class="text-center"><strong style="color: green;">ผ่าน</strong></td>
            									</tr>
            								</tbody>
                						</table>
                                    </div>
                                  </div>
                                </div>

                                <div class="nav-tabs-custom">
                                  <ul class="nav nav-tabs">
                                    <li class="active"><a href="#type4" data-toggle="tab" aria-expanded="false"><strong>แบบทดสอบส่วนที่ 4</strong></a></li>
                                  </ul>
                                  <div class="tab-content">
                                    <div class="tab-pane active" id="type4">
                                        <table id="" class="table table-bordered table-striped">
                                            <thead>
            									<tr>
            									    <th class="text-center" width="10%">ครั้งที่</th>
            										<th class="text-center" width="15%">วันที่สอบ</th>
                                                    <th class="text-center" width="15%">จำนวนข้อ</th>
            										<th class="text-center" width="15%">เกณฑ์คะแนน</th>
                                                    <th class="text-center" width="15%">คะแนนที่ได้</th>
            										<th class="text-center" width="15%">ผลสอบ</th>
            									</tr>
            								</thead>
            								<tbody>
                                                <tr>
            									    <td class="text-center">1</td>
            										<td class="text-center">02/03/2562 | 10:00 น</td>
                                                    <td class="text-center">5 ข้อ</td>
            										<td class="text-center">3 - 5 ผ่าน</td>
                                                    <td class="text-center">1 คะแนน</td>
            										<td class="text-center"><strong style="color: red;">ไม่ผ่าน</strong></td>
            									</tr>
            								</tbody>
                						</table>
                                    </div>
                                  </div>
                                </div>

    						</div>
    					</div>
    				</div>

                </div>
           </div>

        </div>


    <?php echo form_close();?>
  </div>
</div>

