<script type="text/javascript">
$(document).ready(function() {
    var base_url = '<?php echo base_url();?>';
    var site_url = '<?php echo site_url();?>';
    //$('.content').css({height:'350px'});
    $('form').on('keyup keypress', function(e) { // not enter submit
      var keyCode = e.keyCode || e.which;
      if (keyCode === 13) {
        e.preventDefault();
        return false;
      }
    });

    $('#form_data').bootstrapValidator({
      message: 'กรุณากรอกข้อมูลให้ถูกต้อง.',
      feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
      },
      /*fields: {
          QUIZ_ID: {
                validators: {
                    notEmpty: {
                        message: 'กรุณาเลือกข้อสอบ.'
                    }
                }
          },

        }*/
    }).on('#rootwizard').bootstrapWizard({
        onTabClick: function(tab, navigation, index) {
            var $validator = $('#form_data').data('bootstrapValidator').validate();
            if($validator.isValid() === true){
                $(".button-next").show();
        		$(".btn-primary").show();
                $("#submit_form").hide();
            }
            return $validator.isValid();

        },onPrevious: function(tab, navigation, index){
            var $validator = $('#form_data').data('bootstrapValidator').validate();
            if($validator.isValid() === true){
                $(".button-next").show();
            	$(".btn-primary").show();
                $("#submit_form").hide();
            }
            return $validator.isValid();

      	},onNext: function(tab, navigation, index){
            var $validator = $('#form_data').data('bootstrapValidator').validate();
            //***** edit index number*****
            if($validator.isValid() === true){
                bootbox.dialog({
            		message: "<span class='bigger-110'><i class='fa fa-question-circle text-primary'></i> ยืนยันการทำข้อสอบเสร็จสิ้น</span>",
            		className : "my_width",
            		buttons:
            		{
            			"success" :
            			  {
            				"label" : "<i class='fa fa-check'></i> ตกลง",
            					"className" : "btn-sm btn-success",
            					"callback": function() {
            						//$("#form_data").submit();
                                    window.location.href= site_url+"quiz/index/2/";
            					}
            			 },
            			 "cancel" :
            			  {
            				"label" : "<i class='fa fa-times'></i> ยกเลิก",
            				"className" : "btn-sm btn-white",
            			 }
            		}
            	});
            }
            return $validator.isValid();
      	}
    });


});

function alert_box(text){
  bootbox.dialog({
    message: "<span class='bigger-110'><i class='fa fa-exclamation-circle text-warning'></i> "+text+"</span>",
    className : "my_width",
    buttons:
    {
    	"success" :
    	  {
    		"label" : "<i class='fa fa-check'></i> ตกลง",
    			"className" : "btn-sm btn-default",
    			"callback": function() {
    			}
    	 }
    }
  });
}

</script>

