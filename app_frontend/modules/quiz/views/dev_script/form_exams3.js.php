<script type="text/javascript">
$(document).ready(function() {
    var base_url = '<?php echo base_url();?>';
    var site_url = '<?php echo site_url();?>';
    //$('.content').css({height:'350px'});
    $('form').on('keyup keypress', function(e) { // not enter submit
      var keyCode = e.keyCode || e.which;
      if (keyCode === 13) {
        e.preventDefault();
        return false;
      }
    });

    $('#form_data').bootstrapValidator({
      message: 'กรุณากรอกข้อมูลให้ถูกต้อง.',
      feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
      },
      /*fields: {
          QUIZ_ID: {
                validators: {
                    notEmpty: {
                        message: 'กรุณาเลือกข้อสอบ.'
                    }
                }
          },

        }*/
    }).on('#rootwizard').bootstrapWizard({
        onTabClick: function(tab, navigation, index) {
            var $validator = $('#form_data').data('bootstrapValidator').validate();
            if($validator.isValid() === true){
                $(".button-next").show();
        		$(".btn-primary").show();
                $("#submit_form").hide();
            }
            return $validator.isValid();

        },onPrevious: function(tab, navigation, index){
            var $validator = $('#form_data').data('bootstrapValidator').validate();
            if($validator.isValid() === true){
                $(".button-next").show();
            	$(".btn-primary").show();
                $("#submit_form").hide();
            }
            return $validator.isValid();

      	},onNext: function(tab, navigation, index){
            var $validator = $('#form_data').data('bootstrapValidator').validate();
            //***** edit index number*****
            if(index == 5 && $validator.isValid() === true){
                bootbox.dialog({
            		message: "<span class='bigger-110'><i class='fa fa-question-circle text-primary'></i> ยืนยันการทำข้อสอบเสร็จสิ้น</span>",
            		className : "my_width",
            		buttons:
            		{
            			"success" :
            			  {
            				"label" : "<i class='fa fa-check'></i> ตกลง",
            					"className" : "btn-sm btn-success",
            					"callback": function() {
            						//$("#form_data").submit();
                                    window.location.href= site_url+"quiz/index/2/";
            					}
            			 },
            			 "cancel" :
            			  {
            				"label" : "<i class='fa fa-times'></i> ยกเลิก",
            				"className" : "btn-sm btn-white",
            			 }
            		}
            	});
            }
            return $validator.isValid();

      	},onTabShow: function(tab, navigation, index){
      	    if(index == 0){
                $('input[name="QUIZ_1"]:radio').on('click', function () {
                    var QUIZ_1 = $('input[name="QUIZ_1"]:checked').val();
                    $('#INPUT_QUIZ_1').val(QUIZ_1);
                    $(".tab1").css("background-color","#00a65a");
                });
            }else if(index == 1){
                $('input[name="QUIZ_2"]:radio').on('click', function () {
                    var QUIZ_2 = $('input[name="QUIZ_2"]:checked').val();
                    $('#INPUT_QUIZ_2').val(QUIZ_2);
                    $(".tab2").css("background-color","#00a65a");
                });
            }else if(index == 2){
                $('input[name="QUIZ_3"]:radio').on('click', function () {
                    var QUIZ_3 = $('input[name="QUIZ_3"]:checked').val();
                    $('#INPUT_QUIZ_3').val(QUIZ_3);
                    $(".tab3").css("background-color","#00a65a");
                });
            }else if(index == 3){
                $('input[name="QUIZ_4"]:radio').on('click', function () {
                    var QUIZ_4 = $('input[name="QUIZ_4"]:checked').val();
                    $('#INPUT_QUIZ_4').val(QUIZ_4);
                    $(".tab4").css("background-color","#00a65a");
                });
            }else if(index == 4){
                $('input[name="QUIZ_5"]:radio').on('click', function () {
                    var QUIZ_5 = $('input[name="QUIZ_5"]:checked').val();
                    $('#INPUT_QUIZ_5').val(QUIZ_5);
                    $(".tab5").css("background-color","#00a65a");
                });
            }

            if(index==0){
                $(".button-previous").hide();
            }else if(index==4){//***** edit index number*****
    			$(".button-next").hide();
    			$(".button-previous").show();
                $("#submit_form").show();
                $("#submit_form").attr("disabled", false);
    		}else{
    			$(".button-next").show();
                $(".button-previous").show();
                $("#submit_form").hide();
    		}
      	}
    });


});

function alert_box(text){
  bootbox.dialog({
    message: "<span class='bigger-110'><i class='fa fa-exclamation-circle text-warning'></i> "+text+"</span>",
    className : "my_width",
    buttons:
    {
    	"success" :
    	  {
    		"label" : "<i class='fa fa-check'></i> ตกลง",
    			"className" : "btn-sm btn-default",
    			"callback": function() {
    			}
    	 }
    }
  });
}

</script>

