<script type="text/javascript">
$(document).ready(function() {
    var base_url = '<?php echo base_url();?>';
    var site_url = '<?php echo site_url();?>';

    $('form').on('keyup keypress', function(e) { // not enter submit
      var keyCode = e.keyCode || e.which;
      if (keyCode === 13) {
        e.preventDefault();
        return false;
      }
    });

    $('#tb_quiz').dataTable( {
            "aoColumnDefs": [
                  {"bSortable":false,"aTargets":[6]},
                  { "bSearchable": false, "aTargets": [6] }
            ],
            //'iDisplayLength': 1
            //"aaSorting": [[ 2, "asc" ]],
    });

    $('#form_data').bootstrapValidator({
      message: 'กรุณากรอกข้อมูลให้ถูกต้อง.',
      feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
          QUIZ_ID: {
                validators: {
                    notEmpty: {
                        message: 'กรุณาเลือกข้อสอบ.'
                    }
                }
          },

        }
    }).on('#rootwizard').bootstrapWizard({
        onTabClick: function(tab, navigation, index) {
            var $validator = $('#form_data').data('bootstrapValidator').validate();
            if($validator.isValid() === true){
                $(".button-next").show();
        		$(".btn-primary").show();
                $("#submit_form").hide();
            }
            return $validator.isValid();

        },onPrevious: function(tab, navigation, index){
            var $validator = $('#form_data').data('bootstrapValidator').validate();
            if($validator.isValid() === true){
                $(".button-next").show();
            	$(".btn-primary").show();
                $("#submit_form").hide();
            }
            return $validator.isValid();

      	},onNext: function(tab, navigation, index){
            var $validator = $('#form_data').data('bootstrapValidator').validate();
            //***** edit index number*****
            if(index == 4 && $validator.isValid() === true){
                bootbox.dialog({
            		message: "<span class='bigger-110'><i class='fa fa-question-circle text-primary'></i> ยืนยันการบันทึกข้อมูล</span>",
            		className : "my_width",
            		buttons:
            		{
            			"success" :
            			  {
            				"label" : "<i class='fa fa-check'></i> ตกลง",
            					"className" : "btn-sm btn-success",
            					"callback": function() {
            						$("#form_data").submit();
            					}
            			 },
            			 "cancel" :
            			  {
            				"label" : "<i class='fa fa-times'></i> ยกเลิก",
            				"className" : "btn-sm btn-white",
            			 }
            		}
            	});
            }
            return $validator.isValid();

      	},onTabShow: function(tab, navigation, index){
      	    if(index == 0){

                $('#rootwizard .progress-bar').css({width:'12%'});
                $('.tab-content').css({height:'350px'});

                $('.button-next').click(function () {
                  var QUIZ_ID =  $('input[name="QUIZ_ID"]:checked').val();
                  if(QUIZ_ID == undefined){
                      alert_box('กรุณาเลือกข้อสอบ');
                  }
                });

            }else if(index == 1){
                $('#rootwizard .progress-bar').css({width:'38%'});
                $('.tab-content').css({height:'400px'});
            }else if(index == 2){
                $('#rootwizard .progress-bar').css({width:'63%'});
                $('.tab-content').css({height:'600px'});
            }else if(index == 3){
                $('#rootwizard .progress-bar').css({width:'100%'});
                $('.tab-content').css({height:'600px'});
            }

            if(index==0){
                $(".button-previous").hide();
            }else if(index==3){//***** edit index number*****
    			$(".button-next").hide();
    			$(".button-previous").show();
                $("#submit_form").show();
                $("#submit_form").attr("disabled", false);
    		}else{
    			$(".button-next").show();
                $(".button-previous").show();
                $("#submit_form").hide();
    		}
      	}
    });


});

function alert_box(text){
  bootbox.dialog({
    message: "<span class='bigger-110'><i class='fa fa-exclamation-circle text-warning'></i> "+text+"</span>",
    className : "my_width",
    buttons:
    {
    	"success" :
    	  {
    		"label" : "<i class='fa fa-check'></i> ตกลง",
    			"className" : "btn-sm btn-default",
    			"callback": function() {
    			}
    	 }
    }
  });
}

</script>

