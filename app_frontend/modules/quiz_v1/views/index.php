<div class="box-body">
  <div class="wrap-form clearfix ">
  <?php
  $form = array("id"=>"form_data","name"=>"form_data","accept-charset"=>"utf-8","class"=>"form-horizontal");
  echo (! empty($item) ?  form_open_multipart('quiz/update', $form) :  form_open_multipart('quiz/save', $form));
  ?>
  <section id="wizard">
      <div id="rootwizard">
        <div class="navbar">
         <div class="navbar-inner">
            <div class="progress progress-sm active">
              <div id="bar" class="progress active" >
                  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>
            <ul class="nav nav-wizard">
              <li class="active" style="width: 25%;"><a href="#tab1" data-toggle="tab"></a>ข้อสอบ</li>
              <li style="width: 25%;"><a href="#tab2" data-toggle="tab"></a>คำอธิบาย</li>
              <li style="width: 25%;"><a href="#tab3" data-toggle="tab"></a>ทำข้อสอบ</li>
              <li style="width: 25%;"><a href="#tab4" data-toggle="tab"></a>คะแนน</li>
            </ul>
         </div>
        </div>

        <div class="tab-content">

           <div class="tab-pane active" id="tab1">
                <div class="portlet portlet-basic">
                    <div class="portlet-body">
    					<div class="row">
    						<div class="col-lg-12">
    							<table id="tb_quiz" class="table table-bordered table-striped">
    								<thead>
    									<tr>
    									    <th class="text-center" width="10%">รหัสข้อสอบ</th>
    										<th class="text-center" width="30%">ข้อสอบ</th>
    										<th class="text-center" width="15%">ชนิดข้อสอบ</th>
                                            <th class="text-center" width="10%">จำนวนข้อ</th>
    										<th class="text-center" width="10%">เกณฑ์คะแนน</th>
                                            <th class="text-center" width="10%">เวลาที่ใช้สอบ</th>
                                            <th class="text-center" width="10%">เลือกข้อสอบ</th>
    									</tr>
    								</thead>
    								<tbody>

                                        <?php foreach($quiz as $quiz_data){ ?>
                                        <tr>
                                          <td class="text-center">
                                            <?php echo $quiz_data['QUIZ_ID'];?>
                                          </td>
                                          <td class="text-left">
                                            <?php echo $quiz_data['QUIZ_NAME'];?>
                                          </td>
                                          <td class="text-center">
                                            <?php echo $quiz_data['QUIZ_TYPE'];?>
                                          </td>
                                          <td class="text-center">
                                            <?php echo $quiz_data['QUIZ_QUANTITY'];?>
                                          </td>
                                          <td class="text-center">
                                            <?php echo $quiz_data['QUIZ_SCORE'];?>
                                          </td>
                                          <td class="text-center">
                                            <?php echo $quiz_data['QUIZ_TIME'];?>
                                          </td>
                                          <td class="text-center">
                                            <label class="control-label">
                                              <input type="radio" id="QUIZ_ID" name="QUIZ_ID" value="<?php echo $quiz_data['QUIZ_ID'];?>">
                                              เลือก
                                            </label>
                                          </td>
    									</tr>
                                        <?php } ?>

                                    </tbody>
        						</table>
    						</div>
    					</div>
    				</div>

                </div>
           </div>

           <div class="tab-pane" id="tab2">

           </div>

           <div class="tab-pane" id="tab3">

           </div>

           <div class="tab-pane" id="tab4">

           </div>

        </div>

        <ul class="pager wizard">
           <div id="form-error-div" class="text-danger"></div>
           <li class="previous"><button type="button" class="btn btn-default button-previous" name="previous" value="Prev"> <i class="fa fa-chevron-left"></i> ก่อนหน้า </button></li>
           <li class="next">
            <button type="button" class="btn btn-primary button-next" name="next" value="Next"> ถัดไป <i class="fa fa-chevron-right"></i></button>
            <button type="button" id="submit_form" class="btn btn-success" title="บันทึกข้อมูล" style="display:none"><i class="fa fa-check"></i>&nbsp;เสร็จสิ้น</button>
           </li>
         </ul>

      </div>
    </section>
    <?php echo form_close();?>
  </div>
</div>

