<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quiz extends MX_Controller {

    private $title = "แบบทดสอบ";
    public function __construct()
    {
        parent::__construct();

        //***** load Model *****
        //$this->load->model('admin/quiz_model');

        //***** Include Template *****
        $this->template
        ->set_layout('template/layout/template')
        ->set_view('notices','template/notices')
        ->set_view('user_signin','template/user_signin')
        ->set_view('footer','template/footer');
    }

	public function index()
	{
            //***** DATA MENU Active *****
            $text_header = $this->title;
            $data_menu['menu_active'] = "quiz";
            $data_menu['submenu_active'] = "";

            //*****DATA Breadcrumb*****
            $data_bread['page_icon'] = "fa fa-file-text-o";
            $data_bread['setting_bread']["bread_name"] = array($text_header);
            $data_bread['setting_bread']["bread_link"] = array();
            $data_bread['setting_bread']["bread_active"] = 0;

            //*****DATA Build*****
            $result = array();
            $result[] = array(
                'QUIZ_ID' => '1001',
                'QUIZ_NAME' => 'ข้อสอบโครงการอัจฉริยภาพ วิทยาศาสตร์ ป.6',
                'QUIZ_TYPE' => 'ปรนัย',
                'QUIZ_QUANTITY' => '5 ข้อ',
                'QUIZ_SCORE' => '3 - 5 ผ่าน',
                'QUIZ_TIME' => '1 ชั่วโมง',
            );
            $result[] = array(
                'QUIZ_ID' => '1002',
                'QUIZ_NAME' => 'ข้อสอบภาษาไทย ชั้น ป.4',
                'QUIZ_TYPE' => 'อัตนัย',
                'QUIZ_QUANTITY' => '5 ข้อ',
                'QUIZ_SCORE' => '3 - 5 ผ่าน',
                'QUIZ_TIME' => 'ไม่จำกัด',
            );
            $result[] = array(
                'QUIZ_ID' => '1003',
                'QUIZ_NAME' => 'ข้อสอบคณิตศาสตร์ ชั้น ป.3',
                'QUIZ_TYPE' => 'จับคู่',
                'QUIZ_QUANTITY' => '5 ข้อ',
                'QUIZ_SCORE' => '3 - 5 ผ่าน',
                'QUIZ_TIME' => '40 นาที',
            );

            $result[] = array(
                'QUIZ_ID' => '1003',
                'QUIZ_NAME' => 'ข้อสอบความรู้เบื้องต้นเกี่ยวกับคอมพิวเตอร์',
                'QUIZ_TYPE' => 'เติมคำในช่องว่าง',
                'QUIZ_QUANTITY' => '5 ข้อ',
                'QUIZ_SCORE' => '3 - 5 ผ่าน',
                'QUIZ_TIME' => '30 นาที',
            );

            $data['quiz'] = $result;

            //***** Template *****
            $this->template
            //***** Title *****
            ->title($text_header)

            //***** Plugins Datatables *****
            ->css('assets/plugins/datatables/dataTables.bootstrap.css')
            ->js_foot('assets/plugins/datatables/jquery.dataTables.min.js')
            ->js_foot('assets/plugins/datatables/dataTables.bootstrap.min.js')

             //***** Plugins Jquery-ui *****
             ->css('assets/plugins/jquery-ui/css/jquery-ui.min.css')
             ->js_foot('assets/plugins/jquery-ui/js/jquery-ui.min.js')

            //***** Plugins Wizard *****
            ->css('assets/plugins/bootstrap-wizard/prettify.css')
            ->css('assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.css')
            ->js_foot('assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js')
            ->js_foot('assets/plugins/bootstrap-wizard/prettify.js')

            //***** Plugins fancyapps-fancyBox *****
            ->css('assets/admin/plugins/fancyapps-fancyBox/source/jquery.fancybox.css?v=2.1.5')
            ->css('assets/admin/plugins/fancyapps-fancyBox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5')
            ->css('assets/admin/plugins/fancyapps-fancyBox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7')
            ->js('assets/admin/plugins/fancyapps-fancyBox/lib/jquery.mousewheel-3.0.6.pack.js')
            ->js('assets/admin/plugins/fancyapps-fancyBox/source/jquery.fancybox.js?v=2.1.5')
            ->js('assets/admin/plugins/fancyapps-fancyBox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5')
            ->js('assets/admin/plugins/fancyapps-fancyBox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7')
            ->js('assets/admin/plugins/fancyapps-fancyBox/js/plugin_lightbox.js')

            //***** Plugins Validator *****
            ->css('assets/plugins/bootstrap-validator/css/bootstrapValidator.css')
            ->js_foot('assets/plugins/bootstrap-validator/js/bootstrapValidator.js')

            //***** Plugins bootstrap-fileupload *****
            ->css('assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css')
            ->js_foot('assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js')

            //***** DEV JAVA SCRIPT *****
            ->set_view('dev_script','quiz/dev_script/index.js.php')

            //***** Response *****
            ->set_view('menu','template/menu', $data_menu)
            ->set_view('breadcrumb','template/breadcrumb',$data_bread)
            ->build('quiz/index',$data);
	}

    public function detail($id)
    {

    }
}
