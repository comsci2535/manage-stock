<?php if(!defined('BASEPATH'))exit('No direct script allowed');

class Member_model extends CI_model{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

    public function getDataRows($recycle=NULL,$keyword=NULL)
	{
	    $_where = NULL;
        $_where .= "WHERE member.MemberRecycle = '".$recycle."' AND member.MemberPermissionLevel != 'super_administrator'";
        if($keyword != NULL)
		{
            $_where .= " AND member.MemberUser LIKE '%".$keyword."%' OR member.MemberFirstname LIKE '%".$keyword."%' OR member.MemberLastname LIKE '%".$keyword."%'";
		}
		$sql = "SELECT * FROM member ".$_where;
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

    public function getData($recycle=NULL,$colOrder=NULL,$orderType=NULL,$keyword=NULL,$sLimit=NULL)
	{
		$_where = NULL;
		$_order = NULL;
        $_where .= "WHERE member.MemberRecycle = '".$recycle."' AND member.MemberPermissionLevel != 'super_administrator'";

        if($keyword != NULL)
		{
            $_where .= " AND member.MemberUser LIKE '%".$keyword."%' OR member.MemberFirstname LIKE '%".$keyword."%' OR member.MemberLastname LIKE '%".$keyword."%'";
		}

        if($colOrder == 1){
		  if($orderType=="asc")
		  {
			$_order = " order by member.MemberUser asc ";
		  }
		  if($orderType=="desc")
		  {
			$_order = " order by member.MemberUser desc ";
		  }
		}else if($colOrder == 2){
		  if($orderType=="asc")
		  {
			$_order = " order by member.MemberFirstname asc ";
		  }
		  if($orderType=="desc")
		  {
			$_order = " order by member.MemberFirstname desc ";
		  }
		}else if($colOrder == 3){
		  if($orderType=="asc")
		  {
			$_order = " order by member_level.member_level_name asc ";
		  }
		  if($orderType=="desc")
		  {
			$_order = " order by member_level.member_level_name desc ";
		  }
        }else if($colOrder == 4){
		  if($orderType=="asc")
		  {
			$_order = " order by member.MemberEditorDate asc ";
		  }
		  if($orderType=="desc")
		  {
			$_order = " order by member.MemberEditorDate desc ";
		  }
        }else if($colOrder == 5){
		  if($orderType=="asc")
		  {
			$_order = " order by member.MemberActive asc ";
		  }
		  if($orderType=="desc")
		  {
			$_order = " order by member.MemberActive desc ";
		  }
		}
		else
		{
		  $_order = " order by member.MemberEditorDate desc";
		}

        $sql = "SELECT * FROM member
        LEFT JOIN member_level ON member.MemberPermissionLevel = member_level.member_level_code
        $_where $_order $sLimit";

        $query = $this->db->query($sql);
		return $query->result_array();
	}

    public function getDetail($id)
	{
	    $_where = NULL;
		$_where .= "WHERE member_id = '".$id."'";
		$sql = "SELECT * FROM member ".$_where;
		$query = $this->db->query($sql);
		return $query->row_array();

	}

    public function getMemberLevel()
	{
	    $_where = NULL;
		$_where .= "WHERE member_level_active = '1'";
		$sql = "SELECT * FROM member_level ".$_where." ORDER BY member_level_order ASC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

    public function getPermission()
	{
	    $_where = NULL;
		$_where .= "WHERE PermissionRecycle = '0' AND PermissionActive = '1'";
		$sql = "SELECT * FROM permission ".$_where." ORDER BY PermissionID ASC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

    public function check_username($username=NULL)
    {
      return $this->db->where("MemberUser",$username)->get("member")->num_rows();
    }

    public function check_email($email=NULL)
    {
        return $this->db->where('MemberEmail',$email)->get('member')->num_rows();
    }

    public function insertData($member_data="")
    {
      return $this->db->insert('member', $member_data);
    }

    public function updateData($id=0,$data="")
    {
      return $this->db->where('member_id',$id)->update('member',$data);
    }

    public function getCategory($CategoryType=NULL,$CategoryParent=NULL)
	{
	    $_where = NULL;
		$_where .= "WHERE CategoryRecycle = '0' AND CategoryActive = '1' ";
        if($CategoryType!=NULL){
            $_where .= " AND CategoryType = '".$CategoryType."'";
        }
        if($CategoryParent!=NULL){
            $_where .= " AND CategoryParent = '".$CategoryParent."'";
        }
		$sql = "SELECT * FROM category ".$_where." ORDER BY CategoryOrder ASC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}


}
