<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/***********
 * @todo config database
This can be set to anything, but default usage is:
 *     default
 *     testing
 *     production
 ***********/
$ENVIRONMENT = 'default';
$hostname = '';
$username = '';
$password = '';
$database = '';
switch ($ENVIRONMENT) {
    case 'production'   :
        $active_group = 'default';
        $hostname = 'localhost';
        $username = 'webdemo_user';
        $password = 'Y06j35jswY';
        $database = 'webdemo_db';
        break;
    case 'testing'      :
        $active_group = 'default';
        $hostname = '';
        $username = '';
        $password = '';
        $database = 'med_db';
        break;
    default :
        $active_group = 'default';
        $hostname = 'localhost';
        $username = 'root';
        $password = '12345678';
        $database = 'db_qalb';
        break;
}
/* End of Config database */

$db['default'] = array(
	'dsn'	=> '',
	'hostname' => $hostname,
	'username' => $username,
	'password' => $password,
    'database' => $database,
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);
