/*
Navicat MySQL Data Transfer

Source Server         : MySQL Local
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : plateform_db

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2019-03-30 00:19:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for coupons
-- ----------------------------
DROP TABLE IF EXISTS `coupons`;
CREATE TABLE `coupons` (
  `coupon_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `slug` text,
  `detail` text,
  `type` varchar(30) DEFAULT '' COMMENT '1=ราคา,2=%',
  `discount` int(15) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `excerpt` text,
  `titleLink` varchar(255) DEFAULT NULL,
  `metaTitle` text,
  `metaDescription` text,
  `metaKeyword` text,
  `couponCode` varchar(15) DEFAULT NULL,
  `isCourse` int(1) DEFAULT '0' COMMENT '0=ไม่จำกัดคอร์ส,1=จำกัดจำนวนคอร์ส',
  `isCourseNum` int(10) DEFAULT NULL COMMENT 'ระบุจำนวน',
  `isMember` int(1) DEFAULT '0' COMMENT '0=ไม่จำกัดจำนวนคน,1=จำกัดจำนวนคน',
  `isMemberNum` int(10) DEFAULT NULL COMMENT 'ระบุจำนวน',
  `created_at` datetime NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`coupon_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of coupons
-- ----------------------------
INSERT INTO `coupons` VALUES ('13', 'จำกัดคอร์สเรียน/คน 1 คอร์ส แต่ไม่จำกัดคนลงทะเบียน ', 'จำกัดคอร์สเรียน-คน-1-คอร์ส-แต่ไม่จำกัดคนลงทะเบียน', null, '2', '10', '1', '2018-12-08', '2019-01-31', '', null, null, null, null, 'MGKC54R', '1', '1', '0', '0', '0000-00-00 00:00:00', '', '2019-03-29 22:47:33', '1', '0', null, null);
INSERT INTO `coupons` VALUES ('14', 'ไม่จำกัดคอร์สเรียน/คน  แต่จำกัดคนลงทะเบียนเพียง 2 คน', null, null, '2', '5', '0', '2018-10-12', '2018-10-20', null, null, null, null, null, 'CMV63L3', '0', '0', '1', '2', '0000-00-00 00:00:00', '', '2019-03-29 22:55:27', '1', '0', '2019-03-29 22:55:22', '1');
INSERT INTO `coupons` VALUES ('15', 'จำกัดคอร์สเรียน/คน 1 คอร์ส และ จำกัดคนลงทะเบียนแค่ 2 คน', null, null, '1', '100', '1', '2018-10-12', '2018-10-20', null, null, null, null, null, '9GVBBWR', '1', '1', '1', '2', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0', null, null);
